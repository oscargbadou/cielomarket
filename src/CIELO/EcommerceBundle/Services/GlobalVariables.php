<?php

namespace CIELO\EcommerceBundle\Services;

class GlobalVariables extends \Twig_Extension {

    private $doctrine;

    public function __construct($doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getVars() {
        $domaineActivites = $this->doctrine
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")
                ->createQueryBuilder("d")
                ->where("d.actif = true")
                ->getQuery()
                ->getResult();
                //->findBy(array(), array(), 7, 0);
        $domainePlus =  $this->doctrine
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")->nbreDomaine() > 7;
        $familles = $this->doctrine
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Famille")
                ->findAll();
        $entreprises = $this->doctrine
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->findAll();
        $nouveauProduit = $this->doctrine
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->createQueryBuilder("p")
                ->orderBy("p.dateAjout", "DESC")
                ->setFirstResult(0)
                ->setMaxResults(10)
                ->getQuery()
                ->getResult();
        $now = new \DateTime();
        $evenements = $this->doctrine
                ->getManager()
                ->getRepository("CIELOAdminBundle:Evenement")
                ->createQueryBuilder("e")
                ->where("e.dateDebut <= :now")
                ->where("e.dateFin >= :now")
                ->setParameter("now", $now)
                ->getQuery()
                ->getResult();
//        $produitEnPromo = $this->doctrine
//                ->getManager()
//                ->getRepository("CIELOEntrepriseBundle:Produit")
//                ->createQueryBuilder("p")
//                ->where("p.prix != p.prixPromo")
//                ->getQuery()
//                ->getResult();
        $produitPromo = $this->doctrine
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:PromotionProduit")
                ->createQueryBuilder("pp")
                ->leftJoin("pp.promotion", "p")
                ->where("p.dateDebutPromotion <= :now")
                ->andWhere("p.dateFinPromotion >= :now")
                ->setParameter("now", $now)
                ->setFirstResult(0)
                ->setMaxResults(10)
                ->getQuery()
                ->getResult();
        $newTexteDefilant = $this->doctrine
                ->getManager()
                ->getRepository("CIELOAdminBundle:TexteDefilant")
                ->createQueryBuilder("td")
                ->where("td.actif = true")
                ->getQuery()
                ->getResult();
        $pubPanelGauche = $this->doctrine
                ->getManager()
                ->getRepository("CIELOAdminBundle:Pub")
                ->createQueryBuilder("p")
                ->orderBy("p.date", "DESC")
                ->where("p.panelGauche = true")
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
        $pubPanelDroit = $this->doctrine
                ->getManager()
                ->getRepository("CIELOAdminBundle:Pub")
                ->createQueryBuilder("p")
                ->orderBy("p.date", "DESC")
                ->where("p.panelDroit = true")
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
        
        $dom = array();
        $fam = array();
        $ent = array();
        if (count($domaineActivites) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($dom, $domaineActivites[$i]);
            }
        } else {
            $dom = $domaineActivites;
        }

        if (count($entreprises) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($ent, $entreprises[$i]);
            }
        } else {
            $ent = $entreprises;
        }
        
        return array(
            'domaineActivites' => $domaineActivites,
            'domainePlus'=> $domainePlus,
            'nouveauProduit' => $nouveauProduit,
            'produitEnPromo' => $produitPromo,
            'evenements' => $evenements,
            'texteDefilant'=>$newTexteDefilant,
            'pubPanelGauche'=>$pubPanelGauche?$pubPanelGauche[0]:null,
            'pubPanelDroit'=>$pubPanelDroit?$pubPanelDroit[0]:null
        );
    }

    public function getFunctions() {
        return array(
            "getVars" => new \Twig_Function_Method($this, "getVars")
        );
    }

    public function getName() {
        return "cieloGlobals";
    }

}

