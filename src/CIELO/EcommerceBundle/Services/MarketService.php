<?php

namespace CIELO\EcommerceBundle\Services;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManager;
use CIELO\EcommerceBundle\Utils\Utils;
use CIELO\EcommerceBundle\Entity\Commande;
use CIELO\EcommerceBundle\Entity\CommandeProduit;
use CIELO\EntrepriseBundle\Entity\CommentaireProduit;
use CIELO\EntrepriseBundle\Entity\NoteProduitUser;
use CIELO\AdminBundle\Entity\ParticipationEvenement;
use CIELO\EntrepriseBundle\Entity\RendezVous;

class MarketService {

    private $um;
    private $em;
    private $discriminator;
    private $encodedFactory;

    public function __construct($um, $em, $discriminator, $encodedFactory) {
        $this->um = $um;
        $this->em = $em;
        $this->discriminator = $discriminator;
        $this->encodedFactory = $encodedFactory;
    }

    public function login($username, $password) {
        $this->discriminator->setClass('CIELO\UserBundle\Entity\Client');
        //die($this->session->get("pugx_user.user_discriminator.class"));
        $user = $this->um->findUserByUsername($username);
        if ($user != null) {
            $encoder = $this->encodedFactory->getEncoder($user);
            if ($user->getPassword() === $encoder->encodePassword($password, $user->getSalt())) {
                return $user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function produitsCritere($criteria, $start, $count) {
        $now = new \DateTime();
        $promoTermines = $this->em->getRepository("CIELOEntrepriseBundle:Promotion")
                ->createQueryBuilder("p")
                ->where("p.dateFinPromotion < :now")
                ->setParameter("now", $now)
                ->getQuery()
                ->getResult();
        foreach ($promoTermines as $p) {
            $produitPromotion = $this->em->getRepository("CIELOEntrepriseBundle:PromotionProduit")
                    ->findByPromotion($p);
            foreach ($produitPromotion as $pp) {
                $modeles = $this->em->getRepository("CIELOEntrepriseBundle:Modele")
                        ->findByProduit($pp->getProduit());
                foreach ($modeles as $m) {
                    if ($m->getPrixPromo() != $m->getPrix()) {
                        $m->setPrixPromo($m->getPrix());
                        $this->em->flush();
                    }
                }
            }
        }
        $produits = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->produitsParCriter($criteria, $start, $count);
        if ($produits == null)
            return null;
        $ReturnArray = array();
        foreach ($produits as $p) {
            $ReturnArray[] = $p->toJSON(true);
        }

        return Utils::jsonRemoveUnicodeSequences(json_encode($ReturnArray));
    }

    public function detailProduit($id) {
        $produit = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->find($id);
        $noteProduit = $this->em->getRepository("CIELOEntrepriseBundle:NoteProduitUser")->noteProduit($id);
        $noteProduitArray = array(
            'note' => ($noteProduit > 0) ? intval($noteProduit) : 0
        );
        if ($produit != null) {
            $detail = $produit->toCompleteJSON();
            $detailArray = json_decode($detail, true);
            $detailComplete = array_merge((array) $detailArray, (array) $noteProduitArray);
            return Utils::jsonRemoveUnicodeSequences(json_encode($detailComplete));
        } else {
            return null;
        }
    }

    public function detailFamille($id) {
        $famille = $this->em->getRepository("CIELOEntrepriseBundle:Famille")->find($id);
        if ($famille != null) {
            return $detail = $famille->toCompleteJSON();
        } else {
            return null;
        }
    }

    public function detailEntreprise($id) {
        $entreprise = $this->em->getRepository("CIELOEntrepriseBundle:Entreprise")->find($id);
        if ($entreprise != null) {
            return $entreprise->toCompleteJSON();
        } else {
            return null;
        }
    }

    public function familleEntreprise($entr_id, $start, $count) {
        $familles = $this->em->getRepository("CIELOEntrepriseBundle:Entreprise")->famillesEntreprise($entr_id, $start, $count);
        if ($familles == null)
            return null;
        $ReturnArray = array();
        foreach ($familles as $fa) {
            $ReturnArray[] = $fa->toJSON(true);
        };

        return Utils::jsonRemoveUnicodeSequences(json_encode($ReturnArray));
    }

    public function produitFamilleEntreprise($entr_id, $fam_id, $categorie, $start, $count) {

        $produits = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->produitsParFamilleEntrepriseCategorie($entr_id, $fam_id, $categorie, $start, $count);
        if ($produits == null)
            return null;
        $ReturnArray = array();
        foreach ($produits as $p) {
            $ReturnArray[] = $p->toJSON(true);
        }

        return Utils::jsonRemoveUnicodeSequences(json_encode($ReturnArray));
    }

    public function produitFamille($fam_id, $criteria, $start, $count) {
        $produits = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->produitFamille($fam_id, $criteria, $start, $count);
        if ($produits == null)
            return null;
        $ReturnArray = array();
        foreach ($produits as $p) {
            $ReturnArray[] = $p->toJSON(true);
        }

        return Utils::jsonRemoveUnicodeSequences(json_encode($ReturnArray));
    }

    public function categorieEntreprise($entr_id) {
        $categorie = $this->em->getRepository("CIELOEntrepriseBundle:Categorie")->findByEntreprise($entr_id);
        $categorieArray = array();
        foreach ($categorie as $cat) {
            array_push($categorieArray, $cat->getNom());
        }
        return $this->jsonRemoveUnicodeSequences(json_encode($categorieArray));
    }

    public function campagneJeu($start, $count) {
        $campagnesJeu = $this->em->getRepository("CIELOAdminBundle:CampagneJeu")->findBy(array(), array(), $count, $start);
        $campagneJeuArray = array();
        foreach ($campagnesJeu as $campagneJeu) {
            $campagneJeuArray[] = $campagneJeu->toJSON(true);
        }
        return $this->jsonRemoveUnicodeSequences(json_encode($campagneJeuArray));
    }

    public function campagneJeuEntreprise($entr_id) {

        $campagne = $this->em->getRepository("CIELOGameBundle:CampagneJeu")->campagneJeuEntreprise($entr_id);
        foreach ($campagne as $c) {
            $campagneArray[] = $c->toJSON(true);
        }
        return $this->jsonRemoveUnicodeSequences(json_encode($campagneArray));
    }

    public function enregistrerCommande($id_user, $id_modelivraison, $id_modepaiement, $id_zone, $adresse, $type, $panier) {
        $client = $this->em->getRepository("CIELOUserBundle:User")->find(intval($id_user));
        $zone = $this->em
                ->getRepository('CIELOAdminBundle:ZoneGeographique')
                ->find(intval($id_zone));
        $modeLivraison = $this->em
                ->getRepository('CIELOAdminBundle:ModeLivraison')
                ->find(intval($id_modelivraison));
        $modePaiement = $this->em
                ->getRepository('CIELOAdminBundle:ModePaiement')
                ->find(intval($id_modepaiement));
        $modeLivraisonZoneGeographique = $this->em
                ->getRepository('CIELOAdminBundle:ModeLivraisonZoneGeographique')
                ->findOneBy(array(
            'modeLivraison' => $modeLivraison,
            'zoneGeographique' => $zone
        ));
        $montantLivraison = $modeLivraisonZoneGeographique->getPrix();
        if ($type == "facture") {
            $msgFacture = "";
            $montantTotalFacture = 0;

            foreach ($panier as $modeleCommander) {
                $modele = $this->em->getRepository("CIELOEntrepriseBundle:Modele")->find(intval($modeleCommander["id_modele"]));
                $quantite = intval($modeleCommander["quantite"]);
                $msgFacture = $msgFacture . "" . $quantite . " x " . $modele->getProduit()->getNom() . ": " . ($modele->getPrixPromo() * $quantite) . " F CFA \n";
                $montantTotalFacture += $modele->getPrixPromo() * $quantite;
            }
            $msgFacture = $msgFacture . "\n--------------------------------\n" . "Total: " . $montantTotalFacture . " F CFA \n\n - Mode de paiement: " . $modePaiement->getNom() . "\n\n Autres Frais:\n - Mode de livraison: " . $modeLivraison->getNom() . "\n Cout: " . $montantLivraison . "F CFA\n--------------------------------\n Total: " . ($montantTotalFacture + $montantLivraison) . " F CFA\n";
            return $msgFacture;
        } else {
            $msgFacture = "";
            $montantTotalFacture = 0;
            foreach ($panier as $modeleCommander) {
                $modele = $this->em->getRepository("CIELOEntrepriseBundle:Modele")->find(intval($modeleCommander["id_modele"]));
                $quantite = intval($modeleCommander["quantite"]);
                $msgFacture = $msgFacture . "" . $quantite . " x " . $modele->getProduit()->getNom() . ": " . ($modele->getPrixPromo() * $quantite) . " F CFA \n";
                $montantTotalFacture += $modele->getPrixPromo() * $quantite;
            }
            $msgFacture = $msgFacture . "\n--------------------------------\n" . "Total: " . $montantTotalFacture . " F CFA \n\n - Mode de paiement: " . $modePaiement->getNom() . "\n\n Autres Frais:\n - Mode de livraison: " . $modeLivraison->getNom() . "\n Cout: " . $montantLivraison . "F CFA \n--------------------------------\n Total: " . ($montantTotalFacture + $montantLivraison) . " F CFA\n";
            $commande = new Commande();
            $commande->setClient($client);
            $commande->setAdresse($adresse);
            $commande->setModeLivraison($modeLivraison);
            $commande->setModePaiement($modePaiement);
            $commande->setZone($zone->getNom());
            $commande->setContenuFacture($msgFacture);
            $commande->setMontantFacture($montantTotalFacture + $montantLivraison);

            $this->em->persist($commande);
            $this->em->flush();
            $commande->setCodeFacture($id_user . date("Ymd") . $commande->getId());
            foreach ($panier as $i => $produitCommander) {
                $modele = $this->em->getRepository("CIELOEntrepriseBundle:Modele")->find(intval($produitCommander["id_modele"]));
                $commandeProduit[$i] = new CommandeProduit();
                $commandeProduit[$i]->setCommande($commande);
                $commandeProduit[$i]->setModele($modele);
                $commandeProduit[$i]->setQuantite(intval($produitCommander["quantite"]));
                $modele->getProduit()->setPurchased($modele->getProduit()->getPurchased() + 1);
                $this->em->persist($commandeProduit[$i]);
            }
            $this->em->flush();
            $result = array(
                'msgFacture' => $msgFacture,
                'codeFacture' => $commande->getCodeFacture()
            );
            return $this->jsonRemoveUnicodeSequences(json_encode($result));
        }
    }

    public function commenterProduit($id_auteur, $id_produit, $contenu) {
        $auteur = $this->em->getRepository("CIELOUserBundle:User")->find(intval($id_auteur));
        $produit = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->find(intval($id_produit));
        if ($auteur != null && $produit != null && $contenu != null) {
            $newCommentaire = new CommentaireProduit();
            $newCommentaire->setUser($auteur);
            $newCommentaire->setProduit($produit);
            $newCommentaire->setContenu($contenu);
            $this->em->persist($newCommentaire);
            $this->em->flush();
        }
    }

    public function participerEvenement($id_user, $id_evenement) {
        $user = $this->em->getRepository("CIELOUserBundle:User")->find(intval($id_user));
        $evenement = $this->em->getRepository("CIELOAdminBundle:Evenement")->find(intval($id_evenement));
        $participerEvenement = $this->em
                ->getRepository("CIELOAdminBundle:ParticipationEvenement")
                ->findOneBy(array(
            'utilisateur' => $user,
            'evenement' => $evenement
        ));
        if ($participerEvenement) {
            return "Vous participez déjà à cet évènement";
        } else {
            $newParticipationEvenement = new ParticipationEvenement();
            $newParticipationEvenement->setEvenement($evenement);
            $newParticipationEvenement->setUtilisateur($user);
            $this->em->persist($newParticipationEvenement);
            $this->em->flush();
            return "Votre participation a été enregistrée avec succès: CieloMarket vous enverra un email pour beaucoup plus 'dinformation sur l'évènement";
        }
    }

    public function noterProduit($id_auteur, $id_produit, $note) {
        $user = $this->em->getRepository("CIELOUserBundle:User")->find(intval($id_auteur));
        $produit = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->find(intval($id_produit));
        $noteProduitUser = $this->em
                ->getRepository("CIELOEntrepriseBundle:NoteProduitUser")
                ->findOneBy(array(
            'user' => $user,
            'produit' => $produit
        ));
        if ($noteProduitUser) {
            $noteProduitUser->setNote(intval($note));
            $this->em->flush();
        } else {
            $newnoteProduitUser = new NoteProduitUser();
            $newnoteProduitUser->setProduit($produit);
            $newnoteProduitUser->setUser($user);
            $newnoteProduitUser->setNote(intval($note));
            $this->em->persist($newnoteProduitUser);
            $this->em->flush();
        }
    }

    public function commentaireProduit($id, $start, $count) {
        $commentaires = $this->em
                ->getRepository("CIELOEntrepriseBundle:CommentaireProduit")
                ->createQueryBuilder("c")
                ->leftJoin("c.produit", "p")
                ->where("p.id = :idproduit")
                ->orderBy("c.date", "DESC")
                ->setParameter("idproduit", $id)
                ->setFirstResult($start)
                ->setMaxResults($count)
                ->getQuery()
                ->getResult();
        if (count($commentaires) > 0) {
            foreach ($commentaires as $c) {
                $commentaireArray[] = array(
                    'auteur' => $c->getUser()->getUsername(),
                    'date' => $c->getDate()->format('Y-m-d H:i:s'),
                    'contenu' => $c->getContenu(),
                );
            }
        } else {
            $commentaireArray = array();
        }

        return $this->jsonRemoveUnicodeSequences(json_encode($commentaireArray));
    }

    public function produitsEnPromo($start, $count) {
        $now = new \DateTime();
        $produitPromo = $this->em
                ->getRepository("CIELOEntrepriseBundle:PromotionProduit")
                ->createQueryBuilder("pp")
                ->leftJoin("pp.promotion", "p")
                ->where("p.dateDebutPromotion <= :now")
                ->where("p.dateFinPromotion >= :now")
                ->setParameter("now", $now)
                ->setFirstResult($start)
                ->setMaxResults($count)
                ->getQuery()
                ->getResult();
        if (count($produitPromo) > 0) {
            foreach ($produitPromo as $pp) {
                $produitPromoArray[] = array(
                    'promo' => $pp->getPromotion()->toJSON(true),
                    'produit' => $pp->getProduit()->toJSON(true),
                    'reduction' => $pp->getReduction(),
                );
            }
        } else {
            $produitPromoArray = array();
        }

        return $this->jsonRemoveUnicodeSequences(json_encode($produitPromoArray));
    }

    public function listeEvenement($start, $count) {
        $now = new \DateTime();
        $evenements = $this->em
                ->getRepository("CIELOAdminBundle:Evenement")
                ->createQueryBuilder("e")
                ->where("e.dateDebut <= :now")
                ->where("e.dateFin >= :now")
                ->setParameter("now", $now)
                ->orderBy("e.dateDebut", "DESC")
                ->setFirstResult($start)
                ->setMaxResults($count)
                ->getQuery()
                ->getResult();
        if (count($evenements) > 0) {
            foreach ($evenements as $e) {
                $listeEvenementArray[] = $e->toJSON(true);
            }
        } else {
            $listeEvenementArray = array();
        }

        return $this->jsonRemoveUnicodeSequences(json_encode($listeEvenementArray));
    }

    public function detailEvenement($id_user, $id) {
        $evenement = $this->em->getRepository("CIELOAdminBundle:Evenement")->find($id);
        $user = $this->em->getRepository("CIELOUserBundle:User")->find(intval($id_user));
        $participerEvenement = $this->em
                ->getRepository("CIELOAdminBundle:ParticipationEvenement")
                ->findOneBy(array(
            'utilisateur' => $user,
            'evenement' => $evenement
        ));
        if ($evenement != null && $user != null && $participerEvenement) {
            $evenementBrut = json_decode($evenement->toCompleteJSON(), true);
            $evenementComplete = array_merge($evenementBrut, array(
                'participation' => true
            ));
            return $this->jsonRemoveUnicodeSequences(json_encode($evenementComplete));
        } elseif ($evenement != null && $user != null && $participerEvenement == null) {
            $evenementBrut = json_decode($evenement->toCompleteJSON(), true);
            $evenementComplete = array_merge($evenementBrut, array(
                'participation' => false
            ));
            return $this->jsonRemoveUnicodeSequences(json_encode($evenementComplete));
        } elseif ($evenement != null && $id_user == null) {
            $evenementBrut = json_decode($evenement->toCompleteJSON(), true);
            $evenementComplete = array_merge($evenementBrut, array(
                'participation' => false
            ));
            return $this->jsonRemoveUnicodeSequences(json_encode($evenementComplete));
        } else {
            return null;
        }
    }

    public function commandeOptions() {
        $modeLivraison = $this->em->getrepository('CIELOAdminBundle:ModeLivraison')->findAll();
        $modePaiement = $this->em->getrepository('CIELOAdminBundle:ModePaiement')->findAll();
        $zone = $this->em->getrepository('CIELOAdminBundle:ZoneGeographique')->findAll();
        if (count($modeLivraison) > 0) {
            foreach ($modeLivraison as $ml) {
                $modeLivraisonArray[] = $ml->toJSON(true);
            }
        } else {
            $modeLivraisonArray = array();
        }
        if (count($modePaiement) > 0) {
            foreach ($modePaiement as $mp) {
                $modePaiementArray[] = $mp->toJSON(true);
            }
        } else {
            $modePaiementArray = array();
        }
        if (count($zone) > 0) {
            foreach ($zone as $z) {
                $zoneArray[] = $z->toJSON(true);
            }
        } else {
            $zoneArray = array();
        }

        $commandeOptions = array(
            'modePaiement' => $modePaiementArray,
            'modeLivraison' => $modeLivraisonArray,
            'zone' => $zoneArray
        );
        return $this->jsonRemoveUnicodeSequences(json_encode($commandeOptions));
    }

    public function tarifModeLivraisonZoneGeographique($id_mode_livraison, $id_zone_geographique) {
        $zone = $this->em
                ->getRepository('CIELOAdminBundle:ZoneGeographique')
                ->find(intval($id_zone_geographique));
        $modeLivraison = $this->em
                ->getRepository('CIELOAdminBundle:ModeLivraison')
                ->find(intval($id_mode_livraison));
        $modeLivraisonZoneGeographique = $this->em
                ->getRepository('CIELOAdminBundle:ModeLivraisonZoneGeographique')
                ->findOneBy(array(
            'modeLivraison' => $modeLivraison,
            'zoneGeographique' => $zone
        ));
        if ($modeLivraisonZoneGeographique) {
            return $modeLivraisonZoneGeographique->getPrix();
        } else {
            return "Non disponible";
        }
    }

    public function historiqueCommandeUser($id_user, $dateDebut, $dateFin) {
        $client = $this->em->getrepository('CIELOUserBundle:Client')->find(intval($id_user));
        //$historique = $this->em->getrepository('CIELOEcommerceBundle:Commande')->findByClient($client);
        if ($dateDebut == null && $dateFin == null) {
            $historisqueCommande = $this->em
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->findByClient($client);
        } elseif ($dateDebut != null && $dateFin != null) {
            $historisqueCommande = $this->em
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->createQueryBuilder("c")
                    ->leftJoin("c.client", "cl")
                    ->where("c.dateDebutCommande >= :debut")
                    ->andWhere("c.dateDebutCommande <= :fin")
                    ->andWhere("cl.id = :client_id")
                    ->setParameter("debut", $dateDebut)
                    ->setParameter("fin", $dateFin)
                    ->setParameter("client_id", $client->getId())
                    ->getQuery()
                    ->getResult();
        } elseif ($dateDebut == null && $dateFin != null) {
            $historisqueCommande = $this->em
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->createQueryBuilder("c")
                    ->leftJoin("c.client", "cl")
                    ->where("c.dateDebutCommande >= :debut")
                    ->andWhere("c.dateDebutCommande <= :fin")
                    ->andWhere("cl.id = :client_id")
                    ->setParameter("debut", $client->getDateCreationCompte())
                    ->setParameter("fin", $dateFin)
                    ->setParameter("client_id", $client->getId())
                    ->getQuery()
                    ->getResult();
        } elseif ($dateDebut != null && $dateFin == null) {
            $now = new \DateTime();
            $historisqueCommande = $this->em
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->createQueryBuilder("c")
                    ->leftJoin("c.client", "cl")
                    ->where("c.dateDebutCommande >= :debut")
                    ->andWhere("c.dateDebutCommande <= :fin")
                    ->andWhere("cl.id = :client_id")
                    ->setParameter("debut", $dateDebut)
                    ->setParameter("fin", $now)
                    ->setParameter("client_id", $client->getId())
                    ->getQuery()
                    ->getResult();
        }

        if (count($historisqueCommande) > 0) {
            foreach ($historisqueCommande as $h) {
                $historiqueArray[] = $h->toJSON(true);
            }
        } else {
            $historiqueArray = array();
        }
        return $this->jsonRemoveUnicodeSequences(json_encode($historiqueArray));
    }

    public function search($query) {
        $produitsRechercher = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->produitsRechercherService($query);
        $evenementsRechercher = $this->em->getRepository("CIELOAdminBundle:Evenement")->evenementsRechercherService($query);
        if (count($produitsRechercher) > 0) {
            foreach ($produitsRechercher as $pr) {
                $produitsRechercherArray[] = $pr->toJSON(true);
            }
        } else {
            $produitsRechercherArray = array();
        }
        if (count($evenementsRechercher) > 0) {
            foreach ($evenementsRechercher as $er) {
                $evenementsRechercherArray[] = $er->toJSON(true);
            }
        } else {
            $evenementsRechercherArray = array();
        }
        $resultSearch = array(
            'produits' => $produitsRechercherArray,
            'evenements' => $evenementsRechercherArray
        );
        return $this->jsonRemoveUnicodeSequences(json_encode($resultSearch));
    }

    public function prendreRDV($id_modele, $id_user) {
        $user = $this->em->getRepository("CIELOUserBundle:User")->find(intval($id_user));
        $modele = $this->em->getRepository("CIELOEntrepriseBundle:Modele")->find(intval($id_modele));
        if ($user != null && $modele != null) {
            $rdv = new RendezVous();
            $rdv->setUser($user);
            $rdv->setModele($modele);
            $rdv->setVue(false);
            $this->em->persist($rdv);
            $this->em->flush();
        }
    }

    public function mobileVersion($os) {
        if ($os == "Android") {
            return 1;
        } elseif ($os == "WindosPhone") {
            return 1;
        } elseif ($os == "BlackBerry") {
            return 1;
        }
    }

    public function promoSetting() {
        $now = new \DateTime();
        $allPromotion = $this->em->getRepository("CIELOEntrepriseBundle:Promotion")
                ->findAll();
        foreach ($allPromotion as $ap) {
            if ($ap->getDateDebutPromotion()->format("Y-m-d") == $now->format("Y-m-d")) {
                $allPromotionProduit = $ap->getPromotionProduits();
                foreach ($allPromotionProduit as $pp) {
                    $modeles = $pp->getProduit()->getModeles();
                    foreach ($modeles as $m) {
                        $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($pp->getReduction() / 100)));
                        $this->em->flush();
                    }
                }
            }
            $dateFinPromo = new DateTime($ap->getDateFinPromotion()->format("Y-m-d"));
            $dateFinPromo->modify("+1 day");
            if ($dateFinPromo->format("Y-m-d") == $now->format("Y-m-d")) {
                $allPromotionProduit = $ap->getPromotionProduits();
                foreach ($allPromotionProduit as $pp) {
                    $modeles = $pp->getProduit()->getModeles();
                    foreach ($modeles as $m) {
                        $m->setPrixPromo($m->getPrix());
                        $this->em->flush();
                    }
                }
            }
        }
    }

    private function jsonRemoveUnicodeSequences($struct) {
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $struct);
    }

}
