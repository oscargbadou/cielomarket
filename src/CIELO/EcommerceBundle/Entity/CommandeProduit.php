<?php

namespace CIELO\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommandeProduit
 *
 * @ORM\Table(name="cielo_commande_modele")
 * @ORM\Entity(repositoryClass="CIELO\EcommerceBundle\Entity\CommandeProduitRepository")
 */
class CommandeProduit
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="CIELO\EcommerceBundle\Entity\Commande", inversedBy="commandeProduits")
     */
    private $commande;
    
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Modele", inversedBy="commandeProduits")
     */
    private $modele;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * Set quantite
     *
     * @param integer $quantite
     * @return CommandeProduit
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    
        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer 
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set commande
     *
     * @param \CIELO\EcommerceBundle\Entity\Commande $commande
     * @return CommandeProduit
     */
    public function setCommande(\CIELO\EcommerceBundle\Entity\Commande $commande)
    {
        $this->commande = $commande;
    
        return $this;
    }

    /**
     * Get commande
     *
     * @return \CIELO\EcommerceBundle\Entity\Commande 
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return CommandeProduit
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit)
    {
        $this->produit = $produit;
    
        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set modele
     *
     * @param \CIELO\EntrepriseBundle\Entity\Modele $modele
     * @return CommandeProduit
     */
    public function setModele(\CIELO\EntrepriseBundle\Entity\Modele $modele)
    {
        $this->modele = $modele;
    
        return $this;
    }

    /**
     * Get modele
     *
     * @return \CIELO\EntrepriseBundle\Entity\Modele 
     */
    public function getModele()
    {
        return $this->modele;
    }
}