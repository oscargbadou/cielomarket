<?php

namespace CIELO\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="cielo_commande")
 * @ORM\Entity(repositoryClass="CIELO\EcommerceBundle\Entity\CommandeRepository")
 */
class Commande
{
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EcommerceBundle\Entity\CommandeProduit", mappedBy="commande", cascade={"remove","persist"})
     */
    private $commandeProduits;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\UserBundle\Entity\Client")
     */
    private $client;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ModePaiement", inversedBy="modePaiements")
     */
    private $modePaiement;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ModeLivraison")
     */
    private $modeLivraison;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebutCommande", type="datetime")
     */
    private $dateDebutCommande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFinCommande", type="datetime", nullable=true)
     */
    private $dateFinCommande;

    /**
     * @var boolean
     *
     * @ORM\Column(name="commandeTerminer", type="boolean")
     */
    private $commandeTerminer;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="vue", type="boolean")
     */
    private $vue;
    
    /**
     * @var string
     *
     * @ORM\Column(name="zone", type="string", length=255, nullable=true)
     */
    private $zone;
    
    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contenuFacture", type="string", length=255, nullable=true)
     */
    private $contenuFacture;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="montantFacture", type="integer")
     */
    private $montantFacture;
    
    /**
     * @var string
     *
     * @ORM\Column(name="codeFacture", type="string", length=255, nullable=true)
     */
    private $codeFacture;

    public function __construct() {
        $this->dateDebutCommande=new \DateTime();
        $this->commandeTerminer=false;
        $this->vue=false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebutCommande
     *
     * @param \DateTime $dateDebutCommande
     * @return Commande
     */
    public function setDateDebutCommande($dateDebutCommande)
    {
        $this->dateDebutCommande = $dateDebutCommande;
    
        return $this;
    }

    /**
     * Get dateDebutCommande
     *
     * @return \DateTime 
     */
    public function getDateDebutCommande()
    {
        return $this->dateDebutCommande;
    }

    /**
     * Set dateFinCommande
     *
     * @param \DateTime $dateFinCommande
     * @return Commande
     */
    public function setDateFinCommande($dateFinCommande)
    {
        $this->dateFinCommande = $dateFinCommande;
    
        return $this;
    }

    /**
     * Get dateFinCommande
     *
     * @return \DateTime 
     */
    public function getDateFinCommande()
    {
        return $this->dateFinCommande;
    }

    /**
     * Set commandeTerminer
     *
     * @param boolean $commandeTerminer
     * @return Commande
     */
    public function setCommandeTerminer($commandeTerminer)
    {
        $this->commandeTerminer = $commandeTerminer;
    
        return $this;
    }

    /**
     * Get commandeTerminer
     *
     * @return boolean 
     */
    public function getCommandeTerminer()
    {
        return $this->commandeTerminer;
    }

    /**
     * Set client
     *
     * @param \CIELO\UserBundle\Entity\Client $client
     * @return Commande
     */
    public function setClient(\CIELO\UserBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \CIELO\UserBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set zone
     *
     * @param string $zone
     * @return Commande
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    
        return $this;
    }

    /**
     * Get zone
     *
     * @return string 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Commande
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set modePaiement
     *
     * @param \CIELO\AdminBundle\Entity\ModePaiement $modePaiement
     * @return Commande
     */
    public function setModePaiement(\CIELO\AdminBundle\Entity\ModePaiement $modePaiement = null)
    {
        $this->modePaiement = $modePaiement;
    
        return $this;
    }

    /**
     * Get modePaiement
     *
     * @return \CIELO\AdminBundle\Entity\ModePaiement 
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    /**
     * Set modeLivraison
     *
     * @param \CIELO\AdminBundle\Entity\ModeLivraison $modeLivraison
     * @return Commande
     */
    public function setModeLivraison(\CIELO\AdminBundle\Entity\ModeLivraison $modeLivraison = null)
    {
        $this->modeLivraison = $modeLivraison;
    
        return $this;
    }

    /**
     * Get modeLivraison
     *
     * @return \CIELO\AdminBundle\Entity\ModeLivraison 
     */
    public function getModeLivraison()
    {
        return $this->modeLivraison;
    }

    /**
     * Set contenuFacture
     *
     * @param string $contenuFacture
     * @return Commande
     */
    public function setContenuFacture($contenuFacture)
    {
        $this->contenuFacture = $contenuFacture;
    
        return $this;
    }

    /**
     * Get contenuFacture
     *
     * @return string 
     */
    public function getContenuFacture()
    {
        return $this->contenuFacture;
    }

    /**
     * Set montantFacture
     *
     * @param integer $montantFacture
     * @return Commande
     */
    public function setMontantFacture($montantFacture)
    {
        $this->montantFacture = $montantFacture;
    
        return $this;
    }

    /**
     * Get montantFacture
     *
     * @return integer 
     */
    public function getMontantFacture()
    {
        return $this->montantFacture;
    }

    /**
     * Set codeFacture
     *
     * @param string $codeFacture
     * @return Commande
     */
    public function setCodeFacture($codeFacture)
    {
        $this->codeFacture = $codeFacture;
    
        return $this;
    }

    /**
     * Get codeFacture
     *
     * @return string 
     */
    public function getCodeFacture()
    {
        return $this->codeFacture;
    }

    /**
     * Set vue
     *
     * @param boolean $vue
     * @return Commande
     */
    public function setVue($vue)
    {
        $this->vue = $vue;
    
        return $this;
    }

    /**
     * Get vue
     *
     * @return boolean 
     */
    public function getVue()
    {
        return $this->vue;
    }

    /**
     * Add commandeProduits
     *
     * @param \CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits
     * @return Commande
     */
    public function addCommandeProduit(\CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits)
    {
        $this->commandeProduits[] = $commandeProduits;
    
        return $this;
    }

    /**
     * Remove commandeProduits
     *
     * @param \CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits
     */
    public function removeCommandeProduit(\CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits)
    {
        $this->commandeProduits->removeElement($commandeProduits);
    }

    /**
     * Get commandeProduits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommandeProduits()
    {
        return $this->commandeProduits;
    }
    
     public function toJSON($toArray = false) {
        $array = Array(
            "adresse" => $this->adresse,
            "date"=>$this->dateDebutCommande->format('Y-m-d H:i:s'),
            "facture"=>$this->contenuFacture,
            "numeroFacture"=>$this->codeFacture,
            "montantFacture"=>$this->montantFacture,
            "modeLivraison"=>$this->modeLivraison->toJSON(true),
            "modePaiement"=>$this->modePaiement->toJSON(true),
            "zone"=>$this->zone,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }
}