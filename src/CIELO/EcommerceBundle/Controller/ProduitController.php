<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\CommentaireProduit;
use CIELO\EntrepriseBundle\Entity\RendezVous;

class ProduitController extends Controller {

    public function templateRender() {
        $domaineActivites = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")
                ->findAll();
        $familles = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Famille")
                ->findAll();
        $entreprises = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->findAll();
        $dom = array();
        $fam = array();
        $ent = array();
        if (count($domaineActivites) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($dom, $domaineActivites[$i]);
            }
        } else {
            $dom = $domaineActivites;
        }

        if (count($familles) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($fam, $familles[$i]);
            }
        } else {
            $fam = $familles;
        }

        if (count($entreprises) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($ent, $entreprises[$i]);
            }
        } else {
            $ent = $entreprises;
        }
        return array(
            'domaineActivites' => $dom,
            'familles' => $fam,
            'entreprises' => $ent,
            'familless' => $familles,
        );
    }

    /**
     * @Secure(roles="ROLE_USER")
     */
    public function prendreRDVAction($id){
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $modele = $em->getRepository("CIELOEntrepriseBundle:Modele")->find(intval($id));
        $rdv = new RendezVous();
        $rdv->setUser($currentUser);
        $rdv->setModele($modele);
        $rdv->setVue(false);
        $em->persist($rdv);
        $em->flush();
        return $this->render('CIELOEcommerceBundle:Ecommerce:rdv.html.twig');
    }
    
    public function famillesAction() {
        $menuContent = FamilleController::templateRender();
        return $this->render('CIELOEcommerceBundle:Ecommerce:familles.html.twig', $menuContent);
    }

    public function detailProduitAction($id) {
        $produit = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->find($id);
        $commentaires = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:CommentaireProduit")
                ->createQueryBuilder("c")
                ->leftJoin("c.produit", "p")
                ->where("p.id = :idproduit")
                ->orderBy("c.date", "DESC")
                ->setParameter("idproduit", $id)
                ->setFirstResult(0)
                ->setMaxResults(10)
                ->getQuery()
                ->getResult();
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $commentaireProduit = new CommentaireProduit();
        $form = $this->createFormBuilder($commentaireProduit)
                ->add('contenu', 'textarea')
                ->getForm();
        $request = $this->get('request');
        $noteProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:NoteProduitUser")
                ->noteProduit($id);
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid() && $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $commentaireProduit->setUser($currentUser);
                $commentaireProduit->setProduit($produit);
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($commentaireProduit);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_user_detail_produit', array(
                                    'id' => $id
                )));
            }
        }
        $noteProduitUser = $this->getDoctrine()->getManager()
                ->getRepository("CIELOEntrepriseBundle:NoteProduitUser")
                ->findOneBy(array(
            'user' => $currentUser,
            'produit' => $produit
        ));
        return $this->render('CIELOEcommerceBundle:Ecommerce:detailProduit.html.twig', array(
                    'produit' => $produit,
                    'form' => $form->createView(),
                    'commentaires' => $commentaires,
                    'noteProduit' => ($noteProduit > 0) ? $noteProduit : 0,
                    'noteProduitUser' => ($noteProduit > 0) ? $noteProduit : 3
        ));
    }

    /**
     * @Secure(roles="IS_AUTHENTICATED_REMEMBERED")
     */
    public function commenterProduitAction($id) {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $produit = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->find($id);
        $commentaireProduit = new CommentaireProduit();
        $form = $this->createFormBuilder($commentaireProduit)
                ->add('contenu', 'textarea')
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid() && $currentUser != null) {
                $commentaireProduit->setUser($currentUser);
                $commentaireProduit->setProduit($produit);
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($commentaireProduit);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_user_detail_produit', array(
                                    'id' => $id
                )));
            } else {
                return $this->redirect($this->generateUrl('cielo_user_homepage'));
            }
        }
        return $this->render('CIELOEcommerceBundle:Ecommerce:detailProduit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function produitDuneFamilleAction($id, $page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierProduit = ($page - 1) * 16;
        $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitFamille($id);
        $totalPages = ceil(intval($totalProduits) / 16);
        $produitFamille = $em
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->createQueryBuilder("p")
                ->leftJoin("p.categorie", "c")
                ->leftJoin("c.famille", "f")
                ->where("f.id = :id_famille")
                ->setParameter("id_famille", $id)
                ->setFirstResult($premierProduit)
                ->setMaxResults(16)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOEcommerceBundle:Ecommerce:produitsDuneFamille.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'produitFamille' => $produitFamille,
                    'id' => $id
        ));
    }

    public function produitDuneCategorieAction($id, $page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierProduit = ($page - 1) * 16;
        $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitCategorie($id);
        $totalPages = ceil(intval($totalProduits) / 16);
        $produitCategorie = $em
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->createQueryBuilder("p")
                ->leftJoin("p.categorie", 'c')
                ->orderBy("p.purchased", "DESC")
                ->where("c.id = :id_categorie")
                ->setParameter("id_categorie", $id)
                ->setFirstResult($premierProduit)
                ->setMaxResults(16)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOEcommerceBundle:Ecommerce:produitsDuneCategorie.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'produitCategorie' => $produitCategorie,
                    'id' => $id
        ));
    }

    public function nouveauxProduitsAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierProduit = ($page - 1) * 16;
        $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduit();
        $totalPages = ceil(intval($totalProduits) / 16);
        $nouveauxProduits = $em
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->findBy(array(), array("dateAjout" => "DESC"), 16, $premierProduit);
        return $this->render('CIELOEcommerceBundle:Ecommerce:nouveauxProduits.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'nouveauxProduits' => $nouveauxProduits
        ));
    }

    public function rechercheProduitAction($page) {
        $searchContent = $this->get('request')->request->get('searchContent');
        $searchType = $this->get('request')->request->get('searchType');
        $em = $this->getDoctrine()
                ->getManager();
        switch ($searchType) {
            case 'produit':
                $premierProduit = ($page - 1) * 16;
                $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitRechercher($searchContent);
                $totalPages = ceil(intval($totalProduits) / 16);
                $rechercheProduit = $em
                        ->getRepository("CIELOEntrepriseBundle:Produit")
                        ->createQueryBuilder("p")
                        ->leftJoin("p.categorie", "c")
                        ->where("p.nom LIKE :searchContent OR c.nom LIKE :searchContent")
                        ->setParameter("searchContent", '%' . $searchContent . '%')
                        ->setFirstResult($premierProduit)
                        ->setMaxResults(16)
                        ->getQuery()
                        ->getResult();
                return $this->render('CIELOEcommerceBundle:Ecommerce:rechercheProduit.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'rechercheProduit' => $rechercheProduit,
                            'totalProduits' => $totalProduits
                ));
                break;
            case 'evenement':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOAdminBundle:Evenement")->nbreEvenementRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOAdminBundle:Evenement")->evenementsRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOEcommerceBundle:Ecommerce:evenementsRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
        }
    }

    public function produitPlusAcheterAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierProduit = ($page - 1) * 16;
        $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduit();
        $totalPages = ceil(intval($totalProduits) / 16);
        $produitsPlusAchetes = $em
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->findBy(array(), array("purchased" => "DESC"), 16, $premierProduit);
        return $this->render('CIELOEcommerceBundle:Ecommerce:produitPlusAcheter.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'produitsPlusAchetes' => $produitsPlusAchetes
        ));
    }

}

?>
