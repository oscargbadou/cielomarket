<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\DomaineActivite;
use Doctrine\ORM\EntityRepository;

class PromotionController extends Controller {

    public function promotionsAction($page) {
        $now = new \DateTime();
        $em = $this->getDoctrine()
                ->getManager();
        $premierProduit = ($page - 1) * 16;
        $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Promotion")->nbreProduit();
        $totalPages = ceil(intval($totalProduits) / 16);
        $produitPromo = $em
                ->getRepository("CIELOEntrepriseBundle:PromotionProduit")
                ->createQueryBuilder("pp")
                ->leftJoin("pp.promotion", "p")
                ->where("p.dateDebutPromotion <= :now")
                ->andWhere("p.dateFinPromotion >= :now")
                ->setParameter("now", $now)
                ->setFirstResult($premierProduit)
                ->setMaxResults(16)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOEcommerceBundle:Ecommerce:promotions.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'produitsEnPromo' => $produitPromo
        ));
    }

}

?>
