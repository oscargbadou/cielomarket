<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EcommerceBundle\Entity\Commande;
use CIELO\EcommerceBundle\Form\CommandeType;
use Doctrine\ORM\EntityRepository;

class CommandeController extends Controller {

    /**
     * @Secure(roles="ROLE_USER")
     */
    public function lancerCommandeAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $newCommande = new Commande();
        $form = $this->createForm(new CommandeType, $newCommande);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $newCommande->setClient($currentUser);
                $zone = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('CIELOAdminBundle:ZoneGeographique')
                        ->find($newCommande->getZone());
                $newCommande->setZone($zone->getNom());
                $em->persist($newCommande);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_commande_panier'));
            }
        }
        return $this->render('CIELOEcommerceBundle:Ecommerce:lancerCommande.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_USER")
     */
    public function historiqueCommandeAction() {
        $dateDebut = $this->get('request')->request->get('debut');
        $dateFin = $this->get('request')->request->get('fin');
        $currentUser = $this->get("security.context")->getToken()->getUser();
        if ($dateDebut == null && $dateFin == null) {
            $historisqueCommande = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->findByClient($currentUser);
            $montantTotalHistorique = 0;
            foreach ($historisqueCommande as $h) {
                $montantTotalHistorique+=$h->getMontantFacture();
            }
            return $this->render('CIELOEcommerceBundle:Ecommerce:historiqueCommande.html.twig', array(
                        'historiqueCommande' => $historisqueCommande,
                        'montantTotalHistorique' => $montantTotalHistorique,
                        'periode' => "tout"
            ));
        } elseif ($dateDebut != null && $dateFin != null) {
            $historisqueCommande = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->createQueryBuilder("c")
                    ->leftJoin("c.client", "cl")
                    ->where("c.dateDebutCommande >= :debut")
                    ->andWhere("c.dateDebutCommande <= :fin")
                    ->andWhere("cl.id = :client_id")
                    ->setParameter("debut", $dateDebut)
                    ->setParameter("fin", $dateFin)
                    ->setParameter("client_id", $currentUser->getId())
                    ->getQuery()
                    ->getResult();
            $montantTotalHistorique = 0;
            foreach ($historisqueCommande as $h) {
                $montantTotalHistorique+=$h->getMontantFacture();
            }
            return $this->render('CIELOEcommerceBundle:Ecommerce:historiqueCommande.html.twig', array(
                        'historiqueCommande' => $historisqueCommande,
                        'montantTotalHistorique' => $montantTotalHistorique,
                        'periode' => array(
                            'debut'=>$dateDebut,
                            'fin'=>$dateFin
                        )
            ));
        }elseif ($dateDebut == null && $dateFin != null) {
            $historisqueCommande = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->createQueryBuilder("c")
                    ->leftJoin("c.client", "cl")
                    ->where("c.dateDebutCommande >= :debut")
                    ->andWhere("c.dateDebutCommande <= :fin")
                    ->andWhere("cl.id = :client_id")
                    ->setParameter("debut", $currentUser->getDateCreationCompte())
                    ->setParameter("fin", $dateFin)
                    ->setParameter("client_id", $currentUser->getId())
                    ->getQuery()
                    ->getResult();
            $montantTotalHistorique = 0;
            foreach ($historisqueCommande as $h) {
                $montantTotalHistorique+=$h->getMontantFacture();
            }
            return $this->render('CIELOEcommerceBundle:Ecommerce:historiqueCommande.html.twig', array(
                        'historiqueCommande' => $historisqueCommande,
                        'montantTotalHistorique' => $montantTotalHistorique,
                        'periode' => array(
                            'debut'=>$currentUser->getDateCreationCompte(),
                            'fin'=>$dateFin
                        )
            ));
        }elseif ($dateDebut != null && $dateFin == null) {
            $now = new \DateTime();
            $historisqueCommande = $this->getDoctrine()
                    ->getEntityManager()
                    ->getRepository("CIELOEcommerceBundle:Commande")
                    ->createQueryBuilder("c")
                    ->leftJoin("c.client", "cl")
                    ->where("c.dateDebutCommande >= :debut")
                    ->andWhere("c.dateDebutCommande <= :fin")
                    ->andWhere("cl.id = :client_id")
                    ->setParameter("debut", $dateDebut)
                    ->setParameter("fin", $now)
                    ->setParameter("client_id", $currentUser->getId())
                    ->getQuery()
                    ->getResult();
            $montantTotalHistorique = 0;
            foreach ($historisqueCommande as $h) {
                $montantTotalHistorique+=$h->getMontantFacture();
            }
            return $this->render('CIELOEcommerceBundle:Ecommerce:historiqueCommande.html.twig', array(
                        'historiqueCommande' => $historisqueCommande,
                        'montantTotalHistorique' => $montantTotalHistorique,
                        'periode' => array(
                            'debut'=>$dateDebut,
                            'fin'=>$now
                        )
            ));
        }
    }

    public function modeLivraisonAction() {
        $modeLivraison = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository("CIELOAdminBundle:ModeLivraison")
                ->findAll();
        return $this->render('CIELOEcommerceBundle:Ecommerce:modeLivraison.html.twig', array(
                    'modeLivraison' => $modeLivraison
        ));
    }

    public function modePaiementAction() {
        $modePaiement = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository("CIELOAdminBundle:ModePaiement")
                ->findAll();
        return $this->render('CIELOEcommerceBundle:Ecommerce:modePaiement.html.twig', array(
                    'modePaiement' => $modePaiement
        ));
    }

}

?>
