<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CIELO\EntrepriseBundle\Entity\PromotionProduit;

class ServiceController extends Controller {

    public function imageAction($id) {
        $em = $this->getDoctrine()->getManager();
        $image = $em->getRepository("CIELOEntrepriseBundle:Document")->find($id);
        if ($image == null) {
            throw new \Symfony\Component\Translation\Exception\NotFoundResourceException("image non trouvée");
        }
        $request = $this->getRequest();
        $w = $request->get("w");
        $h = $request->get("h");
        $bgColor = $request->get('bgcolor');

        if ($bgColor == null)
            $link = $this->get('image.handling')->open($image->getAbsolutePath())->resize($w, $h)->__toString();
        else
            $link = $this->get('image.handling')->open($image->getAbsolutePath())->resize($w, $h, $bgColor)->__toString();

        $response = new Response(file_get_contents("https://" . $_SERVER["HTTP_HOST"] . "" . $link));
        $response->headers->add(array("content-type" => "image/jpeg"));
        return $response;
    }

    public function loginAction() {
        $request = $this->getRequest();
        $username = $request->request->get("username");
        $password = $request->request->get("password");

        $user = $this->get('cielo_market_service')->login($username, $password);
        if ($user === null)
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(403, "Utilisateur non loggé");
        else {
            $response = new Response();
            $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
            $response->setContent($user->getId());
            return $response;
        }
    }

    public function produitsCritereAction() {
        $request = $this->getRequest();
        $criteria = $request->get("criteria");
        $start = $request->get("start");
        $count = $request->get("count");
        $produits = $this->get('cielo_market_service')->produitsCritere($criteria, $start, $count);
        if ($produits === null)
            $produits = "[]";
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($produits);
        return $response;
    }

    public function detailProduitAction() {
        $request = $this->getRequest();
        $id = $request->get("id");
        $detailProduit = $this->get('cielo_market_service')->detailProduit($id);

        if ($detailProduit === null) {
            throw new NotFoundHttpException("Produit non trouvé");
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($detailProduit);
        return $response;
    }

    public function detailFamilleAction() {
        $request = $this->getRequest();
        $id = $request->get("id");
        $detailFamille = $this->get('cielo_market_service')->detailFamille($id);
        if ($detailFamille === null) {
            throw new NotFoundHttpException("Famille non trouvée");
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($detailFamille);
        return $response;
    }

    public function detailEntrepriseAction() {
        $request = $this->getRequest();
        $id = $request->get("id");
        $detailEntreprise = $this->get('cielo_market_service')->detailEntreprise($id);
        if ($detailEntreprise === null) {
            throw new NotFoundHttpException("Entreprise non trouvée");
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($detailEntreprise);
        return $response;
    }

    public function familleEntrepriseAction() {
        $request = $this->getRequest();
        $entr_id = $request->get("entr_id");
        $start = $request->get("start");
        $count = $request->get("count");
        $familleProduitEntreprise = $this->get('cielo_market_service')->familleEntreprise($entr_id, $start, $count);
        if ($familleProduitEntreprise === null) {
            $familleProduitEntreprise = "[]";
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($familleProduitEntreprise);
        return $response;
    }

    public function produitFamilleEntrepriseAction() {
        $request = $this->getRequest();
        $entr_id = $request->get("entr_id");
        $start = $request->get("start");
        $count = $request->get("count");
        $fam_id = $request->get("fam_id");
        $categorie = $request->get("categorie");
        $produits = $this->get('cielo_market_service')->produitFamilleEntreprise($entr_id, $fam_id, $categorie, $start, $count);
        if ($produits === null) {
            $produits = "[]";
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($produits);
        return $response;
    }

    public function categorieEntrepriseAction() {
        $request = $this->getRequest();
        $entr_id = $request->get("entr_id");
        $categorieEntreprise = $this->get('cielo_market_service')->categorieEntreprise($entr_id);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($categorieEntreprise);
        return $response;
    }

    public function campagneJeuAction() {
        $request = $this->getRequest();
        $start = $request->get("start");
        $count = $request->get("count");
        $campagneJeu = $this->get('cielo_market_service')->campagneJeu($start, $count);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($campagneJeu);
        return $response;
    }

    public function campagneJeuEntrepriseAction() {
        $request = $this->getRequest();
        $entr_id = $request->get("entr_id");
        $campagneEntreprise = $this->get('cielo_market_service')->campagneJeuEntreprise($entr_id);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($campagneEntreprise);
        return $response;
    }

    public function commandeOptionsAction() {
        $commandeOptions = $this->get('cielo_market_service')->commandeOptions();
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($commandeOptions);
        return $response;
    }

    public function produitFamilleAction() {
        $request = $this->getRequest();
        $start = $request->get("start");
        $criteria = $request->get("criteria");
        $count = $request->get("count");
        $fam_id = $request->get("fam_id");
        $produits = $this->get('cielo_market_service')->produitFamille($fam_id, $criteria, $start, $count);
        if ($produits === null) {
            $produits = "[]";
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($produits);
        return $response;
    }

    public function commentaireProduitAction() {
        $request = $this->getRequest();
        $start = $request->get("start");
        $count = $request->get("count");
        $produit_id = $request->get("produit_id");
        $commentaires = $this->get('cielo_market_service')->commentaireProduit($produit_id, $start, $count);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($commentaires);
        return $response;
    }

    public function listeEvenementAction() {
        $request = $this->getRequest();
        $start = $request->get("start");
        $count = $request->get("count");
        $evenements = $this->get('cielo_market_service')->listeEvenement($start, $count);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($evenements);
        return $response;
    }

    public function enregistrerCommandeAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $id_user = $request->get("id_user");
            $panierInJSON = $request->get("panier");
            $panierInJSON = stripslashes($panierInJSON);
            //var_dump($panierInJSON);
            $panierInArray = json_decode($panierInJSON, true);
            //var_dump($panierInArray);
            //die('');
            $id_modelivraison = $request->get("id_modelivraison");
            $id_modepaiement = $request->get("id_modepaiement");
            $id_zone = $request->get("id_zone");
            $adresse = $request->get("adresse");
            $type = $request->get("type");
            $client = $this->getDoctrine()->getManager()->getRepository("CIELOUserBundle:Client")->find(intval($id_user));

            if ($id_user != null && $panierInJSON != null) {
                if ($type == "facture") {
                    $result = $this->get("cielo_market_service")->enregistrerCommande($id_user, $id_modelivraison, $id_modepaiement, $id_zone, $adresse, $type, $panierInArray);
                    $response = new Response();
                    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
                    $response->setContent($result);
                    return $response;
                } else {
                    $result = $this->get("cielo_market_service")->enregistrerCommande($id_user, $id_modelivraison, $id_modepaiement, $id_zone, $adresse, $type, $panierInArray);
                    $resultArray = json_decode($result, true);
                    $response = new Response();
                    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
                    $response->setContent($result);
                    $message = \Swift_Message::newInstance()
                            ->setSubject('Facture pour achat sur CieloMarket')
                            ->setFrom('admin@cielo-market.com')
                            ->setTo($client->getEmail())
                            ->setBody($this->renderView('CIELOEcommerceBundle:Ecommerce:emailFacture.txt.twig', array(
                                'client' => $client->getUsername(),
                                'contenuFacture' => $resultArray["msgFacture"],
                                'codeFacture' => $resultArray["codeFacture"]
                    )));
                    $this->get('mailer')->send($message);
                    $admin = $this->getDoctrine()->getManager()->getRepository("CIELOUserBundle:Admin")->findAll();
                    foreach ($admin as $a) {
                        $message = \Swift_Message::newInstance()
                                ->setSubject('Facture pour achat sur CieloMarket')
                                ->setFrom('admin@cielo-market.com')
                                ->setTo($a->getEmail())
                                ->setBody($this->renderView('CIELOEcommerceBundle:Ecommerce:emailFactureAdmin.txt.twig', array(
                                    'client' => $client->getNom() . " " . $client->getPrenom(),
                                    'contenuFacture' => $resultArray["msgFacture"],
                                    'codeFacture' => $resultArray["codeFacture"]
                        )));
                        $this->get('mailer')->send($message);
                    }
                    return $response;
                }
            } else {
                throw new NotFoundHttpException("Parametres non envoyer");
            }
        }
        throw new NotFoundHttpException("Erreur données");
    }

    public function commenterProduitAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $id_auteur = $request->get("id_auteur");
            $id_produit = $request->get("id_produit");
            $contenu = $request->get("contenu");
            if ($id_auteur != null && $id_produit != null && $contenu != null) {
                $this->get("cielo_market_service")->commenterProduit($id_auteur, $id_produit, $contenu);
                return new Response('', 204, array('Access-Control-Allow-Origin' => '*'));
            } else {
                throw new NotFoundHttpException("Erreur données");
            }
        }
        throw new NotFoundHttpException("Erreur données");
    }

    public function participerEvenementAction() {
        $request = $this->getRequest();
        $id_user = $request->get("id_user");
        $id_evenement = $request->get("id_evenement");
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        if ($id_user != null && $id_evenement != null) {
            $result = $this->get("cielo_market_service")->participerEvenement($id_user, $id_evenement);
            $response->setContent($result);
            return $response;
        } else {
            $response->setContent("vous devez etre connecté");
            return $response;
        }
        throw new NotFoundHttpException("Erreur données");
    }

    public function prendreRDVAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $id_modele = $request->get("id_modele");
            $id_user = $request->get("id_user");
            if ($id_modele != null && $id_user != null) {
                $this->get("cielo_market_service")->prendreRDV($id_modele, $id_user);
                return new Response('', 204, array('Access-Control-Allow-Origin' => '*'));
            } else {
                throw new NotFoundHttpException("Erreur données");
            }
        }
        throw new NotFoundHttpException("Erreur données");
    }

    public function noterProduitAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $id_auteur = $request->get("id_auteur");
            $id_produit = $request->get("id_produit");
            $note = $request->get("note");
            if ($id_produit != null && $note != null) {
                $this->get("cielo_market_service")->noterProduit($id_auteur, $id_produit, $note);
                return new Response('', 204, array('Access-Control-Allow-Origin' => '*'));
            } else {
                throw new NotFoundHttpException("Erreur données");
            }
        }
        throw new NotFoundHttpException("Erreur données");
    }

    public function produitsEnPromoAction() {
        $request = $this->getRequest();
        $start = $request->get("start");
        $count = $request->get("count");
        $produitEnPromo = $this->get('cielo_market_service')->produitsEnPromo($start, $count);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($produitEnPromo);
        return $response;
    }

    public function detailEvenementAction() {
        $request = $this->getRequest();
        $id = $request->get("id");
        $id_user = $request->get("id_user");
        $detailEvenement = $this->get('cielo_market_service')->detailEvenement($id_user, $id);

        if ($detailEvenement === null) {
            throw new NotFoundHttpException("Evenement non trouvé");
        }
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($detailEvenement);
        return $response;
    }

    public function ajouterProduitPromoAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $idPromo = $request->get("idPromo");
            $produits = json_decode($request->get("produits"), true);
            $promotion = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('CIELOEntrepriseBundle:Promotion')
                    ->find(intval($idPromo));
            foreach ($produits as $i => $produit) {
                $p = $this->getDoctrine()->getManager()->getRepository("CIELOEntrepriseBundle:Produit")->find(intval($produit["id"]));
                $pomtionProduitExistant = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("CIELOEntrepriseBundle:PromotionProduit")
                        ->findOneBy(array(
                    'promotion' => $promotion,
                    'produit' => $p
                ));
                if ($pomtionProduitExistant) {
                    $pomtionProduitExistant->setPrixPromo(intval($produit["prixPromo"]));
                    $p->setPrixPromo(intval($produit["prixPromo"]));
                } else {
                    $pomtionProduit[$i] = new PromotionProduit();
                    $pomtionProduit[$i]->setPromotion($promotion);
                    $pomtionProduit[$i]->setProduit($p);
                    $pomtionProduit[$i]->setPrixPromo(intval($produit["prixPromo"]));
                    $p->setPrixPromo(intval($produit["prixPromo"]));
                    $this->getDoctrine()->getManager()->persist($pomtionProduit[$i]);
                }
            }
            $this->getDoctrine()->getManager()->flush();
            return new Response('', 204, array('Access-Control-Allow-Origin' => '*'));
        }
        throw new NotFoundHttpException("Erreur données");
    }

    public function tarifModeLivraisonZoneGeographiqueAction() {
        $request = $this->getRequest();
        $id_mode_livraison = $request->get("id_mode_livraison");
        $id_zone_geographique = $request->get("id_zone_geographique");
        $prix = $this->get('cielo_market_service')->tarifModeLivraisonZoneGeographique($id_mode_livraison, $id_zone_geographique);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($prix);
        return $response;
    }

    public function historiqueCommandeUserAction() {
        $request = $this->getRequest();
        $id_user = $request->get("id_user");
        $dateDebut = $request->get("date_debut");
        $dateFin = $request->get("date_fin");
        $historique = $this->get('cielo_market_service')->historiqueCommandeUser($id_user, $dateDebut, $dateFin);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($historique);
        return $response;
    }

    public function searchAction() {
        $request = $this->getRequest();
        $query = $request->get("query");
        $result = $this->get('cielo_market_service')->search($query);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($result);
        return $response;
    }

    public function mobileVersionAction() {
        $request = $this->getRequest();
        $os = $request->get("os");
        $result = $this->get('cielo_market_service')->mobileVersion($os);
        $response = new Response();
        $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
        $response->setContent($result);
        return $response;
    }

    public function promoSettingAction() {
        $this->get("cielo_market_service")->promoSetting();
        return new Response('', 204, array('Access-Control-Allow-Origin' => '*'));
    }

}
