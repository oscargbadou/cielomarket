<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class EvenementController extends Controller {

    public function evenementsAction($page) {
        $now = new \DateTime();        
        $em = $this->getDoctrine()
                ->getManager();
        $premierEvenement = ($page - 1) * 20;
        $totalEvenements = $em->getRepository("CIELOAdminBundle:Evenement")->nbreEvenementFuturOuAVenir();
        $totalPages = ceil(intval($totalEvenements) / 20);
        $evenementsFutur = $em
                ->getRepository("CIELOAdminBundle:Evenement")
                ->createQueryBuilder("e")
                ->where("e.dateDebut <= :now")
                ->where("e.dateFin >= :now")
                ->setParameter("now", $now)
                ->setFirstResult($premierEvenement)
                ->setMaxResults(20)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOEcommerceBundle:Ecommerce:evenements.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'evenementsFutur' => $evenementsFutur
        ));
    }

    public function evenementsPasserAction($page) {
        $now = new \DateTime();
        $em = $this->getDoctrine()
                ->getManager();
        $premierEvenement = ($page - 1) * 20;
        $totalEvenements = $em->getRepository("CIELOAdminBundle:Evenement")->nbreEvenementPasser();
        $totalPages = ceil(intval($totalEvenements) / 20);
        $evenementsPasse = $em
                ->getRepository("CIELOAdminBundle:Evenement")
                ->createQueryBuilder("e")
                ->where("e.dateFin <= :now")
                ->setParameter("now", $now)
                ->setFirstResult($premierEvenement)
                ->setMaxResults(20)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOEcommerceBundle:Ecommerce:evenementsPasser.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'evenementsPasse' => $evenementsPasse
        ));
    }

    public function detailEvenementAction($id) {
        $evenement = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOAdminBundle:Evenement")
                ->find($id);
        return $this->render('CIELOEcommerceBundle:Ecommerce:detailEvenement.html.twig', array(
                    'evenement' => $evenement
        ));
    }

}

?>
