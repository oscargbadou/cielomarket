<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\DomaineActivite;
use Doctrine\ORM\EntityRepository;
use \CIELO\AdminBundle\Entity\CampagneJeu;
use CIELO\AdminBundle\Entity\ParticipationJeu;

class JeuController extends Controller {

    public function listAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        
        $premierJeu = ($page - 1) * 20;
        $totalJeu = $em
                        ->getRepository("CIELOAdminBundle:CampagneJeu")->nbrCampagneJeu();
        $totalPages = ceil(intval($totalJeu) / 20);
        $campagnesJeu = $em
                ->getRepository("CIELOAdminBundle:CampagneJeu")
                ->findBy(array(), array(), 20, $premierJeu);

        return $this->render('CIELOEcommerceBundle:Ecommerce:jeux.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'campagnesJeu' => $campagnesJeu,
        ));
        
    }

    public function jouerMobileAction($idCampagneJeu) {
        $em = $this->getDoctrine()->getManager();
        $idUser = $this->getRequest()->get("id-user");

        $campagneJeu = $em->getRepository("CIELOAdminBundle:CampagneJeu")->find($idCampagneJeu);
        if ($campagneJeu == null) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("jeu non trouvé");
        }
        if ($idUser == null)
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Vous n'êtes pas loggé");
        
        $user = $em->getRepository("CIELOUserBundle:Client")->find($idUser);

        if ($user == null) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Vous n'êtes pas loggé j");
        }

        $derniereParticipationGagnanteJeu = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->derniereParticipationGaganteJeu($campagneJeu->getId());
        $derniereParticipationGagnanteJoueur = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->derniereParticipationGaganteJoueur($campagneJeu->getId(), $user->getId());

        $nombreGagnantJeu = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->countMaxGagnant($campagneJeu->getId());

        $participationJeu = new ParticipationJeu();
        $participationJeu->setCampagneJeu($campagneJeu);
        $participationJeu->setClient($user);
        $num = uniqid();
        $participationJeu->setNumGagnant($num);


        $participationJeu->setDateParticipation(new \DateTime());

        $em->persist($participationJeu);
        $em->flush();
        $nombreParticiapationJoueur = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->nombreParticipationJoueur($campagneJeu->getId(), $user->getId());

        $jeu = $campagneJeu->getJeu();
        $proprietesJeu = $jeu->getProprietes();
        $proprietesValeur = $campagneJeu->getValeurProprieteJeux();
        $args = array();
        foreach ($proprietesJeu as $propriete) {
            $subArg = array();
            foreach ($proprietesValeur as $valeur) {
                if ($propriete->getId() == $valeur->getProprieteJeu()->getId()) {
                    if ($propriete->getType() != "image") {
                        $subArg["id"] = $propriete->getId();
                        $subArg["nom"] = $propriete->getNom();
                        $subArg["alias"] = $propriete->getAlias();
                        $subArg["type"] = $propriete->getType();
                        $subArg["valeur"] = $valeur->getValeur();
                    } else {
                        $valeurJeuDocument = $em->getRepository("CIELOAdminBundle:ValeurJeuDocument")->findBy(array("valeurJeu" => $valeur->getId()));
                        if (is_array($valeurJeuDocument) && count($valeurJeuDocument) > 0) {
                            $subArg["id"] = $propriete->getId();
                            $subArg["nom"] = $propriete->getNom();
                            $subArg["alias"] = $propriete->getAlias();
                            $subArg["type"] = $propriete->getType();
                            foreach ($valeurJeuDocument as $v) {
                                $subSubArg = array();
                                $subSubArg['img'] = "/cielo-market-build2040308/web/" . $v->getDocument()->getWebPath();
                                $subSubArg['gain'] = $v->getGain();
                                $subArg["valeur"][] = $subSubArg;
                            }
                        }
                    }
                }
            }
            $args[] = $subArg;
        }
        $twig = new \Twig_Environment(new \Twig_Loader_String());
        $rendered = $twig->render(
                stripslashes($jeu->getCode()), array(
            "args" => $args,
            "user" => $user,
            "campagneJeu" => $campagneJeu,
            "participationJeu" => $participationJeu,
            "nombreGagnantJeu" => $nombreGagnantJeu,
            "derniereParticipationGagnanteJeu" => $derniereParticipationGagnanteJeu,
            "derniereParticipationGagnanteJoueur" => $derniereParticipationGagnanteJoueur,
            "nombreParticipationJoueur" => $nombreParticiapationJoueur,
                )
        );

        return $this->render('CIELOEcommerceBundle:Ecommerce:jouer_mobile.html.twig', array(
                    "args" => $args,
                    "jeu" => $jeu,
                    "campagneJeu" => $campagneJeu,
                    "code" => $rendered
        ));
    }

    public function jouerAction(CampagneJeu $campagneJeu = null) {
        if ($campagneJeu == null) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("jeu non trouvé");
        }
        $user = $this->getUser();
        if ($user == null) {
            return $this->redirect($this->generateUrl("cielo_client_login"));
        }
        $em = $this->getDoctrine()->getManager();

        $derniereParticipationGagnanteJeu = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->derniereParticipationGaganteJeu($campagneJeu->getId());
        $derniereParticipationGagnanteJoueur = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->derniereParticipationGaganteJoueur($campagneJeu->getId(), $user->getId());

        $nombreGagnantJeu = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->countMaxGagnant($campagneJeu->getId());

        $participationJeu = new ParticipationJeu();
        $participationJeu->setCampagneJeu($campagneJeu);
        $participationJeu->setClient($user);
        $num = uniqid();
        $participationJeu->setNumGagnant($num);


        $participationJeu->setDateParticipation(new \DateTime());

        $em->persist($participationJeu);
        $em->flush();
        $nombreParticiapationJoueur = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->nombreParticipationJoueur($campagneJeu->getId(), $user->getId());

        $jeu = $campagneJeu->getJeu();
        $proprietesJeu = $jeu->getProprietes();
        $proprietesValeur = $campagneJeu->getValeurProprieteJeux();
        $args = array();
        foreach ($proprietesJeu as $propriete) {
            $subArg = array();
            foreach ($proprietesValeur as $valeur) {
                if ($propriete->getId() == $valeur->getProprieteJeu()->getId()) {
                    if ($propriete->getType() != "image") {
                        $subArg["id"] = $propriete->getId();
                        $subArg["nom"] = $propriete->getNom();
                        $subArg["alias"] = $propriete->getAlias();
                        $subArg["type"] = $propriete->getType();
                        $subArg["valeur"] = $valeur->getValeur();
                    } else {
                        $valeurJeuDocument = $em->getRepository("CIELOAdminBundle:ValeurJeuDocument")->findBy(array("valeurJeu" => $valeur->getId()));
                        if (is_array($valeurJeuDocument) && count($valeurJeuDocument) > 0) {
                            $subArg["id"] = $propriete->getId();
                            $subArg["nom"] = $propriete->getNom();
                            $subArg["alias"] = $propriete->getAlias();
                            $subArg["type"] = $propriete->getType();
                            foreach ($valeurJeuDocument as $v) {
                                $subSubArg = array();
                                $subSubArg['img'] = "/cielo-market-build2040308/web/" . $v->getDocument()->getWebPath();
                                $subSubArg['gain'] = $v->getGain();
                                $subArg["valeur"][] = $subSubArg;
                            }
                        }
                    }
                }
            }
            $args[] = $subArg;
        }
        $twig = new \Twig_Environment(new \Twig_Loader_String());
        $rendered = $twig->render(
                stripslashes($jeu->getCode()), array(
            "args" => $args,
            "user" => $user,
            "campagneJeu" => $campagneJeu,
            "participationJeu" => $participationJeu,
            "nombreGagnantJeu" => $nombreGagnantJeu,
            "derniereParticipationGagnanteJeu" => $derniereParticipationGagnanteJeu,
            "derniereParticipationGagnanteJoueur" => $derniereParticipationGagnanteJoueur,
            "nombreParticipationJoueur" => $nombreParticiapationJoueur,
                )
        );

        return $this->render('CIELOEcommerceBundle:Ecommerce:jouer.html.twig', array(
                    "args" => $args,
                    "jeu" => $jeu,
                    "campagneJeu" => $campagneJeu,
                    "code" => $rendered
        ));
    }

    public function traiterJeuAction($idCampagneJeu, $idParticipation) {
        $em = $this->getDoctrine()->getManager();

        $campagneJeu = $em->getRepository("CIELOAdminBundle:CampagneJeu")->find($idCampagneJeu);
        $participationJeu = $em->getRepository("CIELOAdminBundle:ParticipationJeu")->find($idParticipation);


        if ($campagneJeu == null || $participationJeu == null) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("jeu non trouvé");
        }
        $user = $this->getUser();
        if ($user == null) {
            return $this->redirect($this->generateUrl("cielo_client_login"));
        }

        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $gain = $request->request->get("gain");

            $participationJeu->setGain($gain);
            $em->persist($participationJeu);
            $em->flush();
            return new Response('true');
        } else {
            throw new \Symfony\Component\HttpKernel\Exception\HttpException('not found');
        }
    }

}
