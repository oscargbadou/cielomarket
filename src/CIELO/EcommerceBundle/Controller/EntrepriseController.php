<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class EntrepriseController extends Controller {

    public function templateRender() {
        $entreprises = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->findAll();
        return array(
            'entreprisess' => $entreprises,
        );
    }

    public function entreprisesAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premiereEntreprise = ($page - 1) * 20;
        $totalEntreprises = $em
                ->getRepository("CIELOEntrepriseBundle:Entreprise")->nbreEntreprise();
        $totalPages = ceil(intval($totalEntreprises) / 20);
        $entreprises = $em
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->findBy(array(), array(), 20, $premiereEntreprise);
        return $this->render('CIELOEcommerceBundle:Ecommerce:entreprises.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'entreprisess' => $entreprises
        ));
    }

    public function entrepriseAction($id) {
        $entreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->find($id);
        $categories = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Categorie")
                ->findByEntreprise($entreprise);
        $entArray = array(
            'entreprise' => $entreprise,
            'categories' => $categories
        );
        $menuContent = EntrepriseController::templateRender();
        $result = array_merge((array) $menuContent, (array) $entArray);
        return $this->render('CIELOEcommerceBundle:Ecommerce:entreprise.html.twig', $result);
    }

}

?>
