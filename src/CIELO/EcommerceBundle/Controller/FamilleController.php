<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class FamilleController extends Controller {

    public function famillesAction() {
        $familles = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Famille")
                ->findAll();
        return $this->render('CIELOEcommerceBundle:Ecommerce:familles.html.twig', array(
                    'familless' => $familles,
        ));
    }

    public function familleDunDomaineAction($id, $page) {
        $em = $this->getDoctrine()
                ->getManager();
        
        $premiereFamille = ($page - 1) * 20;
        $totalFamille = $em
                        ->getRepository("CIELOEntrepriseBundle:Famille")->nbreFamilleDeDomaine($id);
        $totalPages = ceil(intval($totalFamille) / 20);
        $familles = $em
                ->getRepository("CIELOEntrepriseBundle:Famille")
                ->findByDomaineActivite($id, array(), 20, $premiereFamille);

        return $this->render('CIELOEcommerceBundle:Ecommerce:familleDunDomaine.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'famille' => $familles,
        ));
    }

}

?>
