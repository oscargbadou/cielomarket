<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\DomaineActivite;
use Doctrine\ORM\EntityRepository;

class AcceuilController extends Controller {

    public function acceuilAction() {
        $em = $this->getDoctrine()
                ->getManager();
        $carousel = $em
                ->getRepository("CIELOAdminBundle:Carousel")
                ->createQueryBuilder("c")
                ->where("c.actif = true")
                ->getQuery()
                ->getResult();
        $produitsPlusAchetes = $em
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->findBy(array(), array("purchased" => "DESC"), 16, 0);
        $now = new \DateTime();        
        
        $promoTermines = $em->getRepository("CIELOEntrepriseBundle:Promotion")
                ->createQueryBuilder("p")
                ->where("p.dateFinPromotion < :now")
                ->setParameter("now", $now)
                ->getQuery()
                ->getResult();
        
        foreach ($promoTermines as $p) {
            $produitPromotion = $em->getRepository("CIELOEntrepriseBundle:PromotionProduit")
                    ->findByPromotion($p);
            foreach ($produitPromotion as $pp) {
                $modeles = $em->getRepository("CIELOEntrepriseBundle:Modele")
                        ->findByProduit($pp->getProduit());
                foreach ($modeles as $m) {
//                    var_dump($m->getProduit()->getNom());
                    if ($m->getPrixPromo() != $m->getPrix()) {
                        $m->setPrixPromo($m->getPrix());
                        $em->flush();
                    }
                }
            }
        }
//        die("ok");
        return $this->render('CIELOEcommerceBundle:Ecommerce:acceuil.html.twig', array(
                    'produitsPlusAchetes' => $produitsPlusAchetes,
                    'carousel' => $carousel
        ));
    }

}

?>
