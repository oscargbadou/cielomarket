<?php

namespace CIELO\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class DomaineActiviteController extends Controller {

    public function templateRender() {
        $domaineActivites = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")
                ->findAll();
        $familles = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Famille")
                ->findAll();
        $entreprises = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->findAll();
        $dom = array();
        $fam = array();
        $ent = array();
        if (count($domaineActivites) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($dom, $domaineActivites[$i]);
            }
        } else {
            $dom = $domaineActivites;
        }

        if (count($familles) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($fam, $familles[$i]);
            }
        } else {
            $fam = $familles;
        }

        if (count($entreprises) > 10) {
            for ($i = 0; $i < 10; $i++) {
                array_push($ent, $entreprises[$i]);
            }
        } else {
            $ent = $entreprises;
        }
        return array(
            'domaineActivites' => $dom,
            'familles' => $fam,
            'entreprises' => $ent,
            'domaineActivitess' => $domaineActivites
        );
    }

    public function domaineActvitesAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierDomaine = ($page - 1) * 20;
        $totalDomaines = $em
                        ->getRepository("CIELOEntrepriseBundle:DomaineActivite")->nbreDomaine();
        $totalPages = ceil(intval($totalDomaines) / 20);
        $domaineAvtivites = $em
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")
                ->findBy(array(), array(), 20, $premierDomaine);
        
        return $this->render('CIELOEcommerceBundle:Ecommerce:domaineActivites.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'domaineActivites' => $domaineAvtivites,
        ));
    }

}

?>
