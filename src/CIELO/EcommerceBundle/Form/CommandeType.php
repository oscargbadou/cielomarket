<?php

namespace CIELO\EcommerceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommandeType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('zone', 'entity', array(
                    'class' => "CIELOAdminBundle:ZoneGeographique",
                    'property' => "nom",
                    'empty_value' => 'Choisir votre zone',
                    'attr' => array(
                        'class' => "zone-geographique",
                        'id' => "zone-geographique"
                    )
                ))
                ->add('adresse', 'text', array(
                    'attr' => array(
                        'class' => "adresse",
                        'id' => "adresse"
                    )
                ))
                ->add('modePaiement', 'entity', array(
                    'class' => "CIELOAdminBundle:ModePaiement",
                    'property' => "nom",
                    'empty_value' => 'Choisir un mode de paiement',
                    'attr' => array(
                        'class' => "mode-paiement",
                        'id' => "mode-paiement"
                    )
                ))
                ->add('modeLivraison', 'entity', array(
                    'class' => "CIELOAdminBundle:modeLivraison",
                    'property' => "nom",
                    'empty_value' => 'Choisir un mode de livraison',
                    'attr' => array(
                        'class' => "mode-livraison",
                        'id' => "mode-livraison"
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EcommerceBundle\Entity\Commande'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_ecommercebundle_commande';
    }

}
