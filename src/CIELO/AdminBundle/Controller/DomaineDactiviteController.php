<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\DomaineActivite;
use CIELO\EntrepriseBundle\Form\DomaineActiviteType;

class DomaineDactiviteController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function dashBoardAction() {
        $em=$this->getDoctrine()->getManager();
        $totalDomaineActivites = $em
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")->nbreDomaine();
        $totalFamilles = $em
                ->getRepository("CIELOEntrepriseBundle:Famille")->nbreFamille();
        $totalEntreprises = $em
                ->getRepository("CIELOEntrepriseBundle:Entreprise")->nbreEntreprise();
        $totalClients = $em
                ->getRepository("CIELOUserBundle:Client")
                ->createQueryBuilder("c")
                ->select("COUNT(c)")
                ->getQuery()
                ->getSingleScalarResult();
        $nbreCommandeNonVue = $em
                ->getRepository("CIELOEcommerceBundle:Commande")->nbreCommandeNonVue();
        
        return $this->render('CIELOAdminBundle:Admin:index.html.twig', array(
            'totalDomaineActivites'=>$totalDomaineActivites,
            'totalFamilles'=>$totalFamilles,
            'totalClients'=>$totalClients,
            'totalEntreprises'=>$totalEntreprises,
            'nbreCommandeNonVue'=>$nbreCommandeNonVue
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listeAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierDomaine = ($page - 1) * 100;
        $totalDomaines = $em->getRepository("CIELOEntrepriseBundle:DomaineActivite")->nbreDomaine();
        $totalPages = ceil(intval($totalDomaines) / 100);
        $domaineDactivites = $em
                ->getRepository("CIELOEntrepriseBundle:DomaineActivite")
                ->findBy(array(), array(), 100, $premierDomaine);
        return $this->render('CIELOAdminBundle:Admin:domaines-d-activites.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'domainesdactivites' => $domaineDactivites
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerAction() {
        $domaineDactivite = new DomaineActivite();
        $form = $this->createForm(new DomaineActiviteType, $domaineDactivite);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($domaineDactivite);
                $em->flush();
                //$this->get('session')->getFlashBag()->add('info', 'Domaine d\'activité bien créé');
                return $this->redirect($this->generateUrl('cielo_admin_domainedactivitespage'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerDomaineDactivite.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierAction($id) {
        $domaineDactivite = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:DomaineActivite')
                ->find($id);
        if ($domaineDactivite === null) {
            throw $this->createNotFoundException('Ce domaine d\'activité n\'existe pas encore');
        }
        $form = $this->createForm(new DomaineActiviteType, $domaineDactivite);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($domaineDactivite);
                $em->flush();
                //$this->get('session')->getFlashBag()->add('info', 'Domaine d\'activité bien créé');
                return $this->redirect($this->generateUrl('cielo_admin_domainedactivitespage'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerDomaineDactivite.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction($id) {
        $domaineDactivite = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:DomaineActivite')
                ->find($id);
        if ($domaineDactivite === null) {
            throw $this->createNotFoundException('Ce domaine d\'activité n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($domaineDactivite);
        $em->flush();
        //$this->get('session')->getFlashBag()->add('info', 'Domaine d\'activité bien supprimé');
        return $this->redirect($this->generateUrl('cielo_admin_domainedactivitespage'));
    }
    
    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function activerDomaineActiviteAction($id) {
        $domaine = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:DomaineActivite')
                ->find($id);
        if($domaine->getActif()==false){
            $domaine->setActif(true);
        }else{
            $domaine->setActif(false);
        }        
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($domaine);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_domainedactivitespage'));
    }
}

?>
