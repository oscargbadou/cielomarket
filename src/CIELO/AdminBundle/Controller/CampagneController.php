<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\AdminBundle\Entity\Campagne;
use CIELO\AdminBundle\Form\CampagneType;
use \CIELO\AdminBundle\Entity\CampagneJeu;
use \CIELO\EntrepriseBundle\Form\DocumentType;
use \CIELO\AdminBundle\Entity\ValeurProprieteJeu;
use \CIELO\EntrepriseBundle\Entity\Document;
use \CIELO\AdminBundle\Entity\ValeurJeuDocument;

class CampagneController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premiereCampagne = ($page - 1) * 100;
        $totalCampganes = $em->getRepository("CIELOAdminBundle:Campagne")->nbrCampagne();
        $totalPages = ceil(intval($totalCampganes) / 100);
        $campagnes = $em->getRepository('CIELOAdminBundle:Campagne')
                ->findBy(array(), array(), 100, $premiereCampagne);

        return $this->render('CIELOAdminBundle:Campagne:list.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'campagnes' => $campagnes,
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function editerAction(Campagne $campagne = null) {
        $new = false;
        if ($campagne === null) {
            $campagne = new Campagne();
            $new = true;
        }
        $request = $this->getRequest();
        $form = $this->createForm(new CampagneType(), $campagne);
        if ($request->getMethod() == "POST") {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($campagne);

                $em->flush();
                return $this->redirect($this->generateUrl("cielo_admin_list_campagne"));
            }
        }
        if ($new) {
            $template = "CIELOAdminBundle:Campagne:ajouter.html.twig";
        } else {
            $template = "CIELOAdminBundle:Campagne:modifier.html.twig";
        }
        return $this->render($template, array(
                    "form" => $form->createView()
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction(Campagne $campagne = null) {
        if ($campagne !== null) {
            $em = $this->getDoctrine()->getEntityManager();

            $em->remove($campagne);
            $em->flush();
        }
        return $this->redirect($this->generateUrl("cielo_admin_list_campagne"));
    }

    public function listCampagneJeuxAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premiereCampagneJeu = ($page - 1) * 100;
        $totalCampganesJeux = $em->getRepository("CIELOAdminBundle:CampagneJeu")->nbrCampagneJeu();
        $totalPages = ceil(intval($totalCampganesJeux) / 100);
        $campagnesJeux = $em
                ->getRepository('CIELOAdminBundle:CampagneJeu')
                ->findBy(array(), array(), 100, $premiereCampagneJeu);

        return $this->render('CIELOAdminBundle:Campagne:list_campagne_jeu.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'campagnesJeux' => $campagnesJeux,
                    'host' => $_SERVER["HTTP_HOST"]
        ));
    }

    public function editerCampagneJeuxAction() {
        $request = $this->getRequest();
        $campagneId = $request->get("campagne");
        $jeuId = $request->get("jeu");

        $em = $this->getDoctrine()->getEntityManager();


        $jeux = $em->getRepository("CIELOAdminBundle:Jeu")->findAll();
        $campagnes = $em->getRepository("CIELOAdminBundle:Campagne")->findAll();
        if ($campagneId == null || $jeuId == null) {
            return $this->render("CIELOAdminBundle:Campagne:editer_campagne_jeu.html.twig", array(
                        'details' => false,
                        'jeux' => $jeux,
                        'jeuId' => $jeuId,
                        'campagneId' => $campagneId,
                        'campagnes' => $campagnes
            ));
        } else {
            $jeu = $em->getRepository("CIELOAdminBundle:Jeu")->find($jeuId);
            $campagne = $em->getRepository("CIELOAdminBundle:Campagne")->find($campagneId);
            if ($jeu == null || $campagne == null) {
                throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('campagne ou jeu non trouvé');
            }
            $campagneJeu = $em->getRepository("CIELOAdminBundle:CampagneJeu")->findOneBy(array("campagne" => $campagneId, "jeu" => $jeuId));
            if ($campagneJeu == null) {
                $campagneJeu = new CampagneJeu();
            }
            $campagneJeu->setCampagne($campagne);
            $campagneJeu->setJeu($jeu);

            $proprietesJeu = $jeu->getProprietes();

            $formBuilder = $this->createFormBuilder();
            $formBuilder->add("nomCampagneJeu", "text", array(
                'label' => "Nom de la campagne de jeu",
                'attr' => array(
                    'desc' => "Donnez un nom à cette campagne de jeu"
                )
            ));
            $formBuilder->add("descriptionCampagneJeu", "textarea", array(
                'label' => "Description de la campagne de jeu",
                'attr' => array(
                    'desc' => "Donnez une description à votre campagne de jeu"
                )
            ));
            $formBuilder->add("icone", new \CIELO\EntrepriseBundle\Form\DocumentFileType(), array(
                'label' => "Image de la campagne de jeu",
                'attr' => array(
                    'desc' => "Ajouter une image à la campagne de jeu"
                )
            ));

            foreach ($proprietesJeu as $propriete) {

                switch ($propriete->getType()) {
                    case "string":
                        $formBuilder->add($propriete->getAlias(), "text", array(
                            'label' => $propriete->getNom(),
                            'attr' => array(
                                'desc' => $propriete->getDescription()
                            )
                        ));
                        break;
                    case "int":
                        $formBuilder->add($propriete->getAlias(), "integer", array(
                            'label' => $propriete->getNom(),
                            'attr' => array(
                                'desc' => $propriete->getDescription()
                            )
                        ));
                        break;
                    case "image":

                        $formBuilder->add($propriete->getAlias(), 'collection', array(
                            'type' => new \CIELO\AdminBundle\Form\ImageJeuType(),
                            'allow_add' => true,
                            'allow_delete' => true,
                            'by_reference' => true,
                            'attr' => array(
                                'desc' => $propriete->getDescription()
                            )
                                )
                        );
                        break;
                    case "choice":
                        $choix = explode(',', $propriete->getChoix());
                        $choices = array();
                        foreach ($choix as $c) {
                            $choices[$c] = $c;
                        }
                        $formBuilder->add($propriete->getAlias(), "choice", array(
                            'label' => $propriete->getNom(),
                            "choices" => $choices,
                            'attr' => array(
                                'desc' => $propriete->getDescription()
                            )
                        ));
                        break;
                    case "entity":
                        $entity = $propriete->getEntity();
                        $formBuilder->add($propriete->getAlias(), "entity", array(
                            'label' => $propriete->getNom(),
                            'class' => $entity,
                            'property' => "id",
                            'empty_value' => 'Choisir l\'id de l\'entité',
                            'required' => true,
                            'attr' => array(
                                'desc' => $propriete->getDescription()
                            )
                        ));
                        break;
                    default:
                        break;
                }
            }
            $form = $formBuilder->getForm();
            if ($request->getMethod() == "POST") {
                $form->handleRequest($request);
                $invalidForm = false;
                $data = $request->request->get("form");
                $fileData = $request->files->get("form");
                /* $file = $form->get("images"); */
                //var_dump($data);
                //var_dump($request->request);
                $campagneJeu->setNom($data['nomCampagneJeu']);
                $campagneJeu->setDescription($data['descriptionCampagneJeu']);

                if ($fileData['icone']) {
                    $iconDocument = new Document();
                    $iconFile = $fileData['icone']['file'];
                    $iconDocument->setFile($iconFile);
                    $campagneJeu->setIcone($iconDocument);
                    $em->persist($iconDocument);
                }
                foreach ($proprietesJeu as $propriete) {
                    $propertyName = $propriete->getAlias();

                    if (array_key_exists($propertyName, $data)) {
                        if ($propriete->getType() != "image") {
                            $valeur = $data[$propertyName];
                            //var_dump($propertyName);
                            $propertyValue = $em->getRepository("CIELOAdminBundle:ValeurProprieteJeu")->findOneBy(array("campagneJeu" => $campagneJeu->getId(), "proprieteJeu" => $propriete->getId()));
                            if ($propertyValue == null) {
                                $propertyValue = new ValeurProprieteJeu();
                            }
                            $propertyValue->setCampagneJeu($campagneJeu);
                            $campagneJeu->addValeurProprieteJeux($propertyValue);
                            $propertyValue->setProprieteJeu($propriete);
                            $propertyValue->setValeur($valeur);
                            $em->persist($propertyValue);
                            $em->persist($campagneJeu);
                        } else {


                            foreach ($data[$propertyName] as $key => $imageData) {

                                $gain = $imageData['gain'];
                                $imageFile = $fileData[$propertyName];
                                $file = $imageFile[$key]["file"];

                                $propertyValue = $em->getRepository("CIELOAdminBundle:ValeurProprieteJeu")->findOneBy(array("campagneJeu" => $campagneJeu->getId(), "proprieteJeu" => $propriete->getId()));
                                if ($propertyValue == null) {
                                    $propertyValue = new ValeurProprieteJeu();
                                } else {
                                    $valeurJeuDocument = $em->getRepository("CIELOAdminBundle:ValeurJeuDocument")->findBy(array("valeurJeu" => $propertyValue->getId()));
                                    foreach ($valeurJeuDocument as $vjd) {
                                        $em->remove($vjd);
                                        $em->remove($vjd->getDocument());
                                    }
                                }
                                $valeurJeuDocument = new ValeurJeuDocument();
                                $document = new Document();

                                $propertyValue->setCampagneJeu($campagneJeu);
                                $campagneJeu->addValeurProprieteJeux($propertyValue);
                                $propertyValue->setProprieteJeu($propriete);

                                $document->setFile($file);

                                $valeurJeuDocument->setDocument($document);
                                $valeurJeuDocument->setValeurJeu($propertyValue);
                                $valeurJeuDocument->setGain($gain);
                                $em->persist($document);
                                $em->persist($propertyValue);
                                $em->persist($valeurJeuDocument);
                                $em->persist($campagneJeu);
                            }
                        }
                    } else {
                        $invalidForm = true;
                        break;
                    }
                }

                if (!$invalidForm) {
                    $em->flush();
                }
            }

            return $this->render("CIELOAdminBundle:Campagne:editer_campagne_jeu.html.twig", array(
                        'details' => true,
                        'jeux' => $jeux,
                        'jeuId' => $jeuId,
                        'campagneId' => $campagneId,
                        'campagnes' => $campagnes,
                        'form' => $form->createView()
            ));
        }
    }

    public function supprimerCampagneJeuAction(CampagneJeu $campagneJeu) {
        if ($campagneJeu !== null) {
            $em = $this->getDoctrine()->getManager();
            foreach ($campagneJeu->getValeurProprieteJeux() as $valeurPropriete) {
                $valeurJeuDocument = $em->getRepository("CIELOAdminBundle:ValeurJeuDocument")->findBy(array("valeurJeu" => $valeurPropriete->getId()));
                foreach ($valeurJeuDocument as $vJeuDoc) {
                    $em->remove($vJeuDoc);
                }
                $em->remove($valeurPropriete);
            }
            foreach ($campagneJeu->getParticipationsJeu() as $participation) {
                $em->remove($participation);
            }
            $em->remove($campagneJeu);
            $em->flush();
            return $this->redirect($this->generateUrl("cielo_admin_list_campagne_jeu"));
        }
    }

}
