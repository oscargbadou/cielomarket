<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\AdminBundle\Entity\Carousel;
use CIELO\AdminBundle\Form\CarouselType;
use CIELO\AdminBundle\Entity\TexteDefilant;
use CIELO\AdminBundle\Form\TexteDefilantType;

class ParametreController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function carouselAction() {
        $carousels = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Carousel')
                ->findAll();
        $newCarousel = new carousel();
        $form = $this->createForm(new CarouselType(), $newCarousel);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newCarousel);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_carousel'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:carousel.html.twig', array(
                    'form' => $form->createView(),
                    'carousels' => $carousels
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function texteDefilantAction() {
        $allTexteDefilant = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository("CIELOAdminBundle:TexteDefilant")
                ->findAll();
        $newTexteDefilant = new texteDefilant();
        $form = $this->createForm(new TexteDefilantType(), $newTexteDefilant);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newTexteDefilant);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_texte_defilant'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:texteDefilant.html.twig', array(
                    'form' => $form->createView(),
                    'allTexteDefilant' => $allTexteDefilant
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierTexteDefilantAction($id) {
        $allTexteDefilant = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository("CIELOAdminBundle:TexteDefilant")
                ->findAll();
        $newTexteDefilant = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository("CIELOAdminBundle:TexteDefilant")
                ->find($id);
        $form = $this->createForm(new TexteDefilantType(), $newTexteDefilant);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newTexteDefilant);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_texte_defilant'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:texteDefilant.html.twig', array(
                    'form' => $form->createView(),
                    'allTexteDefilant' => $allTexteDefilant
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function activerTexteDefilantAction($id) {
        $texteDefilant = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:TexteDefilant')
                ->find($id);
        $texteDefilant->setActif(true);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($texteDefilant);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_texte_defilant'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function activerImageCarouselAction($id) {
        $thisCarousel = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Carousel')
                ->find($id);
        $thisCarousel->setActif(true);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($thisCarousel);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_carousel'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function desactiverTexteDefilantAction($id) {
        $texteDefilant = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:TexteDefilant')
                ->find($id);
        $texteDefilant->setActif(false);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($texteDefilant);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_texte_defilant'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerTexteDefilantAction($id) {
        $texteDefilant = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:TexteDefilant')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($texteDefilant);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_texte_defilant'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function desactiverImageCarouselAction($id) {
        $thisCarousel = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Carousel')
                ->find($id);
        $thisCarousel->setActif(false);
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($thisCarousel);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_carousel'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerImageCarouselAction($id) {
        $thisCarousel = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Carousel')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($thisCarousel);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_carousel'));
    }

}

?>
