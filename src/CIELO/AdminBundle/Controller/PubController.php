<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\AdminBundle\Entity\Pub;
use CIELO\AdminBundle\Form\PubType;

class PubController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listAction() {
        $pubs = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Pub')
                ->createQueryBuilder("p")
                ->orderBy("p.date","DESC")
                ->getQuery()
                ->getResult();
        $newPub = new Pub();
        $form = $this->createForm(new PubType(), $newPub);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newPub);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_pub'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:pub.html.twig', array(
                    'form' => $form->createView(),
                    'pubs' => $pubs
        ));
    }

    
    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierPubAction($id) {
        $pubs = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Pub')
                ->findAll();
        $newPub = $this->getDoctrine()
                ->getEntityManager()
                ->getRepository("CIELOAdminBundle:Pub")
                ->find($id);
        $form = $this->createForm(new PubType(), $newPub);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newPub);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_pub'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:pub.html.twig', array(
                    'form' => $form->createView(),
                    'pubs' => $pubs
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function activerPanelGaucheAction($id) {
        $pub = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Pub')
                ->find($id);
        if($pub->getPanelGauche()==true){
            $pub->setPanelGauche(false);
        }else{
            $pub->setPanelGauche(true);
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($pub);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_pub'));
    }
    
    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function activerPanelDroitAction($id) {
        $pub = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Pub')
                ->find($id);
        if($pub->getPanelDroit()==true){
            $pub->setPanelDroit(false);
        }else{
            $pub->setPanelDroit(true);
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($pub);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_pub'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerPubAction($id) {
        $pub = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Pub')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($pub);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_pub'));
    }

   
}

?>
