<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class RendezVousController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierRendezVous = ($page - 1) * 100;
        $totalRendezVous = $em
                ->getRepository("CIELOEntrepriseBundle:RendezVous")
                ->createQueryBuilder("c")
                ->select("COUNT(c)")
                ->getQuery()
                ->getSingleScalarResult();
        $totalPages = ceil(intval($totalRendezVous) / 100);
        $rendezVous = $em
                ->getRepository("CIELOEntrepriseBundle:RendezVous")
                ->createQueryBuilder("r")
                ->orderBy("r.dateEnvoie", "DESC")
                ->setFirstResult($premierRendezVous)
                ->setMaxResults(100)
                ->getQuery()
                ->getResult();
        foreach($rendezVous as $r){
            if($r->getVue()!=true){
                $r->setVue(true);
            }
        }
        return $this->render('CIELOAdminBundle:Admin:listRendezVous.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'rendezVous' => $rendezVous
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerRendezVousAction($id) {
        $rdv = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:RendezVous')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($rdv);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_list_rendez_vous'));
    }

}

?>
