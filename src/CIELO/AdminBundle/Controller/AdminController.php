<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\DomaineActivite;
use CIELO\EntrepriseBundle\Form\DomaineActiviteType;

class AdminController extends Controller {

    public function loginAction() {
        return $this->render('CIELOAdminBundle:Admin:login.html.twig');
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function indexAction() {
        return $this->render('CIELOAdminBundle:Admin:index.html.twig');
    }

    public function domaineDactivitesAction() {
        $domaineDactivites = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:DomaineActivite')
                ->findAll();
        return $this->render('CIELOAdminBundle:Admin:domaines-d-activites.html.twig', array(
                    'domainesdactivites' => $domaineDactivites
        ));
    }

    public function creerDomaineDactiviteAction() {
        $domaineDactivite = new DomaineActivite();
        $form = $this->createForm(new DomaineActiviteType, $domaineDactivite);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($domaineDactivite);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info', 'Domaine d\'activité bien créé');
                return $this->redirect($this->generateUrl('cielo_admin_domainedactivitespage'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerDomaineDactivite.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function administrationAction() {
        $admins = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOUserBundle:Admin')
                ->findAll();
        $adminArray = array();
        foreach ($admins as $ad) {
            $admin = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('CIELOUserBundle:User')
                    ->find($ad->getId());
            $adminArray[] = $admin;
        }
        return $this->render('CIELOAdminBundle:Admin:administration.html.twig', array(
                    'admin' => $adminArray
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function rechercheAdminAction($page) {
        $searchContent = $this->get('request')->request->get('searchContent');
        $searchType = $this->get('request')->request->get('searchType');
        $em = $this->getDoctrine()
                ->getManager();
        switch ($searchType) {
            case 'produit':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOEntrepriseBundle:Produit")->produitsRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:produitsRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'domaineActivite':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEntrepriseBundle:DomaineActivite")->nbreDomaineRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOEntrepriseBundle:DomaineActivite")->domainesRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:domainesRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'famille':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEntrepriseBundle:Famille")->nbreFamilleRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOEntrepriseBundle:Famille")->famillesRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:famillesRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'entreprise':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEntrepriseBundle:Entreprise")->nbreEntrepriseRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOEntrepriseBundle:Entreprise")->entreprisesRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:entreprisesRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'client':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em
                        ->getRepository("CIELOUserBundle:Client")
                        ->createQueryBuilder("c")
                        ->select("COUNT(c)")
                        ->where("c.nom LIKE :searchContent OR c.prenom LIKE :searchContent OR c.username LIKE :searchContent")
                        ->setParameter("searchContent", '%' . $searchContent . '%')
                        ->getQuery()
                        ->getSingleScalarResult();
                $elementsRechercher = $em
                        ->getRepository("CIELOUserBundle:Client")
                        ->createQueryBuilder("c")
                        ->where("c.nom LIKE :searchContent OR c.prenom LIKE :searchContent OR c.username LIKE :searchContent")
                        ->setParameter("searchContent", '%' . $searchContent . '%')
                        ->setFirstResult($premierElement)
                        ->setMaxResults(100)
                        ->getQuery()
                        ->getResult();
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:clientsRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'evenement':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOAdminBundle:Evenement")->nbreEvenementRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOAdminBundle:Evenement")->evenementsRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:evenementsRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'commande':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEcommerceBundle:Commande")->nbreCommandeRechercher($searchContent);
                $elementsRechercher = $em->getRepository("CIELOEcommerceBundle:Commande")->commandesRechercher($searchContent, $premierElement);
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOAdminBundle:Admin:commandesRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
        }
    }

}

?>
