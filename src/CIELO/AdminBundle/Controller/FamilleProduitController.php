<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\Famille;
use CIELO\EntrepriseBundle\Form\FamilleType;

class FamilleProduitController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listeFamilleAction($page) {        
        $em = $this->getDoctrine()
                ->getManager();
        $premiereFamille = ($page - 1) * 100;
        $totalFamilles = $em->getRepository("CIELOEntrepriseBundle:Famille")->nbreFamille();
        $totalPages = ceil(intval($totalFamilles) / 100);
        $familleProduits = $em
                ->getRepository("CIELOEntrepriseBundle:Famille")
                ->findBy(array(), array(), 100, $premiereFamille);
        return $this->render('CIELOAdminBundle:Admin:listeFamille.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'familleProduit' => $familleProduits
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerAction() {
        $familleProduit = new Famille();
        $form = $this->createForm(new FamilleType, $familleProduit);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($familleProduit);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info', 'famille de produit bien ajouté');
                return $this->redirect($this->generateUrl('cielo_admin_famille_produit'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerFamilleProduit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierAction($id) {
        $familleProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Famille')
                ->find($id);
        if ($familleProduit === null) {
            throw $this->createNotFoundException('Cette famille de produit n\'existe pas encore');
        }
        $form = $this->createForm(new FamilleType, $familleProduit);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($familleProduit);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info', 'famille de produit bien ajouté');
                return $this->redirect($this->generateUrl('cielo_admin_famille_produit'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerFamilleProduit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction($id) {
        $familleProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Famille')
                ->find($id);
        if ($familleProduit === null) {
            throw $this->createNotFoundException('Cette famille de produit n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($familleProduit);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'Famille de produit bien supprimé');
        return $this->redirect($this->generateUrl('cielo_admin_famille_produit'));
    }

}

?>
