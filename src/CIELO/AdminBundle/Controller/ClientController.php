<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;

class ClientController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listClientAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierClient = ($page - 1) * 100;
        $totalClients = $em
                ->getRepository("CIELOUserBundle:Client")
                ->createQueryBuilder("c")
                ->select("COUNT(c)")
                ->getQuery()
                ->getSingleScalarResult();
        $totalPages = ceil(intval($totalClients) / 100);
        $clients = $em
                ->getRepository("CIELOUserBundle:Client")
                ->findBy(array(), array(), 100, $premierClient);
        return $this->render('CIELOAdminBundle:Admin:clients.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'clients' => $clients
        ));
    }
    
    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerClientAction($id) {
        $client = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOUserBundle:Client')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($client);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_list_client'));
    }

}

?>
