<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\DomaineActivite;
use CIELO\AdminBundle\Entity\Evenement;
use CIELO\EntrepriseBundle\Form\DomaineActiviteType;
use CIELO\AdminBundle\Form\EvenementType;

class EvenementController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function evenementsAction($page) {
        $now = new \DateTime();
        $evenementsEnCoursEtFutur = $this->getDoctrine()
                ->getManager()
                ->getRepository("CIELOAdminBundle:Evenement")
                ->createQueryBuilder("e")
                ->where("e.dateDebut <= :now")
                ->where("e.dateFin >= :now")
                ->setParameter("now", $now)
                ->getQuery()
                ->getResult();

//        $evenements = $this->getDoctrine()
//                ->getManager()
//                ->getRepository('CIELOAdminBundle:Evenement')
//                ->findAll();
//        return $this->render('CIELOAdminBundle:Admin:evenements.html.twig', array(
//                    'evenements' => $evenements,
//                    'evenementsEnCoursEtFutur' => $evenementsEnCoursEtFutur
//        ));

        $em = $this->getDoctrine()
                ->getManager();
        $premierEvenement = ($page - 1) * 100;
        $totalEvenements = $em->getRepository("CIELOAdminBundle:Evenement")->nbreEvenement();
        $totalPages = ceil(intval($totalEvenements) / 100);
        $evenements = $em
                ->getRepository("CIELOAdminBundle:Evenement")
                ->findBy(array(), array(), 100, $premierEvenement);
        return $this->render('CIELOAdminBundle:Admin:evenements.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'evenements' => $evenements,
                    'evenementsEnCoursEtFutur' => $evenementsEnCoursEtFutur
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerAction() {
        $evenement = new Evenement();
        $form = $this->createForm(new EvenementType(), $evenement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                if ($evenement->getProprieteEvenements()) {
                    foreach ($evenement->getProprieteEvenements() as $pro) {
                        $pro->setEvenement($evenement);
                        $em->persist($pro);
                    }
                }
                $em->persist($evenement);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_evenements'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerEvenement.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierAction($id) {
        $evenement = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Evenement')
                ->find($id);
        if ($evenement === null) {
            throw $this->createNotFoundException('Ce domaine d\'activité n\'existe pas encore');
        }
        $form = $this->createForm(new EvenementType, $evenement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($evenement->getProprieteEvenements() as $pro) {
                    if ($pro->getId() == null) {
                        $pro->setEvenement($evenement);
                        $em->persist($pro);
                    }
                }
                $em->persist($evenement);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_evenements'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerEvenement.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction($id) {
        $evenement = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Evenement')
                ->find($id);
        if ($evenement === null) {
            throw $this->createNotFoundException('Cet événement n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($evenement);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_evenements'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function participantsEvenementAction($id) {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository('CIELOAdminBundle:Evenement')->find($id);
        foreach ($evenement->getParticipationEvenements() as $pe) {
            if ($pe->getVue() == false) {
                $pe->setVue(true);
                $em->flush();
            }
        }
        return $this->render('CIELOAdminBundle:Admin:participantsEvenement.html.twig', array(
                    'participationEvenement' => $evenement->getParticipationEvenements(),
        ));
    }

}

?>
