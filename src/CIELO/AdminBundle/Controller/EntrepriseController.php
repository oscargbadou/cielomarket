<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\Entreprise;
use CIELO\EntrepriseBundle\Entity\AdminEntreprise;
use CIELO\EntrepriseBundle\Form\EntrepriseType;
use CIELO\EntrepriseBundle\Form\AdminEntrepriseType;

class EntrepriseController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listeEntrepriseAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premiereEntreprise = ($page - 1) * 100;
        $totalEntreprises = $em->getRepository("CIELOEntrepriseBundle:Entreprise")->nbreEntreprise();
        $totalPages = ceil(intval($totalEntreprises) / 100);
        $listeEntreprise = $em
                ->getRepository("CIELOEntrepriseBundle:Entreprise")
                ->findBy(array(), array(), 100, $premiereEntreprise);
        return $this->render('CIELOAdminBundle:Admin:listeEntreprise.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'listeEntreprise' => $listeEntreprise
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerAction() {
        $newEntreprise = new Entreprise();
        $form = $this->createForm(new EntrepriseType, $newEntreprise);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newEntreprise);
                $em->flush();
                //$this->get('session')->getFlashBag()->add('info', 'famille de produit bien ajouté');
                return $this->redirect($this->generateUrl('cielo_admin_entreprise'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerEntreprise.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierAction($id) {
        $entreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Entreprise')
                ->find($id);
        if ($entreprise === null) {
            throw $this->createNotFoundException('Cette entreprise n\'existe pas encore');
        }
        $form = $this->createForm(new EntrepriseType, $entreprise);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($entreprise);
                $em->flush();
                //$this->get('session')->getFlashBag()->add('info', 'famille de produit bien ajouté');
                return $this->redirect($this->generateUrl('cielo_admin_entreprise'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerEntreprise.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction($id) {
        $entreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Entreprise')
                ->find($id);
        if ($entreprise === null) {
            throw $this->createNotFoundException('Cette entreprise n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($entreprise);
        $em->flush();
        //$this->get('session')->getFlashBag()->add('info', 'Famille de produit bien supprimé');
        return $this->redirect($this->generateUrl('cielo_admin_entreprise'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerAdminEntrepriseAction() {
        $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('CIELO\EntrepriseBundle\Entity\AdminEntreprise');
        $userManager = $this->container->get('pugx_user_manager');
        
        $newAdminEntreprise = new AdminEntreprise();
        $form = $this->createForm($this->get("admin_enterprise.form.type"), $newAdminEntreprise);
        $request = $this->get('request');

        
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
//                $em = $this->getDoctrine()->getEntityManager();
//                $em->persist($newAdminEntreprise);
//                $em->flush();
               // $adminEntreprise = $userManager->createUser();
                $newAdminEntreprise->addRole("ROLE_ADMIN_ENTERPRISE");
                $newAdminEntreprise->setEnabled(true);
                $userManager->updateUser($newAdminEntreprise, true);
                return $this->redirect($this->generateUrl('cielo_admin_entreprise'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:creerAdminEntreprise.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}

?>
