<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\AdminBundle\Entity\Jeu;
use \CIELO\AdminBundle\Form\JeuType;
use \CIELO\AdminBundle\Entity\ProprieteJeu;

class JeuController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierJeu = ($page - 1) * 100;
        $totalJeux = $em->getRepository("CIELOAdminBundle:Jeu")->nbrJeu();
        $totalPages = ceil(intval($totalJeux) / 100);
        $jeux = $em
                ->getRepository('CIELOAdminBundle:Jeu')
                ->findBy(array(), array(), 100, $premierJeu);

        return $this->render('CIELOAdminBundle:Jeu:list.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'jeux' => $jeux,
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function editerAction(Jeu $jeu = null) {
        $new = false;
        if ($jeu === null) {
            $jeu = new Jeu();
            $new = true;
        }
        $request = $this->getRequest();
        $form = $this->createForm(new JeuType(), $jeu);
        if ($request->getMethod() == "POST") {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($jeu);
                foreach ($jeu->getProprietes() as $propriete) {
                    $propriete->setJeu($jeu);
                    $em->persist($propriete);
                }
                $em->flush();
            }
        }
        if ($new) {
            $template = "CIELOAdminBundle:Jeu:ajouter.html.twig";
        } else {
            $template = "CIELOAdminBundle:Jeu:modifier.html.twig";
        }
        return $this->render($template, array(
                    "form" => $form->createView()
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction(Jeu $jeu = null) {
        if ($jeu !== null) {
            $em = $this->getDoctrine()->getEntityManager();

            foreach ($jeu->getProprietes() as $propriete) {
                $jeu->removePropriete($propriete);
                $em->remove($propriete);
            }
            $em->remove($jeu);
            $em->flush();
        }
        return $this->redirect($this->generateUrl("cielo_admin_list_jeux"));
    }

    public function proprieteslistAction(Jeu $jeu) {

        return $this->render('CIELOAdminBundle:Jeu:proprietes_list.html.twig', array(
                    'jeu' => $jeu,
        ));
    }

    public function supprimerProprieteAction(ProprieteJeu $propr) {
        $jeu = $propr->getJeu();
        if ($propr !== null) {
            $em = $this->getDoctrine()->getEntityManager();

            $valeursJeu = $em->getRepository("CIELOAdminBundle:ValeurProprieteJeu")->findBy(array("proprieteJeu" => $propr->getId()));
            foreach ($valeursJeu as $value) {
                if ($propr->getType() == "image") {

                    $valeursJeuDocument = $em->getRepository("CIELOAdminBundle:ValeurJeuDocument")->findBy(array("valeurJeu" => $value->getId()));
                    foreach ($valeursJeuDocument as $vJeuDoc) {
                        $em->remove($vJeuDoc);
                        $em->remove($vJeuDoc->getDocument());
                    }
                }
                $em->remove($value);
            }

            $em->remove($propr);
            $em->flush();
        }
        return $this->redirect($this->generateUrl("cielo_admin_list_propriete_jeu", array("id"=>$jeu->getId())));
    }

}
