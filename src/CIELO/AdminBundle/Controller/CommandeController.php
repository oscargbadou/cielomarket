<?php

namespace CIELO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\AdminBundle\Entity\ModeLivraison;
use CIELO\AdminBundle\Form\ModeLivraisonType;
use CIELO\AdminBundle\Entity\ModePaiement;
use CIELO\AdminBundle\Form\ModePaiementType;
use CIELO\AdminBundle\Entity\ZoneGeographique;
use CIELO\AdminBundle\Form\ZoneGeographiqueType;
use CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique;
use CIELO\AdminBundle\Form\ModeLivraisonZoneGeographiqueType;

class CommandeController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modeLivraisonAction() {
        $modeLivraison = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraison')
                ->findAll();
        $newModeLivraison = new ModeLivraison();
        $form = $this->createForm(new ModeLivraisonType(), $newModeLivraison);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($newModeLivraison->getProprieteModeLivraisons() as $pml) {
                    $pml->setModeLivraison($newModeLivraison);
                    $em->persist($pml);
                }
                $em->persist($newModeLivraison);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_mode_livraison'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:modeLivraison.html.twig', array(
                    'form' => $form->createView(),
                    'modeLivraison' => $modeLivraison
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierModeLivraisonAction($id) {
        $modeLivraison = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraison')
                ->findAll();
        $newModeLivraison = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraison')
                ->find($id);
        $form = $this->createForm(new ModeLivraisonType(), $newModeLivraison);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newModeLivraison);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_mode_livraison'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:modeLivraison.html.twig', array(
                    'form' => $form->createView(),
                    'modeLivraison' => $modeLivraison
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerModeLivraisonAction($id) {
        $modeLivraison = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraison')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($modeLivraison);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_mode_livraison'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modePaiementAction() {
        $modePaiement = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModePaiement')
                ->findAll();
        $newModePaiement = new ModePaiement();
        $form = $this->createForm(new ModePaiementType(), $newModePaiement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($newModePaiement->getProprieteModePaiements() as $pmp) {
                    $pmp->setModePaiement($newModePaiement);
                    $em->persist($pmp);
                }
                $em->persist($newModePaiement);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_mode_paiement'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:modePaiement.html.twig', array(
                    'form' => $form->createView(),
                    'modePaiement' => $modePaiement
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierModePaiementAction($id) {
        $modePaiement = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModePaiement')
                ->findAll();
        $newModePaiement = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModePaiement')
                ->find($id);
        $form = $this->createForm(new ModePaiementType(), $newModePaiement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($newModePaiement->getProprieteModePaiements() as $pmp) {
                    $pmp->setModePaiement($newModePaiement);
                    $em->persist($pmp);
                }
                $em->persist($newModePaiement);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_mode_paiement'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:modePaiement.html.twig', array(
                    'form' => $form->createView(),
                    'modePaiement' => $modePaiement
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerModePaiementAction($id) {
        $modePaiement = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModePaiement')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($modePaiement);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_mode_paiement'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function zoneGeographiqueAction() {
        $zoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ZoneGeographique')
                ->findAll();
        $newZoneGeographique = new ZoneGeographique();
        $form = $this->createForm(new ZoneGeographiqueType(), $newZoneGeographique);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            //die(var_dump($newZoneGeographique));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newZoneGeographique);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_zone_geographique'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:zoneGeographique.html.twig', array(
                    'form' => $form->createView(),
                    'zoneGeographique' => $zoneGeographique,
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modeLivraisonZoneGeographiqueAction() {
        $modeLivraisonZoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraisonZoneGeographique')
                ->findAll();
        $newModeLivraisonZoneGeographique = new ModeLivraisonZoneGeographique();
        $form = $this->createForm(new ModeLivraisonZoneGeographiqueType(), $newModeLivraisonZoneGeographique);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newModeLivraisonZoneGeographique);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_mode_livraison_zone_geographique'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:modeLivraisonZoneGeographique.html.twig', array(
                    'form' => $form->createView(),
                    'modeLivraisonZoneGeographique' => $modeLivraisonZoneGeographique
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierModeLivraisonZoneGeographiqueAction($id) {
        $modeLivraisonZoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraisonZoneGeographique')
                ->findAll();
        $newModeLivraisonZoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraisonZoneGeographique')
                ->find($id);
        $form = $this->createForm(new ModeLivraisonZoneGeographiqueType(), $newModeLivraisonZoneGeographique);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newModeLivraisonZoneGeographique);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_mode_livraison_zone_geographique'));
            }
        }
        return $this->render('CIELOAdminBundle:Admin:modeLivraisonZoneGeographique.html.twig', array(
                    'form' => $form->createView(),
                    'modeLivraisonZoneGeographique' => $modeLivraisonZoneGeographique
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerTarifAction($id) {
        $modeLivraisonZoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ModeLivraisonZoneGeographique')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($modeLivraisonZoneGeographique);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_mode_livraison_zone_geographique'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierZoneGeographiqueAction($id) {
        $zoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ZoneGeographique')
                ->findAll();
        $newZoneGeographique = $this->getDoctrine()
                ->getmanager()
                ->getRepository('CIELOAdminBundle:ZoneGeographique')
                ->find($id);
        $form = $this->createForm(new ZoneGeographiqueType(), $newZoneGeographique);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($newZoneGeographique);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_zone_geographique'));
            }
        }

        return $this->render('CIELOAdminBundle:Admin:zoneGeographique.html.twig', array(
                    'form' => $form->createView(),
                    'zoneGeographique' => $zoneGeographique,
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerZoneGeographiqueAction($id) {
        $zoneGeographique = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:ZoneGeographique')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($zoneGeographique);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_zone_geographique'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function listCommandeAction($page) {        
        $em = $this->getDoctrine()
                ->getManager();
        $premiereCommande = ($page - 1) * 100;
        $totalCommandes = $em->getRepository("CIELOEcommerceBundle:Commande")->nbreCommande();
        $totalPages = ceil(intval($totalCommandes) / 100);
        $commandes = $em
                ->getRepository("CIELOEcommerceBundle:Commande")
                ->findBy(array(), array("dateDebutCommande"=>"DESC"), 100, $premiereCommande);
        return $this->render('CIELOAdminBundle:Admin:commandes.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'commandes' => $commandes
        ));
    }
    
    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerCommandeAction($id) {
        $commande = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEcommerceBundle:Commande')
                ->find($id);
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($commande);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_list_commande'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function produitCommandeAction($id) {
        $em = $this->getDoctrine()->getManager();
        $commande = $em
                ->getRepository('CIELOEcommerceBundle:Commande')
                ->find($id);
        $commande->setVue(true);
        $em->flush();
        $produitCommande = $em
                ->getRepository('CIELOEcommerceBundle:CommandeProduit')
                ->findByCommande($commande);
        return $this->render('CIELOAdminBundle:Admin:produitCommande.html.twig', array(
                    'produitCommande' => $produitCommande
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function terminerCommandeAction($id) {
        $em = $this->getDoctrine()->getEntityManager();
        $commande = $em
                ->getRepository('CIELOEcommerceBundle:Commande')
                ->find($id);
        $commande->setCommandeTerminer(true);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_list_commande'));
    }

}

?>
