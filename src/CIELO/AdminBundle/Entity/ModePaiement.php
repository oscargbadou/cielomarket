<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModePaiement
 *
 * @ORM\Table(name="cielo_mode_paiement")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ModePaiementRepository")
 */
class ModePaiement
{
    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ProprieteModePaiement", mappedBy="modePaiement", cascade={"persist", "remove"})
     */
    private $proprieteModePaiements;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EcommerceBundle\Entity\Commande", mappedBy="modePaiement", cascade={"persist", "remove"})
     */
    private $commandes;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ModePaiement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ModePaiement
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proprieteModePaiements = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add proprieteModePaiements
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteModePaiement $proprieteModePaiements
     * @return ModePaiement
     */
    public function addProprieteModePaiement(\CIELO\AdminBundle\Entity\ProprieteModePaiement $proprieteModePaiements)
    {
        $this->proprieteModePaiements[] = $proprieteModePaiements;
    
        return $this;
    }

    /**
     * Remove proprieteModePaiements
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteModePaiement $proprieteModePaiements
     */
    public function removeProprieteModePaiement(\CIELO\AdminBundle\Entity\ProprieteModePaiement $proprieteModePaiements)
    {
        $this->proprieteModePaiements->removeElement($proprieteModePaiements);
    }

    /**
     * Get proprieteModePaiements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprieteModePaiements()
    {
        return $this->proprieteModePaiements;
    }
    
    public function toJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Add commandes
     *
     * @param \CIELO\EcommerceBundle\Entity\Commande $commandes
     * @return ModePaiement
     */
    public function addCommande(\CIELO\EcommerceBundle\Entity\Commande $commandes)
    {
        $this->commandes[] = $commandes;
    
        return $this;
    }

    /**
     * Remove commandes
     *
     * @param \CIELO\EcommerceBundle\Entity\Commande $commandes
     */
    public function removeCommande(\CIELO\EcommerceBundle\Entity\Commande $commandes)
    {
        $this->commandes->removeElement($commandes);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommandes()
    {
        return $this->commandes;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return ModePaiement
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }
}