<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Jeu
 *
 * @ORM\Table(name="cielo_jeu")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\JeuRepository")
 */
class Jeu {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"remove", "persist"})
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="text")
     */
    private $code;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ProprieteJeu", mappedBy="jeu")
     */
    private $proprietes;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set nom
     *
     * @param string $nom
     * @return Jeu
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Jeu
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set icon
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $icon
     * @return Jeu
     */
    public function setIcon(\CIELO\EntrepriseBundle\Entity\Document $icon = null)
    {
        $this->icon = $icon;
    
        return $this;
    }

    /**
     * Get icon
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proprietes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add proprietes
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteJeu $proprietes
     * @return Jeu
     */
    public function addPropriete(\CIELO\AdminBundle\Entity\ProprieteJeu $proprietes)
    {
        $this->proprietes[] = $proprietes;
    
        return $this;
    }

    /**
     * Remove proprietes
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteJeu $proprietes
     */
    public function removePropriete(\CIELO\AdminBundle\Entity\ProprieteJeu $proprietes)
    {
        $this->proprietes->removeElement($proprietes);
    }

    /**
     * Get proprietes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprietes()
    {
        return $this->proprietes;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Jeu
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
}