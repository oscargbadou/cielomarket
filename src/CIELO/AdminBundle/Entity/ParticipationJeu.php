<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParticipationJeu
 *
 * @ORM\Table(name="cielo_participation_jeu")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ParticipationJeuRepository")
 */
class ParticipationJeu {
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\CampagneJeu", inversedBy="participationsJeu")
     */
    private $campagneJeu;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\UserBundle\Entity\Client")
     */
    private $client;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateParticipation", type="datetime")
     */
    private $dateParticipation;

    /**
     * @var string
     *
     * @ORM\Column(name="numGagnant", type="string", length=255, nullable=true)
     */
    private $numGagnant;

    /**
     * @var string
     *
     * @ORM\Column(name="gain", type="string", length=255, nullable=true)
     */
    private $gain;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    
    public function __construct() {
        $this->nombreDeparticipation = 0;
        $this->gain = "0";
    }
    /**
     * Set dateParticipation
     *
     * @param \DateTime $dateParticipation
     * @return ParticipationJeu
     */
    public function setDateParticipation($dateParticipation) {
        $this->dateParticipation = $dateParticipation;

        return $this;
    }

    /**
     * Get dateParticipation
     *
     * @return \DateTime 
     */
    public function getDateParticipation() {
        return $this->dateParticipation;
    }


    /**
     * Set numGagnant
     *
     * @param string $numGagnant
     * @return ParticipationJeu
     */
    public function setNumGagnant($numGagnant)
    {
        $this->numGagnant = $numGagnant;
    
        return $this;
    }

    /**
     * Get numGagnant
     *
     * @return string 
     */
    public function getNumGagnant()
    {
        return $this->numGagnant;
    }

    /**
     * Set client
     *
     * @param \CIELO\UserBundle\Entity\Client $client
     * @return ParticipationJeu
     */
    public function setClient(\CIELO\UserBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \CIELO\UserBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set gain
     *
     * @param string $gain
     * @return ParticipationJeu
     */
    public function setGain($gain)
    {
        $this->gain = $gain;
    
        return $this;
    }

    /**
     * Get gain
     *
     * @return string 
     */
    public function getGain()
    {
        return $this->gain;
    }

    /**
     * Set jeu
     *
     * @param \CIELO\AdminBundle\Entity\CampagneJeu $jeu
     * @return ParticipationJeu
     */
    public function setCampagneJeu(\CIELO\AdminBundle\Entity\CampagneJeu $CampagneJeu = null)
    {
        $this->campagneJeu = $CampagneJeu;
    
        return $this;
    }

    /**
     * Get jeu
     *
     * @return \CIELO\AdminBundle\Entity\CampagneJeu
     */
    public function getCampagneJeu()
    {
        return $this->campagneJeu;
    }

}