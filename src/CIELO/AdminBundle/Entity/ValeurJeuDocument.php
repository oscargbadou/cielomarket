<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jeu
 *
 * @ORM\Table(name="cielo_valeu_jeu_document")
 * @ORM\Entity()
 */
class ValeurJeuDocument {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document")
     */
    private $document;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ValeurProprieteJeu", inversedBy="valeurJeuDocuments")
     */
    private $valeurJeu;
    
    /**
     * @ORM\Column(name="gain", type="string", length=255)
     */
    private $gain;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gain
     *
     * @param string $gain
     * @return ValeurJeuDocument
     */
    public function setGain($gain)
    {
        $this->gain = $gain;
    
        return $this;
    }

    /**
     * Get gain
     *
     * @return string 
     */
    public function getGain()
    {
        return $this->gain;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return ValeurJeuDocument
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set valeurJeu
     *
     * @param \CIELO\AdminBundle\Entity\ValeurProprieteJeu $valeurJeu
     * @return ValeurJeuDocument
     */
    public function setValeurJeu(\CIELO\AdminBundle\Entity\ValeurProprieteJeu $valeurJeu = null)
    {
        $this->valeurJeu = $valeurJeu;
    
        return $this;
    }

    /**
     * Get valeurJeu
     *
     * @return \CIELO\AdminBundle\Entity\ValeurProprieteJeu 
     */
    public function getValeurJeu()
    {
        return $this->valeurJeu;
    }
}