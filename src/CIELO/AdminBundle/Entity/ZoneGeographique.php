<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ZoneGeographique
 *
 * @ORM\Table(name="cielo_zone_geographique")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ZoneGeographiqueRepository")
 */
class ZoneGeographique
{
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique", mappedBy="zoneGeographique", cascade={"remove"})
     */
    private $modeLivraisonZoneGeographiques;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ZoneGeographique
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ZoneGeographique
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modeLivraisonZoneGeographiques = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add modeLivraisonZoneGeographiques
     *
     * @param \CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique $modeLivraisonZoneGeographiques
     * @return ZoneGeographique
     */
    public function addModeLivraisonZoneGeographique(\CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique $modeLivraisonZoneGeographiques)
    {
        $this->modeLivraisonZoneGeographiques[] = $modeLivraisonZoneGeographiques;
    
        return $this;
    }

    /**
     * Remove modeLivraisonZoneGeographiques
     *
     * @param \CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique $modeLivraisonZoneGeographiques
     */
    public function removeModeLivraisonZoneGeographique(\CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique $modeLivraisonZoneGeographiques)
    {
        $this->modeLivraisonZoneGeographiques->removeElement($modeLivraisonZoneGeographiques);
    }

    /**
     * Get modeLivraisonZoneGeographiques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModeLivraisonZoneGeographiques()
    {
        return $this->modeLivraisonZoneGeographiques;
    }
    
    public function toJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }
}