<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValeurProprieteJeu
 *
 * @ORM\Table(name="cielo_valeur_propriete_jeu")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ValeurProprieteJeuRepository")
 */
class ValeurProprieteJeu
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ProprieteJeu")
     */
    private $proprieteJeu;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\CampagneJeu", inversedBy="valeurProprieteJeus")
     */
    private $campagneJeu;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ValeurJeuDocument", mappedBy="valeurJeu", cascade={"remove","persist"})
     */
    private $valeurJeuDocuments;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255, nullable=true)
     */
    private $valeur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ValeurProprieteJeu
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    
        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set proprieteJeu
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteJeu $proprieteJeu
     * @return ValeurProprieteJeu
     */
    public function setProprieteJeu(\CIELO\AdminBundle\Entity\ProprieteJeu $proprieteJeu = null)
    {
        $this->proprieteJeu = $proprieteJeu;
    
        return $this;
    }

    /**
     * Get proprieteJeu
     *
     * @return \CIELO\AdminBundle\Entity\ProprieteJeu 
     */
    public function getProprieteJeu()
    {
        return $this->proprieteJeu;
    }

    /**
     * Set campagneJeu
     *
     * @param \CIELO\AdminBundle\Entity\CampagneJeu $campagneJeu
     * @return ValeurProprieteJeu
     */
    public function setCampagneJeu(\CIELO\AdminBundle\Entity\CampagneJeu $campagneJeu = null)
    {
        $this->campagneJeu = $campagneJeu;
    
        return $this;
    }

    /**
     * Get campagneJeu
     *
     * @return \CIELO\AdminBundle\Entity\CampagneJeu 
     */
    public function getCampagneJeu()
    {
        return $this->campagneJeu;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->valeurJeuDocuments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add valeurJeuDocuments
     *
     * @param \CIELO\AdminBundle\Entity\ValeurJeuDocument $valeurJeuDocuments
     * @return ValeurProprieteJeu
     */
    public function addValeurJeuDocument(\CIELO\AdminBundle\Entity\ValeurJeuDocument $valeurJeuDocuments)
    {
        $this->valeurJeuDocuments[] = $valeurJeuDocuments;
    
        return $this;
    }

    /**
     * Remove valeurJeuDocuments
     *
     * @param \CIELO\AdminBundle\Entity\ValeurJeuDocument $valeurJeuDocuments
     */
    public function removeValeurJeuDocument(\CIELO\AdminBundle\Entity\ValeurJeuDocument $valeurJeuDocuments)
    {
        $this->valeurJeuDocuments->removeElement($valeurJeuDocuments);
    }

    /**
     * Get valeurJeuDocuments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValeurJeuDocuments()
    {
        return $this->valeurJeuDocuments;
    }
}