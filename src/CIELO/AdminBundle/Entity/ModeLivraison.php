<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ModeLivraison
 *
 * @ORM\Table(name="cielo_mode_livraison")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ModeLivraisonRepository")
 */
class ModeLivraison {

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ProprieteModeLivraison", mappedBy="modeLivraison", cascade={"persist", "remove"})
     */
    private $proprieteModeLivraisons;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ModeLivraison
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ModeLivraison
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->proprieteModeLivraisons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add proprieteModeLivraisons
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteModeLivraison $proprieteModeLivraisons
     * @return ModeLivraison
     */
    public function addProprieteModeLivraison(\CIELO\AdminBundle\Entity\ProprieteModeLivraison $proprieteModeLivraisons) {
        $this->proprieteModeLivraisons[] = $proprieteModeLivraisons;

        return $this;
    }

    /**
     * Remove proprieteModeLivraisons
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteModeLivraison $proprieteModeLivraisons
     */
    public function removeProprieteModeLivraison(\CIELO\AdminBundle\Entity\ProprieteModeLivraison $proprieteModeLivraisons) {
        $this->proprieteModeLivraisons->removeElement($proprieteModeLivraisons);
    }

    /**
     * Get proprieteModeLivraisons
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprieteModeLivraisons() {
        return $this->proprieteModeLivraisons;
    }

    public function toJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }


    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return ModeLivraison
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }
}