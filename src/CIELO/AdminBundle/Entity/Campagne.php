<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campagne
 *
 * @ORM\Table(name="cielo_campagne")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\CampagneRepository")
 */
class Campagne
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Entreprise", inversedBy="campagnes")
     */
    private $entreprise;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\CampagneJeu", mappedBy="campagne", cascade={"remove","persist"})
     */
    private $campagneJeux;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime")
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Campagne
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Campagne
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    
        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Campagne
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    
        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Campagne
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set entreprise
     *
     * @param \CIELO\EntrepriseBundle\Entity\Entreprise $entreprise
     * @return Campagne
     */
    public function setEntreprise(\CIELO\EntrepriseBundle\Entity\Entreprise $entreprise = null)
    {
        $this->entreprise = $entreprise;
    
        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CIELO\EntrepriseBundle\Entity\Entreprise 
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->campagneJeux = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add campagneJeux
     *
     * @param \CIELO\AdminBundle\Entity\CampagneJeu $campagneJeux
     * @return Campagne
     */
    public function addCampagneJeux(\CIELO\AdminBundle\Entity\CampagneJeu $campagneJeux)
    {
        $this->campagneJeux[] = $campagneJeux;
    
        return $this;
    }

    /**
     * Remove campagneJeux
     *
     * @param \CIELO\AdminBundle\Entity\CampagneJeu $campagneJeux
     */
    public function removeCampagneJeux(\CIELO\AdminBundle\Entity\CampagneJeu $campagneJeux)
    {
        $this->campagneJeux->removeElement($campagneJeux);
    }

    /**
     * Get campagneJeux
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampagneJeux()
    {
        return $this->campagneJeux;
    }
}