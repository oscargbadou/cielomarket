<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CIELO\EcommerceBundle\Utils\Utils;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Evenement
 *
 * @ORM\Table(name="cielo_evenement")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\EvenementRepository")
 */
class Evenement {

    /**
     * @ORM\ManyToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"remove", "persist"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ProprieteEvenement", mappedBy="evenement", cascade={"remove"})
     */
    private $proprieteEvenements;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ParticipationEvenement", mappedBy="evenement", cascade={"remove"})
     */
    private $participationEvenements;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime")
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=true)
     */
    private $lieu;

    /**
     * @var integer
     *
     * @ORM\Column(name="prixParticipation", type="integer", nullable=true)
     */
    private $prixParticipation;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Evenement
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Evenement
     */
    public function setDateDebut($dateDebut) {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut() {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Evenement
     */
    public function setDateFin($dateFin) {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin() {
        return $this->dateFin;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Evenement
     */
    public function setLieu($lieu) {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu() {
        return $this->lieu;
    }

    /**
     * Set prixParticipation
     *
     * @param integer $prixParticipation
     * @return Evenement
     */
    public function setPrixParticipation($prixParticipation) {
        $this->prixParticipation = $prixParticipation;

        return $this;
    }

    /**
     * Get prixParticipation
     *
     * @return integer 
     */
    public function getPrixParticipation() {
        return $this->prixParticipation;
    }

    /**
     * Constructor
     */
    public function __construct() {

        $this->proprieteEvenements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add proprieteEvenements
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteEvenement $proprieteEvenements
     * @return Evenement
     */
    public function addProprieteEvenement(\CIELO\AdminBundle\Entity\ProprieteEvenement $proprieteEvenements) {
        $this->proprieteEvenements[] = $proprieteEvenements;
        return $this;
    }

    /**
     * Remove proprieteEvenements
     *
     * @param \CIELO\AdminBundle\Entity\ProprieteEvenement $proprieteEvenements
     */
    public function removeProprieteEvenement(\CIELO\AdminBundle\Entity\ProprieteEvenement $proprieteEvenements) {
        $this->proprieteEvenements->removeElement($proprieteEvenements);
    }

    /**
     * Get proprieteEvenements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprieteEvenements() {
        return $this->proprieteEvenements;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Evenement
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Add documents
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $documents
     * @return Evenement
     */
    public function addDocument(\CIELO\EntrepriseBundle\Entity\Document $documents) {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $documents
     */
    public function removeDocument(\CIELO\EntrepriseBundle\Entity\Document $documents) {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments() {
        return $this->documents;
    }

    public function toJSON($toArray = false) {
        $images = Array();
        foreach ($this->documents as $img) {
            if ($img) {
                $images[] = $img->getLink();
            }
        }
        if(count($images)>0){
            $image=$images[0];
        }else {
            $image= "http://" . $_SERVER["HTTP_HOST"] . "/cielo-market-build2040308/web/bundles/cielouser/images/deux.png";
        }
        $dateDebut = $this->dateDebut;
        $dateFin = $this->dateFin;
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "dateDebut" => $dateDebut->format('Y-m-d H:i:s'),
            "dateFin" => $dateFin->format('Y-m-d H:i:s'),
            "lieu" => $this->lieu,
            "image" => $image,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $images = Array();
        foreach ($this->documents as $img) {
            if ($img) {
                $images[] = "http://" . $_SERVER["HTTP_HOST"] . "/CieloMarket/web/" . $img->getWebPath();
            }
        }
        $dateDebut = $this->dateDebut;
        $dateFin = $this->dateFin;
        $proprietes = $this->proprieteEvenements;
        if (count($proprietes) != 0) {
            foreach ($proprietes as $p) {
                $proprieteArray[] = $p->toJSON(true);
            }
        } else {
            $proprieteArray = array();
        }

        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "dateDebut" => $dateDebut->format('Y-m-d H:i:s'),
            "dateFin" => $dateFin->format('Y-m-d H:i:s'),
            "lieu" => $this->lieu,
            "proprietes" => $proprieteArray,
            "images"=>$images,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }


    /**
     * Add participationEvenements
     *
     * @param \CIELO\AdminBundle\Entity\ParticipationEvenement $participationEvenements
     * @return Evenement
     */
    public function addParticipationEvenement(\CIELO\AdminBundle\Entity\ParticipationEvenement $participationEvenements)
    {
        $this->participationEvenements[] = $participationEvenements;
    
        return $this;
    }

    /**
     * Remove participationEvenements
     *
     * @param \CIELO\AdminBundle\Entity\ParticipationEvenement $participationEvenements
     */
    public function removeParticipationEvenement(\CIELO\AdminBundle\Entity\ParticipationEvenement $participationEvenements)
    {
        $this->participationEvenements->removeElement($participationEvenements);
    }

    /**
     * Get participationEvenements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipationEvenements()
    {
        return $this->participationEvenements;
    }
}