<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModeLivraisonZoneGeographique
 *
 * @ORM\Table(name="cielo_mode_livraison_zone_geographique")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographiqueRepository")
 */
class ModeLivraisonZoneGeographique {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ModeLivraison", inversedBy="modeLivraisonZoneGeographiques")
     */
    private $modeLivraison;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ZoneGeographique")
     */
    private $zoneGeographique;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="prix", type="integer")
     * @Assert\Range(
     * min=0,
     * minMessage = "Le prix ne peut pas être négatif"
     * )
     */
    private $prix;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     * @return ModeLivraisonZoneGeographique
     */
    public function setPrix($prix) {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer 
     */
    public function getPrix() {
        return $this->prix;
    }

    /**
     * Set modeLivraison
     *
     * @param \CIELO\AdminBundle\Entity\ModeLivraison $modeLivraison
     * @return ModeLivraisonZoneGeographique
     */
    public function setModeLivraison(\CIELO\AdminBundle\Entity\ModeLivraison $modeLivraison = null) {
        $this->modeLivraison = $modeLivraison;

        return $this;
    }

    /**
     * Get modeLivraison
     *
     * @return \CIELO\AdminBundle\Entity\ModeLivraison 
     */
    public function getModeLivraison() {
        return $this->modeLivraison;
    }

    /**
     * Set zoneGeographique
     *
     * @param \CIELO\AdminBundle\Entity\ZoneGeographique $zoneGeographique
     * @return ModeLivraisonZoneGeographique
     */
    public function setZoneGeographique(\CIELO\AdminBundle\Entity\ZoneGeographique $zoneGeographique = null) {
        $this->zoneGeographique = $zoneGeographique;

        return $this;
    }

    /**
     * Get zoneGeographique
     *
     * @return \CIELO\AdminBundle\Entity\ZoneGeographique 
     */
    public function getZoneGeographique() {
        return $this->zoneGeographique;
    }

}