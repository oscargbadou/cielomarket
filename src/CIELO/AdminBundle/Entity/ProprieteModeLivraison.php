<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProprieteModeLivraison
 *
 * @ORM\Table(name="cielo_propriete_mode_livraison")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ProprieteModeLivraisonRepository")
 */
class ProprieteModeLivraison
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ModeLivraison")
     */
    private $modeLivraison;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255)
     */
    private $valeur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ProprieteModeLivraison
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ProprieteModeLivraison
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    
        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set modeLivraison
     *
     * @param \CIELO\AdminBundle\Entity\ModeLivraison $modeLivraison
     * @return ProprieteModeLivraison
     */
    public function setModeLivraison(\CIELO\AdminBundle\Entity\ModeLivraison $modeLivraison = null)
    {
        $this->modeLivraison = $modeLivraison;
    
        return $this;
    }

    /**
     * Get modeLivraison
     *
     * @return \CIELO\AdminBundle\Entity\ModeLivraison 
     */
    public function getModeLivraison()
    {
        return $this->modeLivraison;
    }
}