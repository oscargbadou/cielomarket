<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParticipationEvenement
 *
 * @ORM\Table(name="cielo_participation_evenement")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ParticipationEvenementRepository")
 */
class ParticipationEvenement {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\Evenement", inversedBy="participationEvenements")
     */
    private $evenement;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\UserBundle\Entity\User")
     */
    private $utilisateur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="vue", type="boolean")
     */
    private $vue;

    public function __construct() {
        $this->date = new \DateTime();
        $this->vue = false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set evenement
     *
     * @param \CIELO\AdminBundle\Entity\Evenement $evenement
     * @return ParticipationEvenement
     */
    public function setEvenement(\CIELO\AdminBundle\Entity\Evenement $evenement = null) {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \CIELO\AdminBundle\Entity\Evenement 
     */
    public function getEvenement() {
        return $this->evenement;
    }

    /**
     * Set utilisateur
     *
     * @param \CIELO\UserBundle\Entity\User $utilisateur
     * @return ParticipationEvenement
     */
    public function setUtilisateur(\CIELO\UserBundle\Entity\User $utilisateur = null) {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \CIELO\UserBundle\Entity\User 
     */
    public function getUtilisateur() {
        return $this->utilisateur;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ParticipationEvenement
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set vue
     *
     * @param boolean $vue
     * @return ParticipationEvenement
     */
    public function setVue($vue)
    {
        $this->vue = $vue;
    
        return $this;
    }

    /**
     * Get vue
     *
     * @return boolean 
     */
    public function getVue()
    {
        return $this->vue;
    }
}