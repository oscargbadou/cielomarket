<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ProprieteJeu
 *
 * @ORM\Table(name="cielo_propriete_jeu")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ProprieteJeuRepository")
 */
class ProprieteJeu {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\Jeu", inversedBy="proprietes")
     */
    private $jeu;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=true)
     */
    private $entity;
    
    /**
     * @var string
     *
     * @ORM\Column(name="choix", type="string", length=255, nullable=true)
     */
    private $choix;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ProprieteJeu
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ProprieteJeu
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProprieteJeu
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set jeu
     *
     * @param \CIELO\AdminBundle\Entity\Jeu $jeu
     * @return ProprieteJeu
     */
    public function setJeu(\CIELO\AdminBundle\Entity\Jeu $jeu = null) {
        $this->jeu = $jeu;

        return $this;
    }

    /**
     * Get jeu
     *
     * @return \CIELO\AdminBundle\Entity\Jeu 
     */
    public function getJeu() {
        return $this->jeu;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * @return ProprieteJeu
     */
    public function setEntity($entity) {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string 
     */
    public function getEntity() {
        return $this->entity;
    }


    /**
     * Set choix
     *
     * @param string $choix
     * @return ProprieteJeu
     */
    public function setChoix($choix)
    {
        $this->choix = $choix;
    
        return $this;
    }

    /**
     * Get choix
     *
     * @return string 
     */
    public function getChoix()
    {
        return $this->choix;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return ProprieteJeu
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }
}