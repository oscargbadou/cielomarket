<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pub
 *
 * @ORM\Table(name="cielo_pub")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\PubRepository")
 */
class Pub {

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255, nullable=true)
     */
    private $lien;

    /**
     * @var string
     *
     * @ORM\Column(name="couleurArrierePlan", type="string", length=255, nullable=true)
     */
    private $couleurArrierePlan;

    /**
     * @var boolean
     *
     * @ORM\Column(name="panelGauche", type="boolean", nullable=true)
     */
    private $panelGauche;

    /**
     * @var boolean
     *
     * @ORM\Column(name="panelDroit", type="boolean", nullable=true)
     */
    private $panelDroit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {
        $this->panelDroit = true;
        $this->panelGauche = true;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Pub
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set lien
     *
     * @param string $lien
     * @return Pub
     */
    public function setLien($lien) {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string 
     */
    public function getLien() {
        return $this->lien;
    }

    /**
     * Set couleurArrierePlan
     *
     * @param string $couleurArrierePlan
     * @return Pub
     */
    public function setCouleurArrierePlan($couleurArrierePlan) {
        $this->couleurArrierePlan = $couleurArrierePlan;

        return $this;
    }

    /**
     * Get couleurArrierePlan
     *
     * @return string 
     */
    public function getCouleurArrierePlan() {
        return $this->couleurArrierePlan;
    }

    /**
     * Set panelGauche
     *
     * @param boolean $panelGauche
     * @return Pub
     */
    public function setPanelGauche($panelGauche) {
        $this->panelGauche = $panelGauche;

        return $this;
    }

    /**
     * Get panelGauche
     *
     * @return boolean 
     */
    public function getPanelGauche() {
        return $this->panelGauche;
    }

    /**
     * Set panelDroit
     *
     * @param boolean $panelDroit
     * @return Pub
     */
    public function setPanelDroit($panelDroit) {
        $this->panelDroit = $panelDroit;

        return $this;
    }

    /**
     * Get panelDroit
     *
     * @return boolean 
     */
    public function getPanelDroit() {
        return $this->panelDroit;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Pub
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return Pub
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null)
    {
        $this->document = $document;
    
        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }
}