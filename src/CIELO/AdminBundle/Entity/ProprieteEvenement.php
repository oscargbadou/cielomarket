<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ProprieteEvenement
 *
 * @ORM\Table(name="cielo_propriete_evenement")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ProprieteEvenementRepository")
 */
class ProprieteEvenement {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\Evenement", inversedBy="proprieteEvenements")
     */
    private $evenement;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $valeur;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ProprieteEvenement
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ProprieteEvenement
     */
    public function setValeur($valeur) {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur() {
        return $this->valeur;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProprieteEvenement
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set evenement
     *
     * @param \CIELO\AdminBundle\Entity\Evenement $evenement
     * @return ProprieteEvenement
     */
    public function setEvenement(\CIELO\AdminBundle\Entity\Evenement $evenement = null) {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \CIELO\AdminBundle\Entity\Evenement 
     */
    public function getEvenement() {
        return $this->evenement;
    }

    public function toJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "valeur" => $this->valeur,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "valeur" => $this->valeur,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

}