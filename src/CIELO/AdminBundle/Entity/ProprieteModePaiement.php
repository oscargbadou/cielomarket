<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * ProprieteModePaiement
 *
 * @ORM\Table(name="cielo_propriete_mode_paiement")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\ProprieteModePaiementRepository")
 */
class ProprieteModePaiement
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\ModePaiement")
     */
    private $modePaiement;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255)
     */
    private $valeur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ProprieteModePaiement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ProprieteModePaiement
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    
        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set modePaiement
     *
     * @param \CIELO\AdminBundle\Entity\ModePaiement $modePaiement
     * @return ProprieteModePaiement
     */
    public function setModePaiement(\CIELO\AdminBundle\Entity\ModePaiement $modePaiement = null)
    {
        $this->modePaiement = $modePaiement;
    
        return $this;
    }

    /**
     * Get modePaiement
     *
     * @return \CIELO\AdminBundle\Entity\ModePaiement 
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }
}