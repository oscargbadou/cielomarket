<?php

namespace CIELO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CIELO\EcommerceBundle\Utils\Utils;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CampagneJeu
 *
 * @ORM\Table(name="cielo_campagne_jeu")
 * @ORM\Entity(repositoryClass="CIELO\AdminBundle\Entity\CampagneJeuRepository")
 */
class CampagneJeu {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\Campagne", inversedBy="campagneJeux")
     */
    private $campagne;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ValeurProprieteJeu", mappedBy="campagneJeu", cascade={"remove"})
     */
    private $valeurProprieteJeux;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\AdminBundle\Entity\Jeu")
     */
    private $jeu;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\ParticipationJeu", mappedBy="campagneJeu", cascade={"remove","persist"})
     */
    private $participationsJeu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $nom;
    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document")
     */
    private $icone;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set campagne
     *
     * @param \CIELO\AdminBundle\Entity\Campagne $campagne
     * @return CampagneJeu
     */
    public function setCampagne(\CIELO\AdminBundle\Entity\Campagne $campagne = null) {
        $this->campagne = $campagne;

        return $this;
    }

    /**
     * Get campagne
     *
     * @return \CIELO\AdminBundle\Entity\Campagne 
     */
    public function getCampagne() {
        return $this->campagne;
    }

    /**
     * Set jeu
     *
     * @param \CIELO\AdminBundle\Entity\Jeu $jeu
     * @return CampagneJeu
     */
    public function setJeu(\CIELO\AdminBundle\Entity\Jeu $jeu = null) {
        $this->jeu = $jeu;

        return $this;
    }

    /**
     * Get jeu
     *
     * @return \CIELO\AdminBundle\Entity\Jeu 
     */
    public function getJeu() {
        return $this->jeu;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->valeurProprieteJeux = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add valeurProprieteJeus
     *
     * @param \CIELO\AdminBundle\Entity\ValeurProprieteJeu $valeurProprieteJeus
     * @return CampagneJeu
     */
    public function addValeurProprieteJeux(\CIELO\AdminBundle\Entity\ValeurProprieteJeu $valeurProprieteJeux) {
        $this->valeurProprieteJeux[] = $valeurProprieteJeux;

        return $this;
    }

    /**
     * Remove valeurProprieteJeus
     *
     * @param \CIELO\AdminBundle\Entity\ValeurProprieteJeu $valeurProprieteJeus
     */
    public function removeValeurProprieteJeux(\CIELO\AdminBundle\Entity\ValeurProprieteJeu $valeurProprieteJeux) {
        $this->valeurProprieteJeux->removeElement($valeurProprieteJeux);
    }

    /**
     * Get valeurProprieteJeus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValeurProprieteJeux() {
        return $this->valeurProprieteJeux;
    }

    /**
     * Add participationsJeu
     *
     * @param \CIELO\AdminBundle\Entity\ParticipationJeu $participationsJeu
     * @return CampagneJeu
     */
    public function addParticipationsJeu(\CIELO\AdminBundle\Entity\ParticipationJeu $participationsJeu) {
        $this->participationsJeu[] = $participationsJeu;

        return $this;
    }

    /**
     * Remove participationsJeu
     *
     * @param \CIELO\AdminBundle\Entity\ParticipationJeu $participationsJeu
     */
    public function removeParticipationsJeu(\CIELO\AdminBundle\Entity\ParticipationJeu $participationsJeu) {
        $this->participationsJeu->removeElement($participationsJeu);
    }

    /**
     * Get participationsJeu
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipationsJeu() {
        return $this->participationsJeu;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return CampagneJeu
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    public function toJSON($toArray = false) {
        $array = array(
            "id" => $this->id,
            "nom" => $this->nom,
            "description" => $this->getJeu()->getDescription(),
            "urlImage" => $this->getJeu()->getIcon()->getLink(),
            "urlJeu" => "http://" . $_SERVER["HTTP_HOST"] . "/cielo-market-build2040308/web/app_dev.php/jeux/mobile/" . $this->id,
            "entreprise" => array(
                "id" => $this->getCampagne()->getEntreprise()->getId(),
                "nom" => $this->getCampagne()->getEntreprise()->getNom(),
            )
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }


    /**
     * Set description
     *
     * @param string $description
     * @return CampagneJeu
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set icone
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $icone
     * @return CampagneJeu
     */
    public function setIcone(\CIELO\EntrepriseBundle\Entity\Document $icone = null)
    {
        $this->icone = $icone;
    
        return $this;
    }

    /**
     * Get icone
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getIcone()
    {
        return $this->icone;
    }
}