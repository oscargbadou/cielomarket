<?php

namespace CIELO\AdminBundle\Services;

class GlobalVariablesAdmin extends \Twig_Extension {

    private $doctrine;

    public function __construct($doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getVarsAdmin() {
        $commande = $this->doctrine
                ->getManager()
                ->getRepository("CIELOEcommerceBundle:Commande")
                ->findBy(array(), array("dateDebutCommande" => "DESC"), 6, 0);
        $nbreCommandeNonVue = $this->doctrine->getManager()->getRepository("CIELOEcommerceBundle:Commande")->nbreCommandeNonVue();
        $now = new \DateTime();
        $evenementsEnCoursEtFutur = $this->doctrine
                ->getManager()
                ->getRepository("CIELOAdminBundle:Evenement")
                ->createQueryBuilder("e")
                ->where("e.dateDebut <= :now")
                ->where("e.dateFin >= :now")
                ->setParameter("now", $now)
                ->getQuery()
                ->getResult();
        $nbreParticipationEvenementNonVueTotal = 0;
        if($evenementsEnCoursEtFutur) {
            foreach ($evenementsEnCoursEtFutur as $e) {
                $compteur = 0;
                foreach ($e->getParticipationEvenements() as $pe) {
                    if ($pe->getVue() == false) {
                        $compteur++;
                    }
                }
                if ($compteur != 0) {
                    $participationEvenementNonVue[] = array(
                        'evenement' => $e,
                        'nbreParticipationEvenementNonVue' => $compteur
                    );
                    $nbreParticipationEvenementNonVueTotal+=$compteur;
                }
            }
        }
        
        $nbreRendezVousNonVue = $this->doctrine->getManager()
                ->getRepository("CIELOEntrepriseBundle:RendezVous")
                ->createQueryBuilder("c")
                ->select("COUNT(c)")
                ->where("c.vue = false")
                ->getQuery()
                ->getSingleScalarResult();

        return array(
            'commande' => $commande,
            'nbreCommandeNonVue' => $nbreCommandeNonVue,
            'participationEvenementNonVue' => $nbreParticipationEvenementNonVueTotal>0?$participationEvenementNonVue:null,
            'nbreParticipationEvenementNonVueTotal' => $nbreParticipationEvenementNonVueTotal,
            'nbreRendezVousNonVue' => $nbreRendezVousNonVue
        );
    }

    public function getFunctions() {
        return array(
            "getVarsAdmin" => new \Twig_Function_Method($this, "getVarsAdmin")
        );
    }

    public function getName() {
        return "cieloGlobalsAdmin";
    }

}

