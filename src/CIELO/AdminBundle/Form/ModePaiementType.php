<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\EntrepriseBundle\Form\DocumentFileType;

class ModePaiementType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('description')
                ->add('document', new DocumentFileType())
                ->add('proprieteModePaiements', 'collection', array(
                    'type' => new ProprieteModePaiementType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\ModePaiement'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_modepaiement';
    }

}
