<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\EntrepriseBundle\Form\DocumentFileType;

class ModeLivraisonType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'required' => true
                ))
                ->add('description', 'textarea', array(
                    'required' => false
                ))
                ->add('proprieteModeLivraisons', 'collection', array(
                    'type' => new ProprieteModeLivraisonType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
                ->add('document', new DocumentFileType())
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\ModeLivraison'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_modelivraison';
    }

}
