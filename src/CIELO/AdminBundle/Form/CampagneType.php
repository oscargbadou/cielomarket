<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use \CIELO\EntrepriseBundle\Form\DocumentType;

class CampagneType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('dateDebut', "date", array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                ))
                ->add('dateFin', "date", array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                ))
                ->add('description', 'textarea', array(
                    'required'=>true
                ))
                ->add('entreprise', "entity", array(
                    'class' => "CIELOEntrepriseBundle:Entreprise",
                    'property' => "nom",
                    'empty_value' => 'Choisir une entreprise',
                    'required' => true,
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\Campagne'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_admin_campagne_form';
    }

}
