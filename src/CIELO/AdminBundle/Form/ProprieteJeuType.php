<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use \CIELO\EntrepriseBundle\Form\DocumentType;

class ProprieteJeuType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('alias', "text", array(
                    "attr"=>array(
                        'class'=> "propriete_jeu"
                    )
                ))
                ->add('type', "choice", array(
                    "choices" => array(
                        "string" => "String",
                        "int" => "Int",
                        "choice" => "Choice",
                        "entity" => "Entity",
                        "image" => "Image",
                    ),
                    "attr"=>array(
                        'class'=> "propriete_select_choice"
                    )
                ))
                
                ->add('entity', "choice", array(
                    "choices" => array(
                        "CIELOEntrepriseBundle:Produit" => "Produit",
                    ),
                    "attr"=>array(
                        'class'=> "propriete_input_entity"
                    )
                ))
                ->add('choix', "text", array(
                    "required" => false,
                    "attr"=>array(
                        'class'=> "propriete_input_choice"
                    )
                ))
                ->add('description', "text")

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\ProprieteJeu'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_admin_prpriete_jeu_form';
    }

}
