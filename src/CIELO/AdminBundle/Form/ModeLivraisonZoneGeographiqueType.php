<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ModeLivraisonZoneGeographiqueType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('prix')
                ->add('modeLivraison', 'entity', array(
                    'class' => "CIELOAdminBundle:ModeLivraison",
                    'property' => "nom",
                    'empty_value' => 'Mode de livraison',
                ))
                ->add('zoneGeographique', 'entity', array(
                    'class' => "CIELOAdminBundle:ZoneGeographique",
                    'property' => "nom",
                    'empty_value' => 'Zone géographique',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\ModeLivraisonZoneGeographique'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_modelivraisonzonegeographique';
    }

}
