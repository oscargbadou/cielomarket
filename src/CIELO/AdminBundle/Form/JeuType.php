<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\EntrepriseBundle\Form\DocumentFileType;

class JeuType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'required' => true
                ))
                ->add('icon', new DocumentFileType())
                ->add('code')
                ->add('description', 'textarea', array(
                    'required' => false
                ))
                ->add('proprietes', "collection", array(
                    "type" => new ProprieteJeuType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\Jeu'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_admin_jeux_form';
    }

}
