<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\EntrepriseBundle\Entity\Document;
use CIELO\EntrepriseBundle\Form\DocumentType;

class EvenementType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'required'=>true
                ))
                ->add('description', 'textarea', array(
                    'required'=>false
                ))
                ->add('dateDebut', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                ))
                ->add('dateFin', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                ))
                ->add('lieu')
                ->add('prixParticipation')
                ->add('documents', 'collection', array(
                    'type' => new DocumentType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
                ->add('proprieteEvenements', 'collection', array(
                    'type' => new ProprieteEvenementType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\Evenement'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_evenement';
    }

}
