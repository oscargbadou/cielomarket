<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\AdminBundle\Form\ValeurProprieteJeuType;

class CampagneJeuType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('campagne', 'entity', array(
                    'class' => "CIELOAdminBundle:Campagne",
                    'property' => "nom",
                    'empty_value' => 'Choisissez une campagne',
                ))
                ->add('jeu', 'entity', array(
                    'class' => "CIELOAdminBundle:Jeu",
                    'property' => "nom",
                    'empty_value' => 'Choisissez un jeu',
                ))
                ->add('valeurProprieteJeus', 'collection', array(
                    'type' => new ValeurProprieteJeuType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\CampagneJeu'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_campagnejeu';
    }

}
