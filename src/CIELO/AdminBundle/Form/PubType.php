<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\EntrepriseBundle\Form\DocumentFileType;
class PubType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('lien')
            ->add('couleurArrierePlan', 'text', array(
                'attr'=>array(
                    'placeholder'=>'#ffddee'
                )
            ))
            ->add('panelGauche')
            ->add('panelDroit')
            //->add('date')
            ->add('document', new DocumentFileType())
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\Pub'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cielo_adminbundle_pub';
    }
}
