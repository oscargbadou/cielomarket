<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ZoneGeographiqueType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('type', 'choice', array(
                    'choices' => array(
                        'departement' => 'Département',
                        'commune' => 'Commune',
                        'arrondissement' => 'Arrondissement',
                        'quartier' => 'Quartier',
                    ),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\ZoneGeographique'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_zonegeographique';
    }

}
