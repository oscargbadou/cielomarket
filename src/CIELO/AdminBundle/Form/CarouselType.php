<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CIELO\EntrepriseBundle\Form\DocumentFileType;

class CarouselType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('textAlternatif', 'text', array(
                'required'=>false
            ))
            ->add('description', 'text', array(
                'required'=>true
            ))
            ->add('actif', 'checkbox', array(
                'required'=>false
            ))
            ->add('document', new DocumentFileType())
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\Carousel'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cielo_adminbundle_carousel';
    }
}
