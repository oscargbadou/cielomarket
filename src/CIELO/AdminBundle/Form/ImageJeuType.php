<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use \CIELO\EntrepriseBundle\Form\DocumentType;

class ImageJeuType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('gain', 'text')
                ->add('file','file')

        ;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_admin_image_jeu_type_form';
    }

}
