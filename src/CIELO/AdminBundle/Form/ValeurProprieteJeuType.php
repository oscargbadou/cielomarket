<?php

namespace CIELO\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ValeurProprieteJeuType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('proprieteJeu', 'entity', array(
                    'class' => "CIELOAdminBundle:ProprieteJeu",
                    'property' => "nom",
                    'empty_value' => 'Choisissez une propriété de jeu',
                ))
                ->add('valeur')
                //->add('campagneJeu')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\AdminBundle\Entity\ValeurProprieteJeu'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_adminbundle_valeurproprietejeu';
    }

}
