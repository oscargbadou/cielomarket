<?php

namespace CIELO\AdminEntrepriseBundle\Services;

class GlobalVariables extends \Twig_Extension {

    private $doctrine;

    public function __construct($doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getVarsAdminEntreprise() {
        $id = $_SESSION['id_entreprise_courant'];
        $entrepriseCourant = $this->doctrine
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Entreprise')
                ->find($id);
        return array(
            'session' => $_SESSION,
            'entrepriseCourant' => $entrepriseCourant
        );
    }

    public function getFunctions() {
        return array(
            "getVarsAdminEntreprise" => new \Twig_Function_Method($this, "getVarsAdminEntreprise")
        );
    }

    public function getName() {
        return "cieloGlobalsAdminEntreprise";
    }

}

