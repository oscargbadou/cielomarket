<?php

namespace CIELO\AdminEntrepriseBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccueilController extends Controller{
    
    
    public function accueilAction($id) {
        $_SESSION['id_entreprise_courant']=  $id;
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $nbreCategorieEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:Categorie")->nbreCategorieEntreprise($id);
        $nbreProduitEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitEntreprise($id);
        $nbrePromotionEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:Promotion")->nbrePromotionEntreprise($id);
        $nbreProduitPromotionEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:PromotionProduit")->nbreProduitPromotionEntreprise($id);
        return $this->render("CIELOAdminEntrepriseBundle:Accueil:index.html.twig", array(
                    'nbreCategorie' => $nbreCategorieEntreprise,
                    'nbreProduit' => $nbreProduitEntreprise,
                    'nbrePromotion' => $nbrePromotionEntreprise,
                    'nbreProduitPromotion' => $nbreProduitPromotionEntreprise
        ));
    }
}
