<?php

namespace CIELO\AdminEntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\Produit;
use CIELO\EntrepriseBundle\Entity\Modele;
use CIELO\EntrepriseBundle\Form\ModeleType;
use CIELO\EntrepriseBundle\Form\DocumentType;
use CIELO\EntrepriseBundle\Form\ProprieteProduitType;

class ProduitController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function produitsAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premierProduit = ($page - 1) * 100;
        $totalProduits = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitEntreprise($_SESSION['id_entreprise_courant']);
        $totalPages = ceil(intval($totalProduits) / 100);
        $produits = $em
                ->getRepository("CIELOEntrepriseBundle:Produit")
                ->createQueryBuilder("p")
                ->leftJoin("p.categorie", "c")
                ->leftJoin("c.entreprise", "e")
                ->where("e.id = :entreprise_id")
                ->setParameter("entreprise_id", $_SESSION['id_entreprise_courant'])
                ->setFirstResult($premierProduit)
                ->setMaxResults(100)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOAdminEntrepriseBundle:Produit:produits.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'produits' => $produits,
                    'host' => $_SERVER["HTTP_HOST"]
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerModeleProduitAction() {
        $_SESSION["id_entreprise"] = $_SESSION["id_entreprise_courant"];
        $modeleProduit = new Modele();
        $form = $this->createForm(new ModeleType(), $modeleProduit);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($modeleProduit->getProprieteProduits() as $pro) {
                    $pro->setModele($modeleProduit);
                    $em->persist($pro);
                }
                $modeleProduit->setPrixPromo($modeleProduit->getPrix());
                $modeleProduit->getProduit()->setPrixParDefaut($modeleProduit->getPrix());
                $em->persist($modeleProduit);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Produit:creerModeleProduit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierAction($id) {
        $_SESSION["id_entreprise"] = $_SESSION["id_entreprise_courant"];
        $modeleProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Modele')
                ->find($id);
        if ($modeleProduit === null) {
            throw $this->createNotFoundException('Ce produit n\'existe pas encore');
        }
        $form = $this->createForm(new ModeleType(), $modeleProduit);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($modeleProduit->getProprieteProduits() as $pro) {
                    $pro->setModele($modeleProduit);
                    $em->persist($pro);
                }
                $modeleProduit->setPrixPromo($modeleProduit->getPrix());
                $modeles = $modeleProduit->getProduit()->getModeles();
                foreach ($modeles as $m) {
                    if ($m->prixPromo() < $modeleProduit->getProduit()->getPrixParDefaut()) {
                        $modeleProduit->getProduit()->setPrixParDefaut($modeleProduit->getPrix());
                    }
                }
                $em->persist($modeleProduit);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Produit:creerModeleProduit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction($id) {
        $produit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Produit')
                ->find($id);
        if ($produit === null) {
            throw $this->createNotFoundException('Ce produit n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($produit);
        $em->flush();
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function ajouterModeleAction($id) {
        $newModele = new Modele();
        $produit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Produit')
                ->find($id);
        $form = $this->createFormBuilder($newModele)
                ->add('prix')
                ->add('documents', 'collection', array(
                    'type' => new DocumentType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
                ->add('proprieteProduits', 'collection', array(
                    'type' => new ProprieteProduitType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($newModele->getProprieteProduits() as $pro) {
                    $pro->setModele($newModele);
                    $em->persist($pro);
                }
                $newModele->setPrixPromo($newModele->getPrix());
                if ($newModele->getPrixPromo() < $newModele->getProduit()->getPrixParDefaut()) {
                    $newModele->getProduit()->setPrixParDefaut($newModele->getPrix());
                }
                $newModele->setProduit($produit);
                $em->persist($newModele);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Produit:ajouterModele.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modeleProduitAction($id) {
        $produit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Produit')
                ->find($id);
        $modeles = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Modele')
                ->findByProduit($produit);
        return $this->render('CIELOAdminEntrepriseBundle:Produit:modeleProduit.html.twig', array(
                    'modeles' => $modeles,
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierModeleProduitAction($id) {
        $modele = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Modele')
                ->find($id);
        $form = $this->createFormBuilder($modele)
                ->add('prix')
                ->add('documents', 'collection', array(
                    'type' => new DocumentType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
                ->add('proprieteProduits', 'collection', array(
                    'type' => new ProprieteProduitType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                foreach ($modele->getProprieteProduits() as $pro) {
                    $pro->setModele($modele);
                    $em->persist($pro);
                }
                if ($modele->prixPromo() < $modele->getProduit()->getPrixParDefaut()) {
                    $modele->getProduit()->setPrixParDefaut($modele->getPrix());
                }
                $em->persist($modele);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Produit:ajouterModele.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerModeleProduitAction($id) {
        $modele = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Modele')
                ->find($id);
        if ($modele === null) {
            throw $this->createNotFoundException('Ce produit n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($modele);
        $em->flush();
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit'));
    }

}

?>
