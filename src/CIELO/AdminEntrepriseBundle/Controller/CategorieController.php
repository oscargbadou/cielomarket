<?php

namespace CIELO\AdminEntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\Categorie;
use CIELO\EntrepriseBundle\Entity\ProprieteCategorie;
use CIELO\EntrepriseBundle\Form\CategorieType;
use CIELO\EntrepriseBundle\Form\ProprieteCategorieType;

class CategorieController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function categoriesAction($page) {
        $em = $this->getDoctrine()
                ->getManager();
        $premiereCatgorie = ($page - 1) * 100;
        $totalCategories = $em->getRepository("CIELOEntrepriseBundle:Categorie")->nbreCategorieEntreprise($_SESSION['id_entreprise_courant']);
        $totalPages = ceil(intval($totalCategories) / 100);
        $categories = $em
                ->getRepository("CIELOEntrepriseBundle:Categorie")
                ->createQueryBuilder("c")
                ->leftJoin("c.entreprise", "e")
                ->where("e.id = :entreprise_id")
                ->setParameter("entreprise_id", $_SESSION['id_entreprise_courant'])
                ->setFirstResult($premiereCatgorie)
                ->setMaxResults(100)
                ->getQuery()
                ->getResult();
        return $this->render('CIELOAdminEntrepriseBundle:Categorie:categorie.html.twig', array(
                    'page' => $page,
                    'nbrTotalPages' => $totalPages,
                    'categories' => $categories
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerAction() {
        $_SESSION['id_entreprise'] = $_SESSION['id_entreprise_courant'];
        $categorie = new Categorie();
        $form = $this->createForm(new CategorieType, $categorie);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $entreprise = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('CIELOEntrepriseBundle:Entreprise')
                        ->find($_SESSION['id_entreprise_courant']);
                if ($categorie->getCategorie()) {
                    $categorie->setFamille($categorie->getCategorie()->getFamille());
                    $categorie->setEntreprise($categorie->getCategorie()->getEntreprise());
                }
                $categorie->setEntreprise($entreprise);
                foreach ($categorie->getProprieteCategories() as $pro) {
                    $pro->setCategorie($categorie);
                    $em->persist($pro);
                }
                $em->persist($categorie);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_entreprise_categories'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Categorie:creerCategorie.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierAction($id) {
        $categorie = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Categorie')
                ->find($id);
        if ($categorie === null) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas encore');
        }
        $form = $this->createForm(new CategorieType, $categorie);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($categorie);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_entreprise_categories'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Categorie:creerCategorie.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerAction($id) {
        $categorie = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Categorie')
                ->find($id);
        if ($categorie === null) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($categorie);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_entreprise_categories'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function ajouterProprieteAction() {
        $proprieteCategorie = new ProprieteCategorie();
        $form = $this->createForm(new ProprieteCategorieType, $proprieteCategorie);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($proprieteCategorie);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_enterprise_voir_propriete_categorie'));
            }
        }
        return $this->render('CIELOEntrepriseBundle:Categorie:ajouterProprieteCategorie.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function voirProprietesAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $categories = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Categorie')
                ->findByEntreprise($currentUser->getEntreprise());
        $proprieteCategoriesArray = array();
        foreach ($categories as $cat) {
            $proprieteCategories = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('CIELOEntrepriseBundle:ProprieteCategorie')
                    ->findByCategorie($cat);
            $proprieteCategoriesArray[] = $proprieteCategories;
        }

        return $this->render('CIELOEntrepriseBundle:Categorie:proprieteCategories.html.twig', array(
                    'proprieteCategories' => $proprieteCategoriesArray
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierProprieteAction($id) {
        $proprieteCategorie = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:ProprieteCategorie')
                ->find($id);
        if ($proprieteCategorie === null) {
            throw $this->createNotFoundException('Cette propriété n\'existe pas encore');
        }
        $form = $this->createForm(new ProprieteCategorieType, $proprieteCategorie);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($proprieteCategorie);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_enterprise_voir_propriete_categorie'));
            }
        }
        return $this->render('CIELOEntrepriseBundle:Categorie:ajouterProprieteCategorie.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerProprieteAction($id) {
        $proprieteCategorie = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:ProprieteCategorie')
                ->find($id);
        if ($proprieteCategorie === null) {
            throw $this->createNotFoundException('Cette propriete n\'existe pas encore');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($proprieteCategorie);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_enterprise_voir_propriete_categorie'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function voirProprieteCategorieAction($id) {
        $categorie = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Categorie')
                ->find($id);
        $proprieteCategories = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:ProprieteCategorie')
                ->findByCategorie($categorie);
        return $this->render('CIELOAdminEntrepriseBundle:Categorie:proprieteCategorie.html.twig', array(
                    'proprieteCategories' => $proprieteCategories
        ));
    }

}

?>
