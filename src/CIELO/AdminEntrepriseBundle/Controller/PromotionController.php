<?php

namespace CIELO\AdminEntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\Produit;
use CIELO\EntrepriseBundle\Entity\Promotion;
use CIELO\EntrepriseBundle\Entity\PromotionProduit;
use CIELO\EntrepriseBundle\Form\PromotionType;
use Doctrine\ORM\EntityRepository;

class PromotionController extends Controller {

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function promotionAction() {
        $id = $_SESSION["id_entreprise_courant"];
        $entreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Entreprise')
                ->find($id);
        $promotions = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->findByEntreprise($entreprise);
        $now = new \DateTime();
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:promotion.html.twig', array(
                    'promotions' => $promotions,
                    'now' => $now
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function creerPromotionAction() {
        $id = $_SESSION["id_entreprise_courant"];
        $entreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Entreprise')
                ->find($id);
        $promotion = new Promotion();
        $form = $this->createForm(new PromotionType, $promotion);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $promotion->setEntreprise($entreprise);
                $em->persist($promotion);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_promotion'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:creerPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierPromotionAction($id) {
        $id_entreprise_courant = $_SESSION["id_entreprise_courant"];
        $entreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Entreprise')
                ->find($id_entreprise_courant);
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        $form = $this->createForm(new PromotionType, $promotion);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $promotion->setEntreprise($entreprise);
                $em->persist($promotion);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_promotion'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:creerPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerPromotionAction($id) {
        $em = $this->getDoctrine()->getEntityManager();
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        if ($promotion === null) {
            throw $this->createNotFoundException('Cette prmotion n\'existe pas');
        }
        $allPromotionProduit = $promotion->getPromotionProduits();
        foreach ($allPromotionProduit as $pp) {
            $modeles = $pp->getProduit()->getModeles();
            foreach ($modeles as $m) {
                if ($m->getPrixPromo() != $m->getPrix()) {
                    $m->setPrixPromo($m->getPrix());
                    $em->flush();
                }
            }
        }
        $em->remove($promotion);
        $em->flush();
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_enterprise_promotion'));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function ajouterUnProduitPromotionAction() {
        $promotionProduit = new PromotionProduit();
        $now = new \DateTime();
        $form = $this->createFormBuilder($promotionProduit)
                ->add('promotion', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Promotion",
                    'property' => "nom",
                    'empty_value' => 'Choisir une promotion',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION["id_entreprise_courant"]);
                    },
                ))
                ->add('produit', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Produit",
                    'property' => "nom",
                    'empty_value' => 'Choisir un produit',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.categorie', 'c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION["id_entreprise_courant"]);
                    },
                ))
                ->add('reduction', 'integer', array(
                    'required' => true
                ))
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $promotionProduitExistant = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                        ->findOneBy(array(
                    'promotion' => $promotionProduit->getPromotion(),
                    'produit' => $promotionProduit->getProduit()
                ));
                if (!$promotionProduitExistant) {
                    $produit = $promotionProduit->getProduit();
                    $modeles = $produit->getModeles();
                    foreach ($modeles as $m) {
                        if ($promotionProduit->getPromotion()->getDateDebutPromotion()->format("Y-m-d") == $now->format("Y-m-d")) {
                            $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($promotionProduit->getReduction() / 100)));
                        }
                    }
                    $em = $this->getDoctrine()->getEntityManager();
                    $em->persist($promotionProduit);
                    $em->flush();
                    return $this->redirect($this->generateUrl('cielo_admin_enterprise_ajouter_un_produit_promotion'));
                } else {
                    return $this->redirect($this->generateUrl('cielo_admin_enterprise_erreur'));
                }
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:ajouterUnProduitPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function ajouterCategoriePromotionAction() {
        $now = new \DateTime();
        $_SESSION['id_entreprise'] = $_SESSION['id_entreprise_courant'];
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('promotion', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Promotion",
                    'property' => "nom",
                    'empty_value' => 'Choisir une promotion',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise_courant']);
                    },
                ))
                ->add('categorie', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Categorie",
                    'property' => "nom",
                    'empty_value' => 'Choisir une catégorie',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise_courant']);
                    },
                ))
                ->add('reduction', 'integer', array(
                    'required' => true
                ))
                ->getForm();
        $request = $this->get('request');
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $data = $form->getData();
                $promotion = $data["promotion"];
                $categorie = $data["categorie"];
                $reduction = $data["reduction"];
                $produits = $categorie->getProduits();
                $promotionProduitExistantArray = array();
                foreach ($produits as $p) {
                    $promotionProduitExistant = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                            ->findOneBy(array(
                        'promotion' => $promotion,
                        'produit' => $p
                    ));
                    if (!$promotionProduitExistant) {
                        $modeles = $p->getModeles();
                        foreach ($modeles as $m) {
                            if ($promotion->getDateDebutPromotion()->format("Y-m-d") == $now->format("Y-m-d")) {
                                $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($reduction / 100)));
                            }
                        }
                        $newPromotionProduit = new PromotionProduit();
                        $newPromotionProduit->setProduit($p);
                        $newPromotionProduit->setPromotion($promotion);
                        $newPromotionProduit->setReduction($reduction);
                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($newPromotionProduit);
                        $em->flush();
                    } else {
                        $promotionProduitExistantArray[] = $promotionProduitExistant;
                    }
                }
                if (count($promotionProduitExistant) > 0) {
                    return $this->render('CIELOAdminEntrepriseBundle:Promotion:erreurCategoriePromotion.html.twig', array(
                                'promotionProduitExistantArray' => $promotionProduitExistantArray
                    ));
                } else {
                    return $this->redirect($this->generateUrl('cielo_admin_enterprise_ajouter_categorie_promotion'));
                }
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:ajouterUneCategoriePromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function modifierProduitDeLaPromotionAction($id) {
        $now = new \DateTime();
        $_SESSION['id_entreprise'] = $_SESSION['id_entreprise_courant'];
        $promotionProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                ->find($id);
        $form = $this->createFormBuilder($promotionProduit)
                ->add('promotion', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Promotion",
                    'property' => "nom",
                    'empty_value' => 'Choisir une promotion',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise_courant']);
                    },
                ))
                ->add('produit', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Produit",
                    'property' => "nom",
                    'empty_value' => 'Choisir un produit',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.categorie', 'c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise_courant']);
                    },
                ))
                ->add('reduction', 'integer', array(
                    'required' => true
                ))
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $produit = $promotionProduit->getProduit();
                $modeles = $produit->getModeles();
                foreach ($modeles as $m) {
                    if ($promotionProduit->getPromotion()->getDateDebutPromotion()->format("Y-m-d") == $now->format("Y-m-d")) {
                        $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($promotionProduit->getReduction() / 100)));
                    }
                }
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($promotionProduit);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_admin_enterprise_ajouter_un_produit_promotion'));
            }
        }
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:ajouterUnProduitPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function supprimerProduitDeLaPromotionAction($id) {
        $promotionProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                ->find($id);
        if ($promotionProduit === null) {
            throw $this->createNotFoundException('Ce produit n\'existe pas dans cette promotion');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($promotionProduit);
        $em->flush();
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_admin_enterprise_produit_dune_promotion', array(
                            'id' => $id
        )));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function produitDeLaPromotionAction($id) {
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        $promotionProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                ->findByPromotion($promotion);
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:produitDeLaPromotion.html.twig', array(
                    'promotionProduit' => $promotionProduit
        ));
    }

    /**
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function erreurAction() {
        return $this->render('CIELOAdminEntrepriseBundle:Promotion:erreur.html.twig');
    }

}

?>
