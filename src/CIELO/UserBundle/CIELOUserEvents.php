<?php
namespace CIELO\UserBundle;

final class CIELOUserEvents {
    const REGISTRATION_SUCCESS = 'cielo_user.registration.success';

    const REGISTRATION_COMPLETED = 'cielo_user.registration.completed';
}
