<?php
namespace CIELO\UserBundle\Form\Type;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationClientFormType extends BaseType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder
                ->add('nom')
                ->add('prenom')
                ->add('telephone')
                ->add('dateNaissance', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                ))
                ->add('pays', 'country')
                ->add('ville')
                ->add('quartier')
                ->add('boitepostale')
        ;
    }

    public function getName() {
        return "cielo_client_registration_form";
    }

}
