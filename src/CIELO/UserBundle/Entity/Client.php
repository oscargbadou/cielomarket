<?php

namespace CIELO\UserBundle\Entity;

use CIELO\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="cielo_client")
 * @UniqueEntity(fields = "username", targetClass = "CIELO\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "CIELO\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Client extends User {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

}
