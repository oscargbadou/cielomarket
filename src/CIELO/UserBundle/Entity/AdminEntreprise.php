<?php

namespace CIELO\UserBundle\Entity;

use CIELO\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="cielo_admin_entreprise")
 * @UniqueEntity(fields = "username", targetClass = "CIELO\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "CIELO\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class AdminEntreprise extends User {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Entreprise", inversedBy="adminEntreprises")
     */
    private $entreprise;

    /**
     * Set entreprise
     *
     * @param \CIELO\EnterpriseBundle\Entity\Entreprise $entreprise
     * @return AdminEntreprise
     */
    public function setEntreprise(\CIELO\EntrepriseBundle\Entity\Entreprise $entreprise) {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CIELO\EntrepriseBundle\Entity\Entreprise 
     */
    public function getEntreprise() {
        return $this->entreprise;
    }

}
