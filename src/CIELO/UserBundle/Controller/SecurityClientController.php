<?php

namespace CIELO\UserBundle\Controller;

use \FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityClientController extends BaseController {

    protected function renderLogin(array $data) {
        $template = sprintf('CIELOUserBundle:Security:client_login.html.%s', $this->container->getParameter('fos_user.template.engine'));

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
