<?php

namespace CIELO\UserBundle\Controller;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CIELO\UserBundle\CIELOUserEvents;
class FOSRegistrationAdminController extends BaseController{
    
    
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->addRole("ROLE_ADMIN_ENTREPRISE");
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        if (!$this->container->get("security.context")->isGranted("ROLE_SUPER_ADMIN")) {
            $form = $formFactory->createForm();
        }else{
            $form = $this->container->get("form.factory")->create($this->container->get("cielo_admin_entreprise_admin.registration.form.type"));
        }
        
        $form->setData($user);

        if ('POST' === $request->getMethod()) {
            $form->bind($request);

            if ($form->isValid()) {
                
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(CIELOUserEvents::REGISTRATION_SUCCESS, $event);
                
                if(!$this->container->get("security.context")->isGranted("ROLE_SUPER_ADMIN")){
                    $currentUser = $this->container->get("security.context")->getToken()->getUser();
                    $user->setEntreprise($currentUser->getEntreprise());
                }
                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
            'form' => $form->createView(),
        ));
    }

}
