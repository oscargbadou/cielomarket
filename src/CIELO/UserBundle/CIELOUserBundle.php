<?php

namespace CIELO\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CIELOUserBundle extends Bundle {

    public function getParent() {
        return "FOSUserBundle";
    }

}
