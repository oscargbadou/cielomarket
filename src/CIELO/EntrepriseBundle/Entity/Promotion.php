<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Promotion
 *
 * @ORM\Table(name="cielo_promotion")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\PromotionRepository")
 */
class Promotion {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Entreprise")
     */
    private $entreprise;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\PromotionProduit", mappedBy="promotion", cascade={"remove", "persist"})
     */
    private $promotionProduits;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebutPromotion", type="datetime")
     */
    private $dateDebutPromotion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFinPromotion", type="datetime")
     */
    private $dateFinPromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    public function __construct() {
        $this->dateDebutPromotion = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dateDebutPromotion
     *
     * @param \DateTime $dateDebutPromotion
     * @return Promotion
     */
    public function setDateDebutPromotion($dateDebutPromotion) {
        $this->dateDebutPromotion = $dateDebutPromotion;
        $this->promotionProduits = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Get dateDebutPromotion
     *
     * @return \DateTime 
     */
    public function getDateDebutPromotion() {
        return $this->dateDebutPromotion;
    }

    /**
     * Set dateFinPromotion
     *
     * @param \DateTime $dateFinPromotion
     * @return Promotion
     */
    public function setDateFinPromotion($dateFinPromotion) {
        $this->dateFinPromotion = $dateFinPromotion;

        return $this;
    }

    /**
     * Get dateFinPromotion
     *
     * @return \DateTime 
     */
    public function getDateFinPromotion() {
        return $this->dateFinPromotion;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Promotion
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Add promotionProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits
     * @return Promotion
     */
    public function addPromotionProduit(\CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits) {
        $this->promotionProduits[] = $promotionProduits;

        return $this;
    }

    /**
     * Remove promotionProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits
     */
    public function removePromotionProduit(\CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits) {
        $this->promotionProduits->removeElement($promotionProduits);
    }

    /**
     * Get promotionProduits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPromotionProduits() {
        return $this->promotionProduits;
    }

    /**
     * Set entreprise
     *
     * @param \CIELO\EntrepriseBundle\Entity\Entreprise $entreprise
     * @return Promotion
     */
    public function setEntreprise(\CIELO\EntrepriseBundle\Entity\Entreprise $entreprise = null) {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CIELO\EntrepriseBundle\Entity\Entreprise 
     */
    public function getEntreprise() {
        return $this->entreprise;
    }

    public function toJSON($toArray = false) {
        $entreprise = $this->entreprise;
        $dateF = $this->dateDebutPromotion;
        $dateE = $this->dateFinPromotion;
        if ($this->dateDebutPromotion != null) {
            $dateFFormat = $dateF->format('Y-m-d H:i:s');
        } else {
            $dateFFormat = "";
        }
        if ($this->dateFinPromotion != null) {
            $dateEFormat = $dateE->format('Y-m-d H:i:s');
        } else {
            $dateEFormat = "";
        }
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "dateDebut" => $dateFFormat,
            "dateFin" => $dateEFormat,
            //"entreprise" => $entreprise->toJSON(true),
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $entreprise = $this->entreprise;
        $dateF = $this->dateDebutPromotion;
        $dateE = $this->dateFinPromotion;
        if ($this->dateDebutPromotion != null) {
            $dateFFormat = $dateF->format('Y-m-d H:i:s');
        } else {
            $dateFFormat = "";
        }
        if ($this->dateFinPromotion != null) {
            $dateEFormat = $dateE->format('Y-m-d H:i:s');
        } else {
            $dateEFormat = "";
        }
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "dateDebut" => $dateFFormat,
            "dateFin" => $dateEFormat,
            "entreprise" => $entreprise->toJSON(true),
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

}