<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Modele
 *
 * @ORM\Table(name="cielo_modele")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\ModeleRepository")
 */
class Modele {

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\ProprieteProduit", mappedBy="modele", cascade={"persist", "remove"})
     */
    private $proprieteProduits;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\RendezVous", mappedBy="modele", cascade={"persist", "remove"})
     */
    private $rendezVouss;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Produit", inversedBy="modeles", cascade={"persist"})
     */
    private $produit;

    /**
     * @ORM\ManyToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"remove", "persist"})
     */
    private $documents;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EcommerceBundle\Entity\CommandeProduit", mappedBy="modele", cascade={"remove", "persist"})
     */
    private $commandeProduits;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="prix", type="integer")
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Le prix doit être positif"
     * )
     * @Assert\NotBlank()
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="prixPromo", type="integer", nullable=true)
     */
    private $prixPromo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     * @return Modele
     */
    public function setPrix($prix) {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer 
     */
    public function getPrix() {
        return $this->prix;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->proprieteProduits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add proprieteProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits
     * @return Modele
     */
    public function addProprieteProduit(\CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits) {
        $this->proprieteProduits[] = $proprieteProduits;

        return $this;
    }

    /**
     * Remove proprieteProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits
     */
    public function removeProprieteProduit(\CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits) {
        $this->proprieteProduits->removeElement($proprieteProduits);
    }

    /**
     * Get proprieteProduits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprieteProduits() {
        return $this->proprieteProduits;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return Modele
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit = null) {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit() {
        return $this->produit;
    }

    /**
     * Add documents
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $documents
     * @return Modele
     */
    public function addDocument(\CIELO\EntrepriseBundle\Entity\Document $documents) {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $documents
     */
    public function removeDocument(\CIELO\EntrepriseBundle\Entity\Document $documents) {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments() {
        return $this->documents;
    }

    public function toJSON($toArray = false) {
        $images = Array();
        foreach ($this->documents as $img) {
            if ($img) {
                $images[] =  $img->getLink();
            }
        }
        $proprietes = array();
        foreach ($this->proprieteProduits as $pp) {
            $proprietes[] = $pp->toJSON(true);
        }
        $array = Array(
            "id" => $this->id,
            "prix" => $this->prix,
            "prix_promotion" => $this->prixPromo,
            "images" => $images,
            "proprietes" => $proprietes
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $images = Array();
        foreach ($this->documents as $img) {
            if ($img) {
                $images[] =  $img->getLink();
            }
        }
        $proprietes = array();
        foreach ($this->proprieteProduits as $pp) {
            $proprietes[] = $pp->toJSON(true);
        }
        $array = Array(
            "id" => $this->id,
            "prix" => $this->prix,
            "prix_promotion" => $this->prixPromo,
            "images" => $images,
            "proprietes" => $proprietes
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Set prixPromo
     *
     * @param integer $prixPromo
     * @return Modele
     */
    public function setPrixPromo($prixPromo) {
        $this->prixPromo = $prixPromo;

        return $this;
    }

    /**
     * Get prixPromo
     *
     * @return integer 
     */
    public function getPrixPromo() {
        return $this->prixPromo;
    }


    /**
     * Add commandeProduits
     *
     * @param \CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits
     * @return Modele
     */
    public function addCommandeProduit(\CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits)
    {
        $this->commandeProduits[] = $commandeProduits;
    
        return $this;
    }

    /**
     * Remove commandeProduits
     *
     * @param \CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits
     */
    public function removeCommandeProduit(\CIELO\EcommerceBundle\Entity\CommandeProduit $commandeProduits)
    {
        $this->commandeProduits->removeElement($commandeProduits);
    }

    /**
     * Get commandeProduits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommandeProduits()
    {
        return $this->commandeProduits;
    }

    /**
     * Add rendezVouss
     *
     * @param \CIELO\EntrepriseBundle\Entity\RendezVous $rendezVouss
     * @return Modele
     */
    public function addRendezVous(\CIELO\EntrepriseBundle\Entity\RendezVous $rendezVouss)
    {
        $this->rendezVouss[] = $rendezVouss;
    
        return $this;
    }

    /**
     * Remove rendezVouss
     *
     * @param \CIELO\EntrepriseBundle\Entity\RendezVous $rendezVouss
     */
    public function removeRendezVous(\CIELO\EntrepriseBundle\Entity\RendezVous $rendezVouss)
    {
        $this->rendezVouss->removeElement($rendezVouss);
    }

    /**
     * Get rendezVouss
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRendezVouss()
    {
        return $this->rendezVouss;
    }
}