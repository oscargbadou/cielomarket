<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ProprieteCategorieRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProprieteCategorieRepository extends EntityRepository
{
}
