<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProprieteProduit
 *
 * @ORM\Table(name="cielo_propriete_produit")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\ProprieteProduitRepository")
 */
class ProprieteProduit {
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Modele", inversedBy="proprieteProduits"), cascade={"remove"})
     */
    private $modele;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255, nullable=true)
     */
    private $valeur;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return ProprieteProduit
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit) {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit() {
        return $this->produit;
    }

    


    /**
     * Set nom
     *
     * @param string $nom
     * @return ProprieteProduit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    
    public function toJSON($toArray = false) {
        $array = Array(
            "nom" => $this->nom,
            "valeur" => $this->valeur,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $array = Array(
            "nom" => $this->nom,
            "valeur" => $this->valeur,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProprieteProduit
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ProprieteProduit
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    
        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set modele
     *
     * @param \CIELO\EntrepriseBundle\Entity\Modele $modele
     * @return ProprieteProduit
     */
    public function setModele(\CIELO\EntrepriseBundle\Entity\Modele $modele = null)
    {
        $this->modele = $modele;
    
        return $this;
    }

    /**
     * Get modele
     *
     * @return \CIELO\EntrepriseBundle\Entity\Modele 
     */
    public function getModele()
    {
        return $this->modele;
    }
}