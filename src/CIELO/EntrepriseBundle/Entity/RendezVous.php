<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RendezVous
 *
 * @ORM\Table(name="cielo_rendez_vous")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\RendezVousRepository")
 */
class RendezVous
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Modele", inversedBy="rendezVouss")
     */
    private $modele;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\UserBundle\Entity\User")
     */
    private $user;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnvoie", type="datetime")
     */
    private $dateEnvoie;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="vue", type="boolean", nullable=true)
     */
    private $vue;

    public function __construct() {
        $this->dateEnvoie = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateEnvoie
     *
     * @param \DateTime $dateEnvoie
     * @return RendezVous
     */
    public function setDateEnvoie($dateEnvoie)
    {
        $this->dateEnvoie = $dateEnvoie;
    
        return $this;
    }

    /**
     * Get dateEnvoie
     *
     * @return \DateTime 
     */
    public function getDateEnvoie()
    {
        return $this->dateEnvoie;
    }

    /**
     * Set modele
     *
     * @param \CIELO\EntrepriseBundle\Entity\Modele $modele
     * @return RendezVous
     */
    public function setModele(\CIELO\EntrepriseBundle\Entity\Modele $modele = null)
    {
        $this->modele = $modele;
    
        return $this;
    }

    /**
     * Get modele
     *
     * @return \CIELO\EntrepriseBundle\Entity\Modele 
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set user
     *
     * @param \CIELO\UserBundle\Entity\User $user
     * @return RendezVous
     */
    public function setUser(\CIELO\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \CIELO\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set vue
     *
     * @param boolean $vue
     * @return RendezVous
     */
    public function setVue($vue)
    {
        $this->vue = $vue;
    
        return $this;
    }

    /**
     * Get vue
     *
     * @return boolean 
     */
    public function getVue()
    {
        return $this->vue;
    }
}