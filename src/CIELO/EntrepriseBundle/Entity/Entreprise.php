<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CIELO\EcommerceBundle\Utils\Utils;
use CIELO\EntrepriseBundle\Entity\Categorie;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entreprise
 *
 * @ORM\Table(name="cielo_entreprise")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\EntrepriseRepository")
 */
class Entreprise {

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\DomaineActivite", inversedBy="entreprises")
     */
    private $domaineactivite;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Promotion", mappedBy="entreprise", cascade={"remove","persist"})
     */
    private $promotions;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\UserBundle\Entity\AdminEntreprise", mappedBy="entreprise", cascade={"remove","persist"})
     */
    private $adminEntreprises;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\AdminBundle\Entity\Campagne", mappedBy="entreprise", cascade={"remove","persist"})
     */
    private $campagnes;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="numIFU", type="string", length=255, nullable=true)
     */
    private $numIFU;

    /**
     * @var string
     *
     * @ORM\Column(name="tel1", type="string", length=255, nullable=true)
     */
    private $tel1;

    /**
     * @var string
     *
     * @ORM\Column(name="tel2", type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="siteweb", type="string", length=255, nullable=true)
     */
    private $siteweb;

    /**
     * @ORM\OneToMany(targetEntity="Categorie", mappedBy="entreprise")
     */
    private $categories;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Entreprise
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set numIFU
     *
     * @param string $numIFU
     * @return Entreprise
     */
    public function setNumIFU($numIFU) {
        $this->numIFU = $numIFU;

        return $this;
    }

    /**
     * Get numIFU
     *
     * @return string 
     */
    public function getNumIFU() {
        return $this->numIFU;
    }

    /**
     * Set tel1
     *
     * @param string $tel1
     * @return Entreprise
     */
    public function setTel1($tel1) {
        $this->tel1 = $tel1;

        return $this;
    }

    /**
     * Get tel1
     *
     * @return string 
     */
    public function getTel1() {
        return $this->tel1;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     * @return Entreprise
     */
    public function setTel2($tel2) {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * Get tel2
     *
     * @return string 
     */
    public function getTel2() {
        return $this->tel2;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Entreprise
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set siteweb
     *
     * @param string $siteweb
     * @return Entreprise
     */
    public function setSiteweb($siteweb) {
        $this->siteweb = $siteweb;

        return $this;
    }

    /**
     * Get siteweb
     *
     * @return string 
     */
    public function getSiteweb() {
        return $this->siteweb;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return Entreprise
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set domaineactivite
     *
     * @param \CIELO\EntrepriseBundle\Entity\DomaineActivite $domaineactivite
     * @return Entreprise
     */
    public function setDomaineactivite(\CIELO\EntrepriseBundle\Entity\DomaineActivite $domaineactivite) {
        $this->domaineactivite = $domaineactivite;

        return $this;
    }

    /**
     * Get domaineactivite
     *
     * @return \CIELO\EntrepriseBundle\Entity\DomaineActivite 
     */
    public function getDomaineactivite() {
        return $this->domaineactivite;
    }

    public function getCategories() {
        return $this->categories;
    }

    public function addCategorie(Categorie $categorie) {
        $this->categories[] = $categorie;
        return $this;
    }

    public function toJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "urlImage" => $this->getDocument() ?  $this->getDocument()->getLink() : null,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "tel1" => $this->tel1,
            "tel2" => $this->tel2,
            "mail" => $this->mail,
            "siteweb" => $this->siteweb,
            "urlImage" => $this->getDocument() ?  $this->getDocument()->getLink() : null,
            "domaine" => $this->domaineactivite->getNom(),
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->promotions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add promotions
     *
     * @param \CIELO\EntrepriseBundle\Entity\Promotion $promotions
     * @return Entreprise
     */
    public function addPromotion(\CIELO\EntrepriseBundle\Entity\Promotion $promotions)
    {
        $this->promotions[] = $promotions;
    
        return $this;
    }

    /**
     * Remove promotions
     *
     * @param \CIELO\EntrepriseBundle\Entity\Promotion $promotions
     */
    public function removePromotion(\CIELO\EntrepriseBundle\Entity\Promotion $promotions)
    {
        $this->promotions->removeElement($promotions);
    }

    /**
     * Get promotions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Remove categories
     *
     * @param \CIELO\EntrepriseBundle\Entity\Categorie $categories
     */
    public function removeCategorie(\CIELO\EntrepriseBundle\Entity\Categorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Add adminEntreprises
     *
     * @param \CIELO\UserBundle\Entity\AdminEntreprise $adminEntreprises
     * @return Entreprise
     */
    public function addAdminEntreprise(\CIELO\UserBundle\Entity\AdminEntreprise $adminEntreprises)
    {
        $this->adminEntreprises[] = $adminEntreprises;
    
        return $this;
    }

    /**
     * Remove adminEntreprises
     *
     * @param \CIELO\UserBundle\Entity\AdminEntreprise $adminEntreprises
     */
    public function removeAdminEntreprise(\CIELO\UserBundle\Entity\AdminEntreprise $adminEntreprises)
    {
        $this->adminEntreprises->removeElement($adminEntreprises);
    }

    /**
     * Get adminEntreprises
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdminEntreprises()
    {
        return $this->adminEntreprises;
    }

    /**
     * Add campagnes
     *
     * @param \CIELO\AdminBundle\Entity\Campagne $campagnes
     * @return Entreprise
     */
    public function addCampagne(\CIELO\AdminBundle\Entity\Campagne $campagnes)
    {
        $this->campagnes[] = $campagnes;
    
        return $this;
    }

    /**
     * Remove campagnes
     *
     * @param \CIELO\AdminBundle\Entity\Campagne $campagnes
     */
    public function removeCampagne(\CIELO\AdminBundle\Entity\Campagne $campagnes)
    {
        $this->campagnes->removeElement($campagnes);
    }

    /**
     * Get campagnes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampagnes()
    {
        return $this->campagnes;
    }
}