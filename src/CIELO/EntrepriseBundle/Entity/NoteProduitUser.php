<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * NoteProduitUser
 *
 * @ORM\Table(name="cielo_note_produit_user")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\NoteProduitUserRepository")
 */
class NoteProduitUser
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\UserBundle\Entity\User")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Produit", inversedBy="noteProduitUsers")
     */
    private $produit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="note", type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 5,
     *      minMessage = "La note doit être au moins de 1%",
     *      maxMessage = "La note doit être au plus de 5%"
     * )
     */
    private $note;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set note
     *
     * @param integer $note
     * @return NoteProduitUser
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return integer 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return NoteProduitUser
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;
    
        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set user
     *
     * @param \CIELO\UserBundle\Entity\User $user
     * @return NoteProduitUser
     */
    public function setUser(\CIELO\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \CIELO\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}