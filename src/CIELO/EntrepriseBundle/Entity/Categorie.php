<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Categorie
 *
 * @ORM\Table(name="cielo_categorie")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\CategorieRepository")
 */
class Categorie
{
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\ProprieteCategorie", mappedBy="categorie", cascade={"remove"})
     */
    private $proprieteCategories;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Categorie")
     */
    private $categorie;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Famille", inversedBy="categories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $famille;
    
    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Entreprise", inversedBy="categories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $entreprise;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Produit", mappedBy="categorie", cascade={"persist", "remove"})
     */
    private $produits;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Categorie
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set categorie
     *
     * @param \CIELO\EntrepriseBundle\Entity\Categorie $categorie
     * @return Categorie
     */
    public function setCategorie(\CIELO\EntrepriseBundle\Entity\Categorie $categorie)
    {
        $this->categorie = $categorie;
    
        return $this;
    }

    /**
     * Get categorie
     *
     * @return \CIELO\EntrepriseBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set famille
     *
     * @param \CIELO\EntrepriseBundle\Entity\Famille $famille
     * @return Categorie
     */
    public function setFamille(\CIELO\EntrepriseBundle\Entity\Famille $famille)
    {
        $this->famille = $famille;

        return $this;
    }

    /**
     * Get famille
     *
     * @return \CIELO\EntrepriseBundle\Entity\Famille 
     */
    public function getFamille()
    {
        return $this->famille;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return Categorie
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set entreprise
     *
     * @param \CIELO\EntrepriseBundle\Entity\Entreprise $entreprise
     * @return Categorie
     */
    public function setEntreprise(\CIELO\EntrepriseBundle\Entity\Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CIELO\EntrepriseBundle\Entity\Entreprise 
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proprieteCategories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add produits
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produits
     * @return Categorie
     */
    public function addProduit(\CIELO\EntrepriseBundle\Entity\Produit $produits)
    {
        $this->produits[] = $produits;

        return $this;
    }

    /**
     * Remove produits
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produits
     */
    public function removeProduit(\CIELO\EntrepriseBundle\Entity\Produit $produits)
    {
        $this->produits->removeElement($produits);
    }

    /**
     * Get produits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Add proprieteCategories
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteCategorie $proprieteCategories
     * @return Categorie
     */
    public function addProprieteCategorie(\CIELO\EntrepriseBundle\Entity\ProprieteCategorie $proprieteCategories)
    {
        $this->proprieteCategories[] = $proprieteCategories;
        $proprieteCategories->setCategorie($this);
        return $this;
    }

    /**
     * Remove proprieteCategories
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteCategorie $proprieteCategories
     */
    public function removeProprieteCategorie(\CIELO\EntrepriseBundle\Entity\ProprieteCategorie $proprieteCategories)
    {
        $this->proprieteCategories->removeElement($proprieteCategories);
        $proprieteCategories->setCategorie(null);
    }

    /**
     * Get proprieteCategories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprieteCategories()
    {
        return $this->proprieteCategories;
    }
}