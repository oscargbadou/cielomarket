<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ProprieteProduitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProprieteProduitRepository extends EntityRepository
{
}
