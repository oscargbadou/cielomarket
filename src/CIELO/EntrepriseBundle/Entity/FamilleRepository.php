<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * FamilleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FamilleRepository extends EntityRepository {

    public function nbreFamille() {
        return $this->createQueryBuilder("f")
                        ->select("COUNT(f)")
                        ->getQuery()
                        ->getSingleScalarResult();
    }

    public function nbreFamilleDeDomaine($domId) {
        return $this->createQueryBuilder("f")
                        ->select("COUNT(f)")
                        ->join("f.domaineActivite", "d")
                        ->where("d.id = :domId")
                        ->setParameter("domId", $domId)
                        ->getQuery()
                        ->getSingleScalarResult();
    }
    
    public function nbreFamilleRechercher($searchContent) {
        return $this->createQueryBuilder("f")
                        ->select("COUNT(f)")
                        ->where("f.nom LIKE :searchContent OR f.description LIKE :searchContent")
                        ->setParameter("searchContent", '%' . $searchContent . '%')
                        ->getQuery()
                        ->getSingleScalarResult();
    }

    public function famillesRechercher($searchContent, $start) {
        return $this->createQueryBuilder("f")
                        ->where("f.nom LIKE :searchContent OR f.description LIKE :searchContent")
                        ->setParameter("searchContent", '%' . $searchContent . '%')
                        ->setFirstResult($start)
                        ->setMaxResults(100)
                        ->getQuery()
                        ->getResult();
    }

}
