<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CIELO\EcommerceBundle\Utils\Utils;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Famille
 *
 * @ORM\Table(name="cielo_famille")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\FamilleRepository")
 */
class Famille {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\DomaineActivite", inversedBy="familles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $domaineActivite;

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Categorie", mappedBy="famille", cascade={"remove"})
     */
    private $categories;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Famille
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Famille
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set domaineActivite
     *
     * @param \CIELO\EntrepriseBundle\Entity\DomaineActivite $domaineActivite
     * @return Famille
     */
    public function setDomaineActivite(\CIELO\EntrepriseBundle\Entity\DomaineActivite $domaineActivite) {
        $this->domaineActivite = $domaineActivite;

        return $this;
    }

    /**
     * Get domaineActivite
     *
     * @return \CIELO\EntrepriseBundle\Entity\DomaineActivite 
     */
    public function getDomaineActivite() {
        return $this->domaineActivite;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return Famille
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument() {
        return $this->document;
    }

    public function toJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "urlImage" => $this->getDocument() ?  $this->getDocument()->getLink() : null,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "urlImage" => $this->getDocument() ?  $this->getDocument()->getLink() : null,
            "description" => $this->description,
            "domaine" => $this->domaineActivite->getNom(),
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add categories
     *
     * @param \CIELO\EntrepriseBundle\Entity\Categorie $categories
     * @return Famille
     */
    public function addCategorie(\CIELO\EntrepriseBundle\Entity\Categorie $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \CIELO\EntrepriseBundle\Entity\Categorie $categories
     */
    public function removeCategorie(\CIELO\EntrepriseBundle\Entity\Categorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }
}