<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DomaineActivite
 *
 * @ORM\Table(name="cielo_domaine_activite")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\DomaineActiviteRepository")
 */
class DomaineActivite {

    /**
     * @ORM\OneToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Entreprise", mappedBy="domaineactivite", cascade={"remove"})
     */
    private $entreprises;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Famille", mappedBy="domaineActivite", cascade={"remove"})
     */
    private $familles;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean")
     */
    private $actif;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return DomaineActivite
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return DomaineActivite
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set document
     *
     * @param \CIELO\EntrepriseBundle\Entity\Document $document
     * @return DomaineActivite
     */
    public function setDocument(\CIELO\EntrepriseBundle\Entity\Document $document = null) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \CIELO\EntrepriseBundle\Entity\Document 
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->entreprises = new \Doctrine\Common\Collections\ArrayCollection();
        $this->familles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actif = true;
    }

    /**
     * Add entreprises
     *
     * @param \CIELO\EntrepriseBundle\Entity\Entreprise $entreprises
     * @return DomaineActivite
     */
    public function addEntreprise(\CIELO\EntrepriseBundle\Entity\Entreprise $entreprises) {
        $this->entreprises[] = $entreprises;

        return $this;
    }

    /**
     * Remove entreprises
     *
     * @param \CIELO\EntrepriseBundle\Entity\Entreprise $entreprises
     */
    public function removeEntreprise(\CIELO\EntrepriseBundle\Entity\Entreprise $entreprises) {
        $this->entreprises->removeElement($entreprises);
    }

    /**
     * Get entreprises
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEntreprises() {
        return $this->entreprises;
    }

    /**
     * Add familles
     *
     * @param \CIELO\EntrepriseBundle\Entity\Famille $familles
     * @return DomaineActivite
     */
    public function addFamille(\CIELO\EntrepriseBundle\Entity\Famille $familles) {
        $this->familles[] = $familles;

        return $this;
    }

    /**
     * Remove familles
     *
     * @param \CIELO\EntrepriseBundle\Entity\Famille $familles
     */
    public function removeFamille(\CIELO\EntrepriseBundle\Entity\Famille $familles) {
        $this->familles->removeElement($familles);
    }

    /**
     * Get familles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFamilles() {
        return $this->familles;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     * @return DomaineActivite
     */
    public function setActif($actif) {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean 
     */
    public function getActif() {
        return $this->actif;
    }

}