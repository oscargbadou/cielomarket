<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PrixProduit
 *
 * @ORM\Table(name="cielo_prix_produit")
 * @ORM\Entity
 */
class PrixProduit
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Produit", inversedBy="prixProduits", cascade={"remove"})
     */
    private $produit;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\ValeurPropriete", mappedBy="prixProduit", cascade={"remove"})
     */
    private $valeurProprietes;
    
    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\ProprieteProduit", mappedBy="prixProduit", cascade={"remove"})
     */
    private $proprieteProduits;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="montant", type="integer")
     */
    private $montant;

    /**
     * @var integer
     *
     * @ORM\Column(name="montantPromo", type="integer")
     */
    private $montantPromo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param string $montant
     * @return PrixProduit
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    
        return $this;
    }

    /**
     * Get montant
     *
     * @return string 
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set montantPromo
     *
     * @param integer $montantPromo
     * @return PrixProduit
     */
    public function setMontantPromo($montantPromo)
    {
        $this->montantPromo = $montantPromo;
    
        return $this;
    }

    /**
     * Get montantPromo
     *
     * @return integer 
     */
    public function getMontantPromo()
    {
        return $this->montantPromo;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return PrixProduit
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;
    
        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->valeurProprietes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add valeurProprietes
     *
     * @param \CIELO\EntrepriseBundle\Entity\ValeurPropriete $valeurProprietes
     * @return PrixProduit
     */
    public function addValeurPropriete(\CIELO\EntrepriseBundle\Entity\ValeurPropriete $valeurProprietes)
    {
        $this->valeurProprietes[] = $valeurProprietes;
    
        return $this;
    }

    /**
     * Remove valeurProprietes
     *
     * @param \CIELO\EntrepriseBundle\Entity\ValeurPropriete $valeurProprietes
     */
    public function removeValeurPropriete(\CIELO\EntrepriseBundle\Entity\ValeurPropriete $valeurProprietes)
    {
        $this->valeurProprietes->removeElement($valeurProprietes);
    }

    /**
     * Get valeurProprietes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValeurProprietes()
    {
        return $this->valeurProprietes;
    }

    /**
     * Add proprieteProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits
     * @return PrixProduit
     */
    public function addProprieteProduit(\CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits)
    {
        $this->proprieteProduits[] = $proprieteProduits;
    
        return $this;
    }

    /**
     * Remove proprieteProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits
     */
    public function removeProprieteProduit(\CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduits)
    {
        $this->proprieteProduits->removeElement($proprieteProduits);
    }

    /**
     * Get proprieteProduits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProprieteProduits()
    {
        return $this->proprieteProduits;
    }
}