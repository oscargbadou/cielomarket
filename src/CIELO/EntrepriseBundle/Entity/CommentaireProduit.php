<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CommentaireProduit
 *
 * @ORM\Table(name="cielo_commentaire_produit")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\CommentaireProduitRepository")
 */
class CommentaireProduit
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\UserBundle\Entity\User")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Produit", inversedBy="commentaires", cascade={"remove"})
     */
    private $produit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=255)
     * @Assert\NotBlank(message="Entrez un commentaire SVP...")
     */
    private $contenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Constructor
     */
    public function __construct() {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return CommentaireProduit
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    
        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return CommentaireProduit
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return CommentaireProduit
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;
    
        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit()
    {
        return $this->produit;
    }
    
    public function toJSON($toArray = false) {
        $auteur=$this->client;
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $date = strftime("%A %d %B %Y", strtotime($this->date->format("Y-m-d")));
        $array = Array(
            "auteur" => $auteur->getUsername(),
            "date"=>$date,
            "contenu" => $this->contenu,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $auteur=$this->user;
        $date=$this->date;
        $array = Array(
            "auteur" => $auteur->getUsername(),
            "date"=>$date->format('Y-m-d H:i:s'),
            "contenu" => $this->contenu,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Set user
     *
     * @param \CIELO\UserBundle\Entity\User $user
     * @return CommentaireProduit
     */
    public function setUser(\CIELO\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \CIELO\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}