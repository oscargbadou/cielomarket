<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CIELO\EcommerceBundle\Utils\Utils;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Produit
 *
 * @ORM\Table(name="cielo_produit")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\ProduitRepository")
 */
class Produit {

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Categorie", inversedBy="produits")
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\Modele", mappedBy="produit", cascade={"remove"})
     */
    private $modeles;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\NoteProduitUser", mappedBy="produit", cascade={"remove"})
     */
    private $noteProduitUsers;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\CommentaireProduit", mappedBy="produit", cascade={"remove"})
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity="CIELO\EntrepriseBundle\Entity\PromotionProduit", mappedBy="produit", cascade={"remove", "persist"})
     */
    private $promotionProduits;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="fabricant", type="string", length=255, nullable=true)
     */
    private $fabricant;

    /**
     * @var string
     *
     * @ORM\Column(name="numProduit", type="string", length=255, nullable=true)
     */
    private $numProduit;

    /**
     * @var integer
     *
     * @ORM\Column(name="purchased", type="integer", nullable=true)
     */
    private $purchased;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAjout", type="datetime")
     */
    private $dateAjout;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFabrication", type="datetime", nullable=true)
     */
    private $dateFabrication;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExpiration", type="datetime", nullable=true)
     */
    private $dateExpiration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=true)
     */
    private $visible;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="commandable", type="boolean", nullable=true)
     */
    private $commandable;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="prixParDefaut", type="integer")
     */
    private $prixParDefaut;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Produit
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set fabricant
     *
     * @param string $fabricant
     * @return Produit
     */
    public function setFabricant($fabricant) {
        $this->fabricant = $fabricant;

        return $this;
    }

    /**
     * Get fabricant
     *
     * @return string 
     */
    public function getFabricant() {
        return $this->fabricant;
    }

    /**
     * Set numProduit
     *
     * @param string $numProduit
     * @return Produit
     */
    public function setNumProduit($numProduit) {
        $this->numProduit = $numProduit;

        return $this;
    }

    /**
     * Get numProduit
     *
     * @return string 
     */
    public function getNumProduit() {
        return $this->numProduit;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Produit
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set categorie
     *
     * @param \CIELO\EntrepriseBundle\Entity\Categorie $categorie
     * @return Produit
     */
    public function setCategorie(\CIELO\EntrepriseBundle\Entity\Categorie $categorie) {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \CIELO\EntrepriseBundle\Entity\Categorie 
     */
    public function getCategorie() {
        return $this->categorie;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->dateAjout = new \DateTime();
        $this->visible = false;
        $this->purchased = 0;
        $this->sommeNote = 0;
        $this->nbreVotant = 0;
    }

    /**
     * Set dateFabrication
     *
     * @param \DateTime $dateFabrication
     * @return Produit
     */
    public function setDateAjout($dateAjout) {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    /**
     * Get dateFabrication
     *
     * @return \DateTime 
     */
    public function getDateAjout() {
        return $this->dateAjout;
    }

    /**
     * Set dateFabrication
     *
     * @param \DateTime $dateFabrication
     * @return Produit
     */
    public function setDateFabrication($dateFabrication) {
        $this->dateFabrication = $dateFabrication;

        return $this;
    }

    /**
     * Get dateFabrication
     *
     * @return \DateTime 
     */
    public function getDateFabrication() {
        return $this->dateFabrication;
    }

    /**
     * Set dateExpiration
     *
     * @param \DateTime $dateExpiration
     * @return Produit
     */
    public function setDateExpiration($dateExpiration) {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * Get dateExpiration
     *
     * @return \DateTime 
     */
    public function getDateExpiration() {
        return $this->dateExpiration;
    }

    public function setVues($vues) {
        $this->vues = $vues;

        return $this;
    }

    public function getVues() {
        return $this->vues;
    }

    public function toJSON($toArray = false) {
        $famille = $this->categorie->getFamille();
        $entreprise = $this->categorie->getEntreprise();
        //$documents = $this->getDocuments();
        $modele = $this->modeles;
        $images = $modele[0]->getDocuments();
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "urlImage" => ($images->count() > 0) ?  $images[0]->getLink() : null,
            "prix" => $modele[0]->getPrix(),
            "prix_promotion" => $modele[0]->getPrixPromo(),
            "famille" => $famille->toJSON(true),
            "entreprise" => $entreprise->toJSON(true),
            "commandable" => $this->commandable,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    public function toCompleteJSON($toArray = false) {
        $famille = $this->categorie->getFamille();
        $entreprise = $this->categorie->getEntreprise();
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        if ($this->dateFabrication != null) {
            $dateFabrication = strftime("%A %d %B %Y", strtotime($this->dateFabrication->format("Y-m-d")));
        } else {
            $dateFabrication = null;
        }
        if ($this->dateExpiration != null) {
            $dateExpiration = strftime("%A %d %B %Y", strtotime($this->dateExpiration->format("Y-m-d")));
        } else {
            $dateExpiration = null;
        }
        $modeles = array();
        foreach ($this->modeles as $m) {
            $modeles[] = $m->toCompleteJSON(true);
        }
        $commentaires = array();
        foreach ($this->commentaires as $c) {
            $commentaires[] = $c->toCompleteJSON(true);
        }
        $mo = $this->modeles;
        $dateF = $this->dateFabrication;
        $dateE = $this->dateExpiration;
        if ($this->dateFabrication != null) {
            $dateFFormat = $dateF->format('Y-m-d H:i:s');
        } else {
            $dateFFormat = "";
        }
        if ($this->dateExpiration != null) {
            $dateEFormat = $dateE->format('Y-m-d H:i:s');
        } else {
            $dateEFormat = "";
        }
        $modele = $this->modeles;
        $array = Array(
            "id" => $this->id,
            "nom" => $this->nom,
            "categorie" => $this->categorie->getNom(),
            "fabricant" => $this->fabricant,
            "numproduit" => $this->numProduit,
            "prix" => $mo[0]->getPrix(),
            "prix_promotion" => $modele[0]->getPrixPromo(),
            "description" => $this->description,
            "dateFabrication" => $dateFFormat,
            "dateExpiration" => $dateEFormat,
            "famille" => $famille->toJSON(true),
            "entreprise" => $entreprise->toJSON(true),
            "modeles" => $modeles,
            "commandable" => $this->commandable,
            //"commentaires" => $commentaires,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Produit
     */
    public function setVisible($visible) {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible() {
        return $this->visible;
    }

    /**
     * Set purchased
     *
     * @param integer $purchased
     * @return Produit
     */
    public function setPurchased($purchased) {
        $this->purchased = $purchased;

        return $this;
    }

    /**
     * Get purchased
     *
     * @return integer 
     */
    public function getPurchased() {
        return $this->purchased;
    }

    /**
     * Add modeles
     *
     * @param \CIELO\EntrepriseBundle\Entity\Modele $modeles
     * @return Produit
     */
    public function addModele(\CIELO\EntrepriseBundle\Entity\Modele $modeles) {
        $this->modeles[] = $modeles;

        return $this;
    }

    /**
     * Remove modeles
     *
     * @param \CIELO\EntrepriseBundle\Entity\Modele $modeles
     */
    public function removeModele(\CIELO\EntrepriseBundle\Entity\Modele $modeles) {
        $this->modeles->removeElement($modeles);
    }

    /**
     * Get modeles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModeles() {
        return $this->modeles;
    }

    /**
     * Add commentaires
     *
     * @param \CIELO\EntrepriseBundle\Entity\CommentaireProduit $commentaires
     * @return Produit
     */
    public function addCommentaire(\CIELO\EntrepriseBundle\Entity\CommentaireProduit $commentaires) {
        $this->commentaires[] = $commentaires;

        return $this;
    }

    /**
     * Remove commentaires
     *
     * @param \CIELO\EntrepriseBundle\Entity\CommentaireProduit $commentaires
     */
    public function removeCommentaire(\CIELO\EntrepriseBundle\Entity\CommentaireProduit $commentaires) {
        $this->commentaires->removeElement($commentaires);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCommentaires() {
        return $this->commentaires;
    }

    /**
     * Add promotionProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits
     * @return Produit
     */
    public function addPromotionProduit(\CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits) {
        $this->promotionProduits[] = $promotionProduits;

        return $this;
    }

    /**
     * Remove promotionProduits
     *
     * @param \CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits
     */
    public function removePromotionProduit(\CIELO\EntrepriseBundle\Entity\PromotionProduit $promotionProduits) {
        $this->promotionProduits->removeElement($promotionProduits);
    }

    /**
     * Get promotionProduits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPromotionProduits() {
        return $this->promotionProduits;
    }

    /**
     * Add noteProduitUsers
     *
     * @param \CIELO\EntrepriseBundle\Entity\NoteProduitUser $noteProduitUsers
     * @return Produit
     */
    public function addNoteProduitUser(\CIELO\EntrepriseBundle\Entity\NoteProduitUser $noteProduitUsers) {
        $this->noteProduitUsers[] = $noteProduitUsers;

        return $this;
    }

    /**
     * Remove noteProduitUsers
     *
     * @param \CIELO\EntrepriseBundle\Entity\NoteProduitUser $noteProduitUsers
     */
    public function removeNoteProduitUser(\CIELO\EntrepriseBundle\Entity\NoteProduitUser $noteProduitUsers) {
        $this->noteProduitUsers->removeElement($noteProduitUsers);
    }

    /**
     * Get noteProduitUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNoteProduitUsers() {
        return $this->noteProduitUsers;
    }


    /**
     * Set prixParDefaut
     *
     * @param integer $prixParDefaut
     * @return Produit
     */
    public function setPrixParDefaut($prixParDefaut)
    {
        $this->prixParDefaut = $prixParDefaut;
    
        return $this;
    }

    /**
     * Get prixParDefaut
     *
     * @return integer 
     */
    public function getPrixParDefaut()
    {
        return $this->prixParDefaut;
    }

    /**
     * Set commandable
     *
     * @param boolean $commandable
     * @return Produit
     */
    public function setCommandable($commandable)
    {
        $this->commandable = $commandable;
    
        return $this;
    }

    /**
     * Get commandable
     *
     * @return boolean 
     */
    public function getCommandable()
    {
        return $this->commandable;
    }
}