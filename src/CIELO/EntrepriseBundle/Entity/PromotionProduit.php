<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PromotionProduit
 *
 * @ORM\Table(name="cielo_promotion_produit")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\PromotionProduitRepository")
 */
class PromotionProduit {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Promotion", inversedBy="promotionProduits")
     */
    private $promotion;

    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\Produit")
     */
    private $produit;

    /**
     * @var integer
     *
     * @ORM\Column(name="reduction", type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      minMessage = "La réduction doit être au moins de 0%",
     *      maxMessage = "La réduction doit être au plus de 100%"
     * )
     */
    private $reduction;

    /**
     * Set reduction
     *
     * @param integer $reduction
     * @return PromotionProduit
     */
    public function setReduction($reduction) {
        $this->reduction = $reduction;

        return $this;
    }

    /**
     * Get reduction
     *
     * @return integer 
     */
    public function getReduction() {
        return $this->reduction;
    }

    /**
     * Set promotion
     *
     * @param \CIELO\EntrepriseBundle\Entity\Promotion $promotion
     * @return PromotionProduit
     */
    public function setPromotion(\CIELO\EntrepriseBundle\Entity\Promotion $promotion) {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \CIELO\EntrepriseBundle\Entity\Promotion 
     */
    public function getPromotion() {
        return $this->promotion;
    }

    /**
     * Set produit
     *
     * @param \CIELO\EntrepriseBundle\Entity\Produit $produit
     * @return PromotionProduit
     */
    public function setProduit(\CIELO\EntrepriseBundle\Entity\Produit $produit) {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \CIELO\EntrepriseBundle\Entity\Produit 
     */
    public function getProduit() {
        return $this->produit;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

}