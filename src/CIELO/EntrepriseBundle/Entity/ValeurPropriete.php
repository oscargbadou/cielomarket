<?php

namespace CIELO\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValeurPropriete
 *
 * @ORM\Table(name="cielo_valeur_propriete")
 * @ORM\Entity(repositoryClass="CIELO\EntrepriseBundle\Entity\ValeurProprieteRepository")
 */
class ValeurPropriete
{
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\PrixProduit", inversedBy="valeurProprietes", cascade={"remove"})
     */
    private $prixProduit;
    
    /**
     * @ORM\ManyToOne(targetEntity="CIELO\EntrepriseBundle\Entity\ProprieteProduit", inversedBy="valeurProprietes", cascade={"remove"})
     */
    private $proprieteProduit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string", length=255)
     */
    private $valeur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     * @return ValeurPropriete
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    
        return $this;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set prixProduit
     *
     * @param \CIELO\EntrepriseBundle\Entity\PrixProduit $prixProduit
     * @return ValeurPropriete
     */
    public function setPrixProduit(\CIELO\EntrepriseBundle\Entity\PrixProduit $prixProduit = null)
    {
        $this->prixProduit = $prixProduit;
    
        return $this;
    }

    /**
     * Get prixProduit
     *
     * @return \CIELO\EntrepriseBundle\Entity\PrixProduit 
     */
    public function getPrixProduit()
    {
        return $this->prixProduit;
    }

    /**
     * Set proprieteProduit
     *
     * @param \CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduit
     * @return ValeurPropriete
     */
    public function setProprieteProduit(\CIELO\EntrepriseBundle\Entity\ProprieteProduit $proprieteProduit = null)
    {
        $this->proprieteProduit = $proprieteProduit;
    
        return $this;
    }

    /**
     * Get proprieteProduit
     *
     * @return \CIELO\EntrepriseBundle\Entity\ProprieteProduit 
     */
    public function getProprieteProduit()
    {
        return $this->proprieteProduit;
    }
}