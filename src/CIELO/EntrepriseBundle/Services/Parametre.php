<?php

namespace CIELO\EntrepriseBundle\Services;

use FOS\UserBundle\Model\UserManager;
use Doctrine\ORM\EntityManager;

class Parametre {

    public function __construct($em) {
        $this->em = $em;
    }
    
    public function categorieEntreprise($entreprise_id){
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $categories = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Categorie')
                ->findByEntreprise($currentUser->getEntreprise());
        return $categories;
    }
    
    public function getCurrentUserEntrepriseId(){
        $currentUser = $this->get("security.context")->getToken()->getUser();
        return $currentUser->getEntreprise()->getId();
    }

}

?>
