<?php

namespace CIELO\EntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\SecurityExtraBundle\Annotation\Secure;

class AccueilController extends Controller {

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function accueilAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $nbreCategorieEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:Categorie")->nbreCategorieEntreprise($currentUser->getEntreprise()->getId());
        $nbreProduitEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitEntreprise($currentUser->getEntreprise()->getId());
        $nbrePromotionEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:Promotion")->nbrePromotionEntreprise($currentUser->getEntreprise()->getId());
        $nbreProduitPromotionEntreprise = $this->getDoctrine()->getEntityManager()->getRepository("CIELOEntrepriseBundle:PromotionProduit")->nbreProduitPromotionEntreprise($currentUser->getEntreprise()->getId());
        return $this->render("CIELOEntrepriseBundle:Accueil:index.html.twig", array(
                    'nbreCategorie' => $nbreCategorieEntreprise,
                    'nbreProduit' => $nbreProduitEntreprise,
                    'nbrePromotion' => $nbrePromotionEntreprise,
                    'nbreProduitPromotion' => $nbreProduitPromotionEntreprise
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function administrationEntrepriseAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $adminEntreprise = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOUserBundle:AdminEntreprise')
                ->findByEntreprise($currentUser->getEntreprise());
        return $this->render('CIELOEntrepriseBundle:Accueil:administrationEntreprise.html.twig', array(
                    'adminEntreprise' => $adminEntreprise
        ));
    }
    
    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function rechercheEntrepriseAction($page) {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $searchContent = $this->get('request')->request->get('searchContent');
        $searchType = $this->get('request')->request->get('searchType');
        $em = $this->getDoctrine()
                ->getManager();
        switch ($searchType) {
            case 'produit':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEntrepriseBundle:Produit")->nbreProduitRechercherEntreprise($searchContent, $currentUser->getEntreprise()->getId());
                $elementsRechercher = $em->getRepository("CIELOEntrepriseBundle:Produit")->produitsRechercherEntreprise($searchContent, $premierElement, $currentUser->getEntreprise()->getId());
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOEntrepriseBundle:Produit:produitsRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
            case 'categorie':
                $premierElement = ($page - 1) * 100;
                $totalElements = $em->getRepository("CIELOEntrepriseBundle:Categorie")->nbreCategorieRechercher($searchContent, $currentUser->getEntreprise()->getId());
                $elementsRechercher = $em->getRepository("CIELOEntrepriseBundle:Categorie")->categoriesRechercher($searchContent, $premierElement, $currentUser->getEntreprise()->getId());
                $totalPages = ceil(intval($totalElements) / 100);
                return $this->render('CIELOEntrepriseBundle:Categorie:categoriesRechercher.html.twig', array(
                            'page' => $page,
                            'nbrTotalPages' => $totalPages,
                            'elementsRechercher' => $elementsRechercher,
                            'totalElements' => $totalElements
                ));
                break;
        }
    }

}
