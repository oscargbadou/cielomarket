<?php

namespace CIELO\EntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\AdminBundle\Entity\CampagneJeu;
use CIELO\AdminBundle\Entity\Campagne;

class CampagneController extends Controller {

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function listAction() {
        $user = $this->getUser();
        $entreprise = $user->getEntreprise();
        $campagnes = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:Campagne')
                ->findBy(array("entreprise"=>$entreprise->getId()));
        return $this->render('CIELOEntrepriseBundle:Campagne:list.html.twig', array(
                    'campagnes' => $campagnes,
        ));
    }

    public function listCampagneJeuAction(Campagne $campagne = null) {
        if ($campagne == null) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Campagne non trouvée');
        }

        $campagnesJeu = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOAdminBundle:CampagneJeu')
                ->findBy(array("campagne" => $campagne->getId()));
        return $this->render('CIELOEntrepriseBundle:Campagne:list_campagne_jeu.html.twig', array(
                    'campagnesJeu' => $campagnesJeu,
                    'campagne' => $campagne,
        ));
    }

    public function consulterCampagneJeuAction(CampagneJeu $campagneJeu = null) {
        if ($campagneJeu == null) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Campagne non trouvée');
        }
        $totalGagant = 0;
        $totalParticipation = $campagneJeu->getParticipationsJeu()->count();
        foreach ($campagneJeu->getParticipationsJeu() as $participation) {
            if ($participation->getGain() != "0")
                $totalGagant += 1;
        }
        return $this->render('CIELOEntrepriseBundle:Campagne:consulter_campagne_jeu.html.twig', array(
                    'totalGagnant' => $totalGagant,
                    'totalParticipation' => $totalParticipation,
                    'campagneJeu' => $campagneJeu
        ));
    }

}
