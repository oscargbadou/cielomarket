<?php

namespace CIELO\EntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use CIELO\EntrepriseBundle\Entity\Produit;
use CIELO\EntrepriseBundle\Entity\Promotion;
use CIELO\EntrepriseBundle\Entity\PromotionProduit;
use CIELO\EntrepriseBundle\Form\PromotionType;
use Doctrine\ORM\EntityRepository;

class PromotionController extends Controller {

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function promotionAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $promotions = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->findByEntreprise($currentUser->getEntreprise());
        $now = new \DateTime();
        return $this->render('CIELOEntrepriseBundle:Promotion:promotion.html.twig', array(
                    'promotions' => $promotions,
                    'now' => $now
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function creerPromotionAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $promotion = new Promotion();
        $form = $this->createForm(new PromotionType, $promotion);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $promotion->setEntreprise($currentUser->getEntreprise());
                $em->persist($promotion);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_enterprise_promotion'));
            }
        }
        return $this->render('CIELOEntrepriseBundle:Promotion:creerPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function modifierPromotionAction($id) {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        $form = $this->createForm(new PromotionType, $promotion);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $promotion->setEntreprise($currentUser->getEntreprise());
                $em->persist($promotion);
                $em->flush();
                return $this->redirect($this->generateUrl('cielo_enterprise_promotion'));
            }
        }
        return $this->render('CIELOEntrepriseBundle:Promotion:creerPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function supprimerPromotionAction($id) {
        $em = $this->getDoctrine()->getEntityManager();
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        if ($promotion === null) {
            throw $this->createNotFoundException('Cette prmotion n\'existe pas');
        }

        $allPromotionProduit = $promotion->getPromotionProduits();
        foreach ($allPromotionProduit as $pp) {
            $modeles = $pp->getProduit()->getModeles();
            foreach ($modeles as $m) {
                if ($m->getPrixPromo() != $m->getPrix()) {
                    $m->setPrixPromo($m->getPrix());
                    $em->flush();
                }
            }
        }      
        $em->remove($promotion);
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_enterprise_promotion'));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function ajouterUnProduitPromotionAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $_SESSION['id_entreprise'] = $currentUser->getEntreprise()->getId();
        $promotionProduit = new PromotionProduit();
        $now = new \DateTime();
        $form = $this->createFormBuilder($promotionProduit)
                ->add('promotion', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Promotion",
                    'property' => "nom",
                    'empty_value' => 'Choisir une promotion',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
//                                ->where("p.dateDebutPromotion <= :now")
//                                ->where("p.dateFinPromotion >= :now")
                                ->leftJoin('p.entreprise', 'e')
                                ->where('e.id = :id')
                                //->setParameter("now", new \DateTime())
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
                ->add('produit', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Produit",
                    'property' => "nom",
                    'empty_value' => 'Choisir un produit',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.categorie', 'c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
                ->add('reduction', 'integer', array(
                    'required' => true,
                    'attr' => array(
                        'min' => 0,
                        'max' => 100
                    )
                ))
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $promotionProduitExistant = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                        ->findOneBy(array(
                    'promotion' => $promotionProduit->getPromotion(),
                    'produit' => $promotionProduit->getProduit()
                ));
                if (!$promotionProduitExistant) {
                    $produit = $promotionProduit->getProduit();
                    $modeles = $produit->getModeles();
                    foreach ($modeles as $m) {
                        if ($promotionProduit->getPromotion()->getDateDebutPromotion()->format("Y-m-d") == $now->format("Y-m-d")) {
                            $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($promotionProduit->getReduction() / 100)));
                        }
                    }
                    $em = $this->getDoctrine()->getEntityManager();
                    $em->persist($promotionProduit);
                    $em->flush();
                    return $this->redirect($this->generateUrl('cielo_enterprise_ajouter_un_produit_promotion'));
                } else {
                    return $this->redirect($this->generateUrl('cielo_enterprise_erreur'));
                }
            }
        }
        return $this->render('CIELOEntrepriseBundle:Promotion:ajouterUnProduitPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function ajouterCategoriePromotionAction() {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $_SESSION['id_entreprise'] = $currentUser->getEntreprise()->getId();
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('promotion', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Promotion",
                    'property' => "nom",
                    'empty_value' => 'Choisir une promotion',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
                ->add('categorie', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Categorie",
                    'property' => "nom",
                    'empty_value' => 'Choisir une catégorie',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
                ->add('reduction', 'integer', array(
                    'required' => true
                ))
                ->getForm();
        $request = $this->get('request');
        $form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $data = $form->getData();
                $promotion = $data["promotion"];
                $categorie = $data["categorie"];
                $reduction = $data["reduction"];
                $produits = $categorie->getProduits();
                $promotionProduitExistantArray = array();
                foreach ($produits as $p) {
                    $promotionProduitExistant = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                            ->findOneBy(array(
                        'promotion' => $promotion,
                        'produit' => $p
                    ));
                    if (!$promotionProduitExistant) {
                        $modeles = $p->getModeles();
                        foreach ($modeles as $m) {
                            $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($reduction / 100)));
                        }
                        $newPromotionProduit = new PromotionProduit();
                        $newPromotionProduit->setProduit($p);
                        $newPromotionProduit->setPromotion($promotion);
                        $newPromotionProduit->setReduction($reduction);
                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($newPromotionProduit);
                        $em->flush();
                    } else {
                        $promotionProduitExistantArray[] = $promotionProduitExistant;
                    }
                }
                if (count($promotionProduitExistant) > 0) {
                    return $this->render('CIELOEntrepriseBundle:Promotion:erreurCategoriePromotion.html.twig', array(
                                'promotionProduitExistantArray' => $promotionProduitExistantArray
                    ));
                } else {
                    return $this->redirect($this->generateUrl('cielo_enterprise_ajouter_categorie_promotion'));
                }
            }
        }
        return $this->render('CIELOEntrepriseBundle:Promotion:ajouterUneCategoriePromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function modifierProduitDeLaPromotionAction($id) {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $_SESSION['id_entreprise'] = $currentUser->getEntreprise()->getId();
        $promotionProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                ->find($id);
        $form = $this->createFormBuilder($promotionProduit)
                ->add('promotion', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Promotion",
                    'property' => "nom",
                    'empty_value' => 'Choisir une promotion',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
                ->add('produit', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Produit",
                    'property' => "nom",
                    'empty_value' => 'Choisir un produit',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.categorie', 'c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
                ->add('reduction', 'integer', array(
                    'required' => true
                ))
                ->getForm();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $promotionProduitExistant = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                        ->findOneBy(array(
                    'promotion' => $promotionProduit->getPromotion(),
                    'produit' => $promotionProduit->getProduit()
                ));
                if (!$promotionProduitExistant) {
                    $produit = $promotionProduit->getProduit();
                    $modeles = $produit->getModeles();
                    foreach ($modeles as $m) {
                        $m->setPrixPromo(ceil($m->getPrix() - $m->getPrix() * ($promotionProduit->getReduction() / 100)));
                    }
                    $em = $this->getDoctrine()->getEntityManager();
                    $em->persist($promotionProduit);
                    $em->flush();
                    return $this->redirect($this->generateUrl('cielo_enterprise_ajouter_un_produit_promotion'));
                } else {
                    return $this->redirect($this->generateUrl('cielo_enterprise_erreur'));
                }
            }
        }
        return $this->render('CIELOEntrepriseBundle:Promotion:ajouterUnProduitPromotion.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function supprimerProduitDeLaPromotionAction($id) {
        $promotionProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                ->find($id);
        if ($promotionProduit === null) {
            throw $this->createNotFoundException('Ce produit n\'existe pas dans cette promotion');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($promotionProduit);
        $em->flush();
        $em->flush();
        return $this->redirect($this->generateUrl('cielo_enterprise_produit_dune_promotion', array(
                            'id' => $id
        )));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function produitDeLaPromotionAction($id) {
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        $promotionProduit = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:PromotionProduit')
                ->findByPromotion($promotion);
        // die(var_dump($promotionProduit));
        return $this->render('CIELOEntrepriseBundle:Promotion:produitDeLaPromotion.html.twig', array(
                    'promotionProduit' => $promotionProduit
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function erreurAction() {
        return $this->render('CIELOEntrepriseBundle:Promotion:erreur.html.twig');
    }

    public function creerPromotionEtape2Action($id) {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $categories = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Categorie')
                ->findByEntreprise($currentUser->getEntreprise());
        $promotion = $this->getDoctrine()
                ->getManager()
                ->getRepository('CIELOEntrepriseBundle:Promotion')
                ->find($id);
        $produitArray = array();
        foreach ($categories as $cat) {
            $produitArray[] = $cat->getProduits();
        }
        return $this->render('CIELOEntrepriseBundle:Promotion:creerPromotionEtape2.html.twig', array(
                    'produits' => $produitArray,
                    'promotion' => $promotion
        ));
    }

    /**
     * @Secure(roles="ROLE_ADMIN_ENTREPRISE")
     */
    public function ajouterProduitPromoAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == "POST") {
            $idPromo = $request->get("idPromo");
            $produits = json_decode($request->get("produits"), true);
            $promotion = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('CIELOEntrepriseBundle:Promotion')
                    ->find(intval($idPromo));
            foreach ($produits as $i => $produit) {
                $p = $this->em->getRepository("CIELOEntrepriseBundle:Produit")->find(intval($produit["id"]));
                $pomtionProduit[$i] = new PromotionProduit();
                $pomtionProduit[$i]->setPromotion($promotion);
                $pomtionProduit[$i]->setProduit($p);
                $pomtionProduit[$i]->setPrixPromo(intval(intval($produit["prixPromo"])));
                $this->em->persist($pomtionProduit[$i]);
            }
            $this->em->flush();
            return new Response('', 204, array('Access-Control-Allow-Origin' => '*'));
        }
        throw new NotFoundHttpException("Erreur données");
    }

}

?>
