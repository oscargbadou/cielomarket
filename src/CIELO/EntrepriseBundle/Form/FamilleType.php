<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FamilleType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        ->add('nom')
        ->add('description', 'textarea', array(
            'required'=>false
        ))
        ->add('domaineActivite', 'entity', array(
        'class' => "CIELOEntrepriseBundle:DomaineActivite",
        'property' => "nom",
        'empty_value' => 'Domaine d\'activité',
        ))
        ->add('document', new DocumentFileType())
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\Famille'
        ));
    }

    public function getName() {
        return 'cielo_enterprisebundle_familletype';
    }

}
