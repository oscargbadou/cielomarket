<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CategorieType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'required'=>true
                ))
                ->add('description', 'textarea', array(
                    'required'=>false
                ))
                ->add('categorie', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Categorie",
                    'property' => "nom",
                    'empty_value' => 'Choisir une catégorie',
                    'required' => false,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    }
                ))
                ->add('famille', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Famille",
                    'property' => "nom",
                    'empty_value' => 'Choisir une famille',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('f')
                                ->leftJoin('f.domaineActivite', 'd')
                                ->leftJoin('d.entreprises', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    }
                ))
                ->add('document', new DocumentFileType())
//                ->add('proprieteCategories', 'collection', array(
//                    'type' => new ProprieteCategorieType(),
//                    'allow_add' => true,
//                    'allow_delete' => true,
//                    'by_reference' => true
//                ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\Categorie'
        ));
    }

    public function getName() {
        return 'cielo_enterprisebundle_categorietype';
    }

}
