<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentFileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('file', 'file', array(
            'required' => false,
            'attr' => array(
                'accept'=>'image/jpeg, image/png, image/gif'
            )
        ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\Document'
        ));
    }

    public function getName() {
        return 'cielo_enterprisebundle_documentfiletype';
    }

}
