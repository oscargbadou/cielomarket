<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PromotionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dateDebutPromotion', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => true
                ))
                ->add('dateFinPromotion', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => true
                ))
                ->add('nom')
//                ->add('promotionProduits', 'collection', array(
//                    'type' => new PromotionProduitType(),
//                    'allow_add' => true,
//                    'allow_delete' => true,
//                    'by_reference' => true
//                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\Promotion'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_entreprisebundle_promotion';
    }

}
