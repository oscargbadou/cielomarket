<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PrixProduitType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('montant')
//                ->add('montantPromo')
//                ->add('produit', 'entity', array(
//                    'class' => "CIELOEntrepriseBundle:Produit",
//                    'property' => "nom",
//                    'empty_value' => 'Choisir un produit'
//                ))
                ->add('proprieteProduits', 'collection', array(
                    'type' => new ProprieteProduitType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\PrixProduit'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_entreprisebundle_prixproduit';
    }

}
