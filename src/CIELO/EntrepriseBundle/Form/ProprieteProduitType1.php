<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ProprieteProduitType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('valeurPropriete')
                ->add('proprieteCategorie', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:ProprieteCategorie",
                    'property' => "nom",
                    'empty_value' => 'Choisir une propriété de catégorie',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.categorie', 'c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
//                ->add('produit', 'entity', array(
//                    'class' => "CIELOEntrepriseBundle:Produit",
//                    'property' => "nom",
//                    'empty_value' => 'Choisir un Produit',
//                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\ProprieteProduit'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_entreprisebundle_proprieteproduit';
    }

}
