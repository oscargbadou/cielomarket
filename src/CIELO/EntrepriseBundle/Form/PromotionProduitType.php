<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PromotionProduitType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('reduction', 'integer', array(
                    'required' => true,
                    'attr' => array(
                        'min' => 0,
                        'max' => 100
                    )
                ))
                //->add('promotion')
                ->add('produit', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Produit",
                    'property' => "nom",
                    'empty_value' => 'Choisir un produit',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('p.categorie', 'c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\PromotionProduit'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_entreprisebundle_promotionproduit';
    }

}
