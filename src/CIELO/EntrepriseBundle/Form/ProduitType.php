<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ProduitType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('fabricant')
                ->add('numProduit')
                ->add('description', 'textarea')
                ->add('commandable', 'checkbox', array(
                    'required' => false,
                    'attr'=>array(
                        'checked'=>true
                    )
                ))
                ->add('dateFabrication', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => '2014-02-28'
                    )
                ))
                ->add('dateExpiration', 'birthday', array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => '2014-02-28'
                    )
                ))
                ->add('categorie', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:Categorie",
                    'property' => "nom",
                    'empty_value' => 'Choisir une catégorie',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->leftJoin('c.entreprise', 'e')
                                ->where('e.id = :id')
                                ->setParameter('id', $_SESSION['id_entreprise']);
                    },
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\Produit'
        ));
    }

    public function getName() {
        return 'cielo_enterprisebundle_produittype';
    }

}
