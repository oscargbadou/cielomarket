<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProprieteCategorieType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('description', 'textarea')
                ->add('type')
//                ->add('categorie', 'entity', array(
//                    'class' => "CIELOEntrepriseBundle:Categorie",
//                    'property' => "nom",
//                    'empty_value' => 'Choisir une Catégorie',
//                ))
                ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\ProprieteCategorie'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cielo_entreprisebundle_proprietecategorie';
    }

}
