<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EntrepriseType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('numIFU', 'text', array(
                    'required'=>false
                ))
                ->add('tel1', 'text', array(
                    'required'=>false
                ))
                ->add('tel2', 'text', array(
                    'required'=>false
                ))
                ->add('mail', 'email', array(
                    'required'=>false
                ))
                ->add('siteweb', 'text', array(
                    'required'=>false
                ))
                ->add('domaineactivite', 'entity', array(
                    'class' => "CIELOEntrepriseBundle:DomaineActivite",
                    'property' => "nom",
                    'empty_value' => 'Domaine d\'activité',
                ))
                ->add('document', new DocumentFileType())
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\Entreprise'
        ));
    }

    public function getName() {
        return 'cielo_enterprisebundle_entreprisetype';
    }

}
