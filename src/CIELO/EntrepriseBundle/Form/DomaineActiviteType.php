<?php

namespace CIELO\EntrepriseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DomaineActiviteType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array(
                    'required' => true
                ))
                ->add('description', 'textarea', array(
                    'required' => false
                ))
                ->add('document', new DocumentFileType())
                ->add('actif', 'checkbox', array(
                    'required' => false
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CIELO\EntrepriseBundle\Entity\DomaineActivite'
        ));
    }

    public function getName() {
        return 'cielo_enterprisebundle_domaineactivitetype';
    }

}
